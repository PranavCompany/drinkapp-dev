<?php

use Illuminate\Database\Seeder;

class OrderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order_items = \App\OrderItem::whereNotNull('addons')->get();
        foreach ($order_items as $order_item) {
            $new_addons = [];
            foreach ($order_item->addons as $key => $addon) {
                if (isset($addon['price']) && !isset($addon['price_with_discount'])) {
                    $addon['price_with_discount'] = $addon['price'];
                }
                $new_addons[$key] = $addon;
            }
            $order_item->fill(['addons'=>$new_addons]);
            $order_item->save();
        }
        \Illuminate\Support\Facades\DB::update('update order_items set item_price_with_discount=item_price where id <> "" and item_price_with_discount is null');
        $locations = \App\Location::whereNotNull('tips_list')->get();
        foreach ($locations as $location) {
            if (count($location->tips_list) > 3) {
                $tip_list = [];
                foreach ($location->tips_list as $index => $tip) {
                    if ($index <= 2) {
                        $tip_list[] = $tip;
                    }
                }
                $location->tips_list = $tip_list;
                $location->save();
            }
        }
    }
}
