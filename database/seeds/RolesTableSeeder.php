<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
	protected $roles = [
		[
			'name' => 'admin',
			'guard_name' => 'web'
		],
		[
			'name' => 'retailer',
			'guard_name' => 'web',
		],
		[
			'name' => 'customer',
			'guard_name' => 'web'
		]
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('roles')->insert($this->roles);
    }
}
