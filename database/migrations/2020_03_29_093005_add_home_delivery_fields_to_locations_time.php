<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHomeDeliveryFieldsToLocationsTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->string('home_delivery_time_start')->nullable(true);
            $table->string('home_delivery_time_end')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('home_delivery_time_start')->nullable(true);
            $table->dropColumn('home_delivery_time_end')->nullable(true);
        });
    }
}
