<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccVerifiedToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            /* 
             * 0: not concected
             * 1: restricted
             * 2: restricted soon
             * 3: pending
             * 4: enabled
             * 5: complete
             * 6: rejected
             */
            $table->unsignedTinyInteger('stripe_acc_verified')->default(0)->after('stripe_acc_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('stripe_acc_verified');
        });
    }
}
