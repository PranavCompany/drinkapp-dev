<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_option', function (Blueprint $table) {
            $table->unsignedInteger('item_id');
            $table->bigInteger('option_id')->unsigned();
            $table->foreign('item_id')
                ->references('id')
                ->on('items');

            $table->foreign('option_id')
                ->references('id')
                ->on('options');
            $table->primary(['item_id', 'option_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_option');
    }
}
