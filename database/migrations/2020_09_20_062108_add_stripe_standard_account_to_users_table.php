<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStripeStandardAccountToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('stripe_standard_account')->nullable();
            $table->integer('stripe_standard_account_verified')->default(0);
            $table->boolean('custom_account_disconnected')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('stripe_standard_account');
            $table->dropColumn('stripe_standard_account_verified');
            $table->dropColumn('custom_account_disconnected');
        });
    }
}
