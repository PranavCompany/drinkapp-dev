<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->BigIncrements('id');

            // Customer ID : refer to users table
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            // Retailer ID : refer to users table
            $table->unsignedInteger('retailer_id');
            $table->foreign('retailer_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            // Location ID : refer to locations table
            $table->unsignedInteger('location_id');
            $table->foreign('location_id')
                ->references('id')
                ->on('locations')
                ->onDelete('cascade');

            $table->decimal('total_price', 8, 2)->nullable();

            $table->text('description')->nullable();

            /*
             * Flag to paid or not
             *
             * 0: unpaid
             * 1: paid
             *
             */            
            $table->boolean('paid')->default(false);

            /*
             * Status
             *
             * 0: pending
             * 1: failed
             * 2: ready for collection
             * 3: ready for delivery
             * 4: completed
             *
             */
            $table->unsignedTinyInteger('status')->default(0);

            $table->timestamp('paid_at')->nullable();
            $table->timestamp('shipped_at')->nullable();
            $table->timestamp('completed_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
