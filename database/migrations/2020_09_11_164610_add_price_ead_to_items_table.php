<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceEadToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->unsignedDecimal('price_aed', 10, 2);
        });
        Schema::table('addons', function (Blueprint $table) {
            $table->unsignedDecimal('price_aed', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('price_aed');
        });
        Schema::table('addons', function (Blueprint $table) {
            $table->dropColumn('price_aed');
        });
    }
}
