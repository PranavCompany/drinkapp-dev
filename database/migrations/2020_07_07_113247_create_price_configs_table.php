<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('location_type')->unique();
            $table->float('customer_fee_percent')->default(0);
            $table->float('customer_fee_number')->default(0);
            $table->float('retailer_fee_percent')->default(0);
            $table->float('retailer_fee_number')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_configs');
    }
}
