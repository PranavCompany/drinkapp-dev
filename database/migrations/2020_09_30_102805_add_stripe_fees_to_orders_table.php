<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStripeFeesToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->bigInteger('stripe_fees')->nullable();
            $table->bigInteger('net_in_order_currency')->nullable();
            $table->bigInteger('fee_in_order_currency')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('stripe_fees');
            $table->dropColumn('net_in_order_currency');
            $table->dropColumn('fee_in_order_currency');
        });
    }
}
