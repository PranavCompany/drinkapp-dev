<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemAddonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_addon', function (Blueprint $table) {
            $table->unsignedInteger('item_id');
            $table->bigInteger('addon_id')->unsigned();
            $table->foreign('item_id')
                ->references('id')
                ->on('items');

            $table->foreign('addon_id')
                ->references('id')
                ->on('addons');
            $table->primary(['item_id', 'addon_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_addon');
    }
}
