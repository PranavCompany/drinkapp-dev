<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryFeeTableToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('total_fee', 8, 2)->after('total_price')->nullable();
            $table->decimal('grandtotal_price', 8, 2)->after('total_price')->nullable();
            $table->boolean('is_delivery')->default(false);
            $table->integer('table_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('total_fee');
            $table->dropColumn('grandtotal_price');
            $table->dropColumn('is_delivery');
            $table->dropColumn('table_number');
        });
    }
}
