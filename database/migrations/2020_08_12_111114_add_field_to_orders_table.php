<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('grandtotal_price_before_discount', 8, 2)->after('total_price')->nullable();
            $table->decimal('total_price_before_discount', 8, 2)->after('total_price')->nullable();
            $table->decimal('discount_sum', 8, 2)->after('total_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('grandtotal_price_before_discount');
            $table->dropColumn('total_price_before_discount');
            $table->dropColumn('discount_sum');
        });
    }
}
