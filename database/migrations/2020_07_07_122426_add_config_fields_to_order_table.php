<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfigFieldsToOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->float('customer_fee_percent')->default(4);
            $table->float('customer_fee_number')->default(0.20);
            $table->float('retailer_fee_percent')->default(1);
            $table->float('retailer_fee_number')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('customer_fee_percent');
            $table->dropColumn('customer_fee_number');
            $table->dropColumn('retailer_fee_percent');
            $table->dropColumn('retailer_fee_number');
        });
    }
}
