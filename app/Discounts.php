<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Discounts
 * @package App
 */
class Discounts extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'location_id',
        'discount',
        'code',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo('App\Location');
    }
    private static function getCode($n) {
        $characters = 'ABCDEFGHIJKLMNPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }
    public static function generateCodes($total, $discount, $location_id)
    {
        while (0 < $total) {
            $total--;
            $number = self::generateNumber($location_id);
            $code = new Discounts();
            $code->code = $number;
            $code->discount = $discount;
            $code->location_id = $location_id;
            $code->save();
        }
    }
    private static function generateNumber($location_id)
    {
        $number = self::getCode(6);
        if (self::codeExists($number, $location_id)) {
            return self::generateNumber($location_id);
        }
        return $number;
    }

    public static function codeExists($number, $location_id)
    {
        return Discounts::where('code', '=', $number)
            ->where('location_id', '=', $location_id)->exists();
    }
}
