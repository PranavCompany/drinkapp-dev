<?php

namespace App;

use App\Scopes\OwnerScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Location extends Model
{
    use SoftDeletes;

    public static $check_query = "case
when
	(time_start is null or time_start='' or  time_end is null or time_end = '') then 1
 when
	cast(replace(time_start, ':', '') as UNSIGNED) > cast(replace(time_end, ':', '') as UNSIGNED)
then
 (cast(date_format(utc_time, '%d%H%i') as UNSIGNED)
 		between
 		cast(concat(date_format(utc_time, '%d'), replace(time_start, ':', '')) as UNSIGNED) and
 		cast(concat(cast(date_format(utc_time, '%d') as UNSIGNED) + 1,replace(time_start, ':', '')) as UNSIGNED)
 	or 	cast(date_format(utc_time, '%d%H%i') as UNSIGNED)
 		between
 		cast(concat(cast(date_format(utc_time, '%d') as UNSIGNED) - 1,replace(time_start, ':', '')) as UNSIGNED) and
 		cast(concat(date_format(utc_time, '%d'), replace(time_end, ':', '')) as UNSIGNED)
 )
 else
  if (
  cast(replace(time_start, ':', '') as UNSIGNED) <= cast(date_format(utc_time, '%H%i') as UNSIGNED)
  and cast(date_format(utc_time, '%H%i') as UNSIGNED) <= cast(replace(time_end, ':', '') as UNSIGNED), 1,0
  )
   end";

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'home_delivery_days' => 'json',
        'home_delivery_radius_settings' => 'json',
        'tips_list' => 'json',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'description',
        'phone',
        'country',
        'state',
        'city',
        'address',
        'latitude',
        'currency',
        'longitude',
        'status',
        'tips_type',
        'tips_list',
        'to_table',
        'collection',
        'collection_time_start',
        'collection_time_end',
        'time_start',
        'location_type',
        'home_delivery_radius',
        'home_delivery_price',
        'time_end',
        'home_delivery_time_start',
        'home_delivery_time_end',
    ];

    public static function currencyToSymbol($currency)
    {
        $map = ['eur' => '€',
                'gbp' => '£',
                'usd' => '$',
                'aed' => 'د.إ',
            ];

        return $map[$currency]??'eur';
    }
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        // static::addGlobalScope(new DraftScope());
        static::addGlobalScope(new OwnerScope());
    }

    /**
     * Get the user for the blog article.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    /**
     * Get the user for the blog article.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function price_config()
    {
        return $this->belongsTo('App\PriceConfig', 'location_type', 'location_type');
    }
    /**
     * Get categories for the location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'location_has_categories')
            ->withPivot('time_start', 'time_end')
            ->orderBy('name', 'asc');
    }


    /**
     * Get categories for the location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories_with_items()
    {
        return $this->belongsToMany('App\Category', 'location_has_categories')

            ->whereRaw('exists(Select 1
                                    from items
                                             inner join location_has_items
                                    on location_has_items.location_id = ?
                                    and items.id = location_has_items.item_id
                                    where items.category_id = location_has_categories.category_id
                                    and items.deleted_at is null
                                    )', [$this->id])
            ->whereRaw(self::$check_query)
            ->withPivot('time_start', 'time_end')
            ->orderBy('name', 'asc');
    }

    /**
     * Get categories for the location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories_with_items_or_parent()
    {
        $cats = Category::join('location_has_categories',
            'categories.id', '=', 'location_has_categories.category_id')->whereRaw('exists(Select 1
             from items
                      inner join location_has_items on location_has_items.item_id = items.id
             where items.category_id = categories.id
               and location_has_items.location_id= ?
               and parent_id <> \'\'
               and items.deleted_at is null)', [$this->id])
//            ->whereRaw(self::$check_query)
            ->distinct()->get('parent_id')->toArray();
        $parents = [];
        foreach ($cats as $cat) {
            $parents[] = $cat['parent_id'];
        }
        $resp = $this->belongsToMany('App\Category', 'location_has_categories')
            ->where(function($query) use($parents) {
                $query->whereRaw('
           (exists(Select 1
           from items
                    inner join location_has_items on location_has_items.item_id = items.id
           where items.category_id = location_has_categories.category_id
             and location_has_items.location_id = `location_has_categories`.`location_id`
             and items.deleted_at is null) and parent_id is null)')
                    ->orWhereIn('categories.id', $parents);
            })
//            ->whereRaw(self::$check_query)
            ->withPivot('time_start', 'time_end');
        if (!empty($parents)) {
            $resp = $resp->orderByRaw('if (categories.id in ('.implode(',', $parents).'), 1, 0) DESC, name asc');
        } else {
            $resp = $resp->orderBy('name', 'asc');
        }
        return $resp;
    }
    /**
     * Get categories for the location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function all_categories_with_items()
    {
        return $this->belongsToMany('App\Category', 'location_has_categories')

            ->whereRaw('exists(Select 1
                                    from items
                                             inner join location_has_items
                                    on location_has_items.location_id = ?
                                    and items.id = location_has_items.item_id
                                    where items.category_id = location_has_categories.category_id
                                    and items.deleted_at is null
                                    )', [$this->id])
            ->withPivot('time_start', 'time_end')
            ->orderBy('name', 'asc');
    }

    /**
     * Get items for the location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function items()
    {
        return $this->belongsToMany('App\Item', 'location_has_items')->withPivot('out_of_stock', 'special_offer')->orderBy('name', 'asc');
    }
    public function special_offers() {
        return $this->belongsToMany('App\Item', 'location_has_items')
            ->where('special_offer', '=', '1')
            ->withPivot('out_of_stock', 'special_offer')->orderBy('name', 'asc');
    }
    public function out_of_stock_items() {
        return $this->belongsToMany('App\Item', 'location_has_items')
            ->where('out_of_stock', '=', '1');
    }
    /**
     * Get items for the location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function ordersToPrint()
    {
        $return_data = [];
        $status_list = [0];
        if ($this->collection) {
            $status_list[] = 2;
        }
        if ($this->to_table) {
            $status_list[] = 3;
        }
        if ($this->to_home) {
            $status_list[] = 5;
        }
        $data = DB::table('orders')
            ->select('orders.id', 'orders.total_price', 'orders.tips', 'orders.table_number', 'orders.queue',
                'orders.grandtotal_price', 'orders.description', 'users.name as customer_name',
                'orders.currency', 'orders.created_at',
                'orders.printed',
                'orders.latitude', 'orders.longitude',
                'orders.delivery_address', 'orders.phone_number',
                'orders.payment_id', 'order_items.item_type',
                'order_items.options', 'order_items.addons',
                'orders.status', 'orders.is_delivery', 'order_items.item_name as name', 'order_items.quantity')
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->join('items', 'order_items.item_id', '=', 'items.id')
            ->whereIn('orders.status', $status_list)
            ->where('orders.location_id', '=', $this->id)
            ->where('orders.printed', '=', false)
            ->whereNull('orders.deleted_at')
            ->where('orders.paid', '=', true)
            ->orderBy('orders.id', 'asc')
            ->get()->toArray();
        foreach ($data as $order_detail) {
            if (!isset($return_data[$order_detail->id])) {
                $return_data[$order_detail->id] = [
                    'total_price' => $order_detail->total_price,
                    'tips' => $order_detail->tips,
                    'table_number' => $order_detail->table_number,
                    'customer_name' => $order_detail->customer_name,
                    'queue' => $order_detail->queue,
                    'payment_id' => $order_detail->payment_id,
                    'grandtotal_price' => $order_detail->grandtotal_price,
                    'currency_symbol' => Location::currencyToSymbol($order_detail->currency),
                    'latitude' => $order_detail->latitude,
                    'longitude' => $order_detail->longitude,
                    'phone_number' => $order_detail->phone_number,
                    'delivery_address' => $order_detail->delivery_address,
                    'description' => $order_detail->description,
                    'printed' => $order_detail->printed,
                    'is_delivery' => $order_detail->is_delivery,
                    'created_at' => $order_detail->created_at,
                    'order_details' => [],
                ];
            }
            $return_data[$order_detail->id]['order_details'][] = [
                'name' => $order_detail->name,
                'quantity' => $order_detail->quantity,
                'item_type' => $order_detail->item_type,
                'options' => $order_detail->options?json_decode($order_detail->options):[],
                'addons' => $order_detail->addons?json_decode($order_detail->addons):[],
            ];
        }
        return $return_data;
    }

    public function ordersAtWork()
    {
        $status_list = [0];
        $return_data = [
            'pending' => [],
        ];
        if ($this->collection) {
            $status_list[] = 2;
            $return_data['collection'] = [];
        }
        if ($this->to_table) {
            $status_list[] = 3;
            $return_data['delivery'] = [];
        }
        if ($this->to_home) {
            $status_list[] = 5;
            $return_data['delivery'] = [];
        }
        $data = DB::table('orders')
            ->select('orders.id', 'orders.total_price', 'orders.tips', 'orders.table_number', 'orders.queue',
                'orders.grandtotal_price', 'orders.description', 'users.name as customer_name',
                'orders.currency', 'orders.created_at',
                'orders.printed',
                'orders.latitude', 'orders.longitude',
                'orders.delivery_address', 'orders.phone_number',
                'orders.payment_id', 'order_items.item_type',
                'order_items.options', 'order_items.addons',
                'orders.status', 'orders.is_delivery', 'order_items.item_name as name', 'order_items.quantity')
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->join('items', 'order_items.item_id', '=', 'items.id')
            ->whereIn('orders.status', $status_list)
            ->where('orders.location_id', '=', $this->id)
            ->where('orders.paid', '=', true)
            ->orderBy('orders.id', 'asc')
            ->get()->toArray();
        $return_data['last_order_id'] = '';
        $key_map = [
            0 => 'pending',
            2 => 'collection',
            3 => 'delivery',
            5 => 'delivery',
        ];
        foreach ($data as $order_detail) {
            $status = $key_map[$order_detail->status];
            if (!isset($return_data[$status][$order_detail->id])) {
                $return_data[$status][$order_detail->id] = [
                    'total_price' => $order_detail->total_price,
                    'tips' => $order_detail->tips,
                    'table_number' => $order_detail->table_number,
                    'customer_name' => $order_detail->customer_name,
                    'queue' => $order_detail->queue,
                    'payment_id' => $order_detail->payment_id,
                    'grandtotal_price' => $order_detail->grandtotal_price,
                    'currency_symbol' => Location::currencyToSymbol($order_detail->currency),
                    'latitude' => $order_detail->latitude,
                    'longitude' => $order_detail->longitude,
                    'phone_number' => $order_detail->phone_number,
                    'delivery_address' => $order_detail->delivery_address,
                    'description' => $order_detail->description,
                    'printed' => $order_detail->printed,
                    'is_delivery' => $order_detail->is_delivery,
                    'created_at' => $order_detail->created_at,
                    'order_id' => $order_detail->id,
                    'items' => [],
                ];
            }
            $return_data[$status][$order_detail->id]['items'][] = [
                'name' => $order_detail->name,
                'quantity' => $order_detail->quantity,
                'item_type' => $order_detail->item_type,
                'options' => $order_detail->options?json_decode($order_detail->options):[],
                'addons' => $order_detail->addons?json_decode($order_detail->addons):[],
            ];
            if (end($data) === $order_detail) {
                $return_data['last_order_id'] = $order_detail->id;
            }
        }
        return $return_data;
    }

    /**
     * Get the images for the location
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphToMany
     */
    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    /**
     * Get the created_at and return formatted date value
     *
     * @param string $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('j M Y');
    }
    public function getCreatedAtDbAttribute($value)
    {
        return $this->attributes['created_at'];
    }
    public function getUpdatedAtDbAttribute($value)
    {
        return $this->attributes['updated_at'];
    }

    public function getCurrencySymbolAttribute($value)
    {
        return self::currencyToSymbol($this->currency);
    }
    private static function getDeliveryByTime($start, $end, $value) {
        if (!empty($start) && !empty($end) && $value) {
            list($start_hours, $start_minutes) = explode(':', $start);
            list($end_hours, $end_minutes) = explode(':', $end);
            $time_start = Carbon::now('UTC')->setTime((int)$start_hours, (int)$start_minutes);
            $time_end = Carbon::now('UTC')->setTime((int)$end_hours, (int)$end_minutes);
            $now = Carbon::now('UTC');
            if ($start == $end) {
                return 1;
            }
            if ($time_start > $time_end) {
                $time_end_added = Carbon::now('UTC')->setTime((int)$end_hours, (int)$end_minutes)->addDays(1);
                $time_start_added = Carbon::now('UTC')->setTime((int)$start_hours, (int)$start_minutes)->addDays(-1);
            } else {
                $time_start_added = $time_start;
                $time_end_added = $time_end;
            }
            if (($time_start <= $now && $now <= $time_end_added)
                || ($time_start_added <= $now && $now <= $time_end)) {
                return 1;
            } else {
                return 0;
            }
        }
        return $value;
    }
    public function getToTableAutoAttribute($value) {
        return self::getDeliveryByTime($this->time_start, $this->time_end, $this->to_table);
    }
    public function getCollectionAutoAttribute($value) {
        return self::getDeliveryByTime($this->collection_time_start, $this->collection_time_end, $this->collection);
    }
    public function getToHomeAutoAttribute($value) {
        if ($this->home_delivery_days) {
            $weekMap = [
                0 => 'sun',
                1 => 'mon',
                2 => 'tue',
                3 => 'wen',
                4 => 'thu',
                5 => 'fri',
                6 => 'sat',
            ];
            $dayOfWeek = Carbon::now()->dayOfWeek;
            $weak_day = $weekMap[$dayOfWeek];
            if (!isset($this->home_delivery_days[$weak_day]) || !$this->home_delivery_days[$weak_day]) {
                return 0;
            }
        }
        return self::getDeliveryByTime($this->home_delivery_time_start, $this->home_delivery_time_end, $this->to_home);
    }
    public function getHomeDeliveryPriceOldAttribute()
    {
        if (is_array($this->home_delivery_radius_settings)) {
            $data = array_map(function ($a) {
                return Location::formatPrice($a['price']);
            }, $this->home_delivery_radius_settings);
            if (count($data) === 0) {
                return 0;
            }
            return max($data);
        }
        return 0;
    }
    public static function formatPrice($price)
    {
        return (float)$price?(string)number_format((float)$price, 2, '.', ''):null;
    }
    public function getHomeDeliveryMinRadiusAttribute()
    {
        if (is_array($this->home_delivery_radius_settings) and count($this->home_delivery_radius_settings) > 0) {
            return min(array_map(function ($a) {
                return (float)$a['radius']??null;
            }, $this->home_delivery_radius_settings));
        }
        return 0;
    }
    public function getHomeDeliveryMaxRadiusAttribute()
    {
        if (is_array($this->home_delivery_radius_settings) and count($this->home_delivery_radius_settings) > 0) {
            return max(array_map(function ($a) {
                return (float)$a['radius']??null;
            }, $this->home_delivery_radius_settings));
        }
        return 0;
    }
    public function getHomeDeliveryMinPriceAttribute()
    {
        if (is_array($this->home_delivery_radius_settings) and count($this->home_delivery_radius_settings) > 0) {
            return min(array_map(function ($a) {
                return (float)$a['price']??null;
            }, $this->home_delivery_radius_settings));
        }
        return 0;
    }
    public function getHomeDeliveryMaxPriceAttribute()
    {
        if (is_array($this->home_delivery_radius_settings) and count($this->home_delivery_radius_settings) > 0) {
            return max(array_map(function ($a) {
                return (float)$a['price']??null;
            }, $this->home_delivery_radius_settings));
        }
        return 0;
    }
    public function getRatingAttribute($value)
    {
        return [
            'total' => $this->hasMany('App\Rating')->count(),
            'avr' => $this->hasMany('App\Rating')->average('rating')
        ];
    }
    public static function distance($lat1, $lon1, $lat2, $lon2, $unit = 'Miles') {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    public function getPriceByDistance($distance)
    {
        $price = false;
        foreach ($this->home_delivery_radius_settings as $radius) {
            if ((float)$radius['radius'] >= $distance) {
                $price = $radius['price'];
                break;
            }
        }
        if (count($this->home_delivery_radius_settings) === 1 && !$this->home_delivery_radius_settings[0]['radius']) {
            $price = $this->home_delivery_radius_settings[0]['price'];
        }
        return (float)$price;
    }

    public function getRealPriceConfigAttribute()
    {
        if ($this->user->prices->count() === 0) {
            $price_config = $this->price_config;
        } else {
            $price_config = $this->user->prices()->where('location_type', $this->location_type)->first();
        }
        if ($this->currency === 'aed') {
            $price_config->customer_fee_number = round($price_config->customer_fee_number * 4.7, 2);
        }
        return $price_config;
    }

    public function getTipsListRealAttribute()
    {
        if (empty($this->tips_list)) {
            return [1, 2, 3];
        } else {
            return array_map(function ($a) {
                return (float)$a;
            }, array_filter($this->tips_list, function ($a) {
                return !empty($a);
            }));
        }
    }
}
