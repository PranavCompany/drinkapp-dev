<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Cashier\Billable;

use DB;

use Illuminate\Support\Carbon;
use App\Scopes\StatusScope;

use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, SoftDeletes, Billable;
    protected $dates = ['deleted_at'];
    protected $casts = [
        'currencies' => 'json',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'player_id', 'gender', 'yob', 'pin', 'facebook_id', 'google_id', 'apple_id', 'currencies'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'card_brand', 'card_last_four', 'pin',
        'stripe_id', 'stripe_acc_id', 'email_verified_at', 'player_id', 'prices'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new StatusScope());
    }

    /**
     * Get the locations for retail admin user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locations()
    {
        return $this->hasMany('App\Location')->orderBy('name', 'desc');
    }

    /**
     * Get the orders of customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order')->orderBy('created_at', 'desc');
    }

    /**
     * Get the orders of customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accepted_orders()
    {
        return $this->hasMany('App\Order', 'retailer_id');
//            ->where('paid', '=', 1);
    }

    /**
     * Get the user is administrator
     */
    public function getIsAdminAttribute()
    {
        return $this->hasRole('admin');
    }

    /**
     * Get the user is retail admin
     */
    public function getIsRetailerAttribute()
    {
        return $this->hasRole('retailer');
    }

    public function prices()
    {
        return $this->hasMany('App\UsersPriceConfig');
    }
    /**
     * Get the user role names
     */
    public function getRoleNamesAttribute()
    {
        return $this->getRoleNames();
    }

    /**
     * Get the created_at and return formatted date value
     *
     * @param string $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('j M Y');
    }

    // public function setPasswordAttribute($password)
    // {
    //     $this->attributes['password'] = bcrypt($password);
    // }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, $this->attributes['name']));
    }

    public function getCurrenciesCheckAttribute($value)
    {
        if (empty($this->currencies)) {
            return ['eur', 'gbp', 'usd', 'aed'];
        } else {
            return $this->currencies;
        }
    }

    public function getPriceConfigAttribute()
    {
        $data = [];
        $prices = $this->prices->count() !== 0 ? $this->prices : PriceConfig::all();
        foreach ($prices as $price) {
            $data[$price->location_type] = [
                'customer_fee_percent' => $price->customer_fee_percent,
                'customer_fee_number' => $price->customer_fee_number,
                'retailer_fee_percent' => $price->retailer_fee_percent,
                'retailer_fee_number' => $price->retailer_fee_number,
                'max_percent' => $price->max_percent,
            ];
        }
        return $data;
    }
}
