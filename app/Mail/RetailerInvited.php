<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class RetailerInvited extends Mailable
{
    use Queueable, SerializesModels;

    protected $retailer;
    protected $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $retailer, $password)
    {
        $this->retailer = $retailer;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'name' => $this->retailer->name,
            'email' => $this->retailer->email,
            'password' => $this->password,
            'app_url' => env('APP_URL')
        ];
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))->subject('You are invited to DrinkApp')->markdown('emails.retailerinvited')->with($data);
    }
}
