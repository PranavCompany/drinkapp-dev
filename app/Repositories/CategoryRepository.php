<?php

namespace App\Repositories;

use Auth;
use App\Category;

use App\Scopes\StatusScope;
use App\Scopes\OwnerScope;

class CategoryRepository
{
    use BaseRepository;

    /**
     * Category Model
     *
     * @var Category
     */
    protected $model;

    /**
     * Constructor
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    /**
     * Get the list of all the category without myself.
     *
     * @return mixed
     */
    public function getList($for_items = false)
    {
        if ($for_items) {
            $categories = Category::whereRaw("not exists(select 1 from categories as c
                     where
                      c.parent_id = categories.id and
                      c.deleted_at is null
                     and c.user_id = ?)", [auth()->user()->id])
                ->orderBy('categories.name', 'asc')->get();
        } else {
            $categories = Category::orderBy('name', 'asc')->get();
        }
        return $categories;
    }

    /**
     * Get the count of all categores.
     *
     * @return mixed
     */
    public function getCount()
    {
        return Category::count();
    }

    /**
     * Get the category by name.
     *
     * @param  string $name
     * @return mixed
     */
    public function getByName($name)
    {
        return Category::where('name', $name)->first();
    }

    /**
     * Get number of the records
     *
     * @param  Request $request
     * @param  int $number
     * @param  string $sort
     * @param  string $sortColumn
     * @return Paginate
     */
    public function pageWithRequest($request, $number = 10, $sort = 'desc', $sortColumn = 'created_at')
    {
        $keyword = $request->get('keyword');

        $user = $request->user();
        if ($user->hasRole('admin')) {
            return Category::withoutGlobalScope(OwnerScope::class)
                    ->when($keyword, function ($query) use ($keyword) {
                        $query->where('name', 'like', "%{$keyword}%");
                    })
                    ->orderBy($sortColumn, $sort)
                    ->paginate($number);
        }
        else {
            return Category::when($keyword, function ($query) use ($keyword) {
                        $query->where('name', 'like', "%{$keyword}%");
                    })
                    ->orderBy($sortColumn, $sort)
                    ->paginate($number);
        }
    }

    /**
     * Get number of the records
     *
     * @param  int $number
     * @param  string $sort
     * @param  string $sortColumn
     * @return Paginate
     */
    public function page($number = 10, $sort = 'desc', $sortColumn = 'created_at')
    {
        return Category::orderBy($sortColumn, $sort)->paginate($number);
    }

    /**
     * Get the article record without draft scope.
     *
     * @param  int $id
     * @return mixed
     */
    public function getById($id)
    {
        return Category::findOrFail($id);
    }

    /**
     * Update the article record without draft scope.
     *
     * @param  int $id
     * @param  array $input
     * @return boolean
     */
    public function update($id, $input)
    {
        $this->model = Category::findOrFail($id);

        return $this->save($this->model, $input);
    }

    /**
     * Delete the draft article.
     *
     * @param int $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->getById($id)->delete();
    }

    public function getModel()
    {
        return $this->model;
    }
}
