<?php

namespace App\Repositories;

use App\Option;
use Auth;

class OptionsRepository
{
    use BaseRepository;

    /**
     * Option Model
     *
     * @var Option
     */
    protected $model;

    /**
     * Constructor
     *
     * @param Option $option
     */
    public function __construct(Option $option)
    {
        $this->model = $option;
    }

    /**
     * Get the list of all the option without myself.
     *
     * @return mixed
     */
    public function getList()
    {
        return Option::orderBy('id', 'asc')->where('user_id', auth()->user()->id)->get();
    }

    /**
     * Get the option by name.
     *
     * @param  string $name
     * @return mixed
     */
    public function getByName($name)
    {
        return Option::where('id', $name)->where('user_id', auth()->user()->id)->first();
    }

    /**
     * Get number of the records
     *
     * @param  Request $request
     * @param  int $number
     * @param  string $sort
     * @param  string $sortColumn
     * @return Paginate
     */
    public function pageWithRequest($request, $number = 10, $sort = 'asc', $sortColumn = 'id')
    {
        $keyword = $request->get('keyword');

        return Option::when($keyword, function ($query) use ($keyword) {
            $query->where('name', 'like', "%{$keyword}%");
        })->where('user_id', auth()->user()->id)
            ->orderBy($sortColumn, $sort)
            ->paginate($number);
    }

    /**
     * Get number of the records
     *
     * @param  int $number
     * @param  string $sort
     * @param  string $sortColumn
     * @return Paginate
     */
    public function page($number = 10, $sort = 'asc', $sortColumn = 'id')
    {
        return Option::orderBy($sortColumn, $sort)->where('user_id', auth()->user()->id)->paginate($number);
    }

    /**
     * Get the article record without draft scope.
     *
     * @param  int $id
     * @return mixed
     */
    public function getById($id)
    {
        return Option::findOrFail($id);
    }

    /**
     * Update the article record without draft scope.
     *
     * @param  int $id
     * @param  array $input
     * @return boolean
     */
    public function update($id, $input)
    {
        $this->model = Option::findOrFail($id);

        return $this->save($this->model, $input);
    }

    /**
     * Delete the draft article.
     *
     * @param int $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->getById($id)->delete();
    }

}
