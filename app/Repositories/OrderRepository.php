<?php

namespace App\Repositories;

use App\Order;
use App\OrderItem;
use App\Scopes\OrderOwnerScope;
use App\Scopes\OwnerScope;
use Auth;

class OrderRepository
{
    use BaseRepository;

    /**
     * Order Model
     *
     * @var Order
     */
    protected $model;

    /**
     * Constructor
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->model = $order;
    }

    /**
     * Get the list of all the order without myself.
     *
     * @return mixed
     */
    public function getList()
    {
        return $this->model
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * Get the last of all the order without myself.
     *
     * @return mixed
     */
    public function getLastOrder($user)
    {
        return $this->model
            ->orderBy('id', 'desc')->where('user_id', $user->id)
            ->first();
    }

    /**
     * Get the order by name.
     *
     * @param string $name
     * @return mixed
     */
    public function getByName($name)
    {
        return $this->model
            ->where('name', $name)
            ->first();
    }

    /**
     * Get number of the records
     *
     * @param Request $request
     * @param int $number
     * @param string $sort
     * @param string $sortColumn
     * @return Paginate
     */
    public function pageWithRequest($request, $number = 5000, $sort = 'desc', $sortColumn = 'created_at')
    {
        $keyword = $request->get('keyword');

        $user = $request->user();
        if ($user->hasRole('admin')) {
            return Order::withoutGlobalScope(OwnerScope::class)
                ->orderBy($sortColumn, $sort)
                ->paginate($number);
        } else {
            return Order::orderBy('id', 'desc')
//                ->where('paid', '=', true)
                ->where('user_id', '=', $user->id)->paginate(5000);
        }
    }

    /**
     * Get number of the records
     *
     * @param Request $request
     * @param int $number
     * @param string $sort
     * @param string $sortColumn
     * @return Paginate
     */
    public function pageWithRequestPaid($request, $number = 5000, $sort = 'desc', $sortColumn = 'created_at')
    {
        $keyword = $request->get('keyword');

        $user = $request->user();
        if ($user->hasRole('admin')) {
            return Order::withoutGlobalScope(OwnerScope::class)
                ->orderBy($sortColumn, $sort)
                ->paginate($number);
        } else {
            return Order::orderBy('id', 'desc')
                ->where('paid', '=', true)
                ->where('user_id', '=', $user->id)->paginate(5000);
        }
    }

    /**
     * Get number of the records
     *
     * @param Request $request
     * @param int $number
     * @param string $sort
     * @param string $sortColumn
     * @return Paginate
     */
    public function pageWithRequestByRetailer($request, $number = 10, $sort = 'asc', $sortColumn = 'name')
    {
        $keyword = $request->get('keyword');

        $user = $request->user();
        if ($user->hasRole('admin')) {
            return Order::withoutGlobalScope(OwnerScope::class)
                ->orderBy($sortColumn, $sort)
                ->paginate($number);
        } else {
            return $user->accepted_orders;
        }
    }

    /**
     * Get number of the records
     *
     * @param int $number
     * @param string $sort
     * @param string $sortColumn
     * @return Paginate
     */
    public function page($number = 10, $sort = 'desc', $sortColumn = 'created_at')
    {
        return Order::orderBy($sortColumn, $sort)->paginate($number);
    }

    public function pageWithRequestAdmin(
        $request,
        $number = 10,
        $sort = 'desc',
        $sortColumn = 'created_at',
        $date_start = '',
        $date_end = ''
    ) {
        $keyword = $request->get('keyword');

        $user = $request->user();
        if ($user->hasRole('admin')) {
            return Order::withoutGlobalScope(OwnerScope::class)->withoutGlobalScope(OrderOwnerScope::class)
                ->orderBy($sortColumn, $sort)->when(!empty($date_start) && !empty($date_end),
                    function ($query) use ($date_start, $date_end) {
                        $query->whereBetween('created_at', [$date_start, $date_end]);
                    })->when($keyword, function ($query) use ($keyword) {
                    $query->where('orders.id', '=', $keyword);
                })
                ->paginate($number);
        }
    }

    /**
     * Get number of the records
     *
     * @param Request $request
     * @param int $number
     * @param string $sort
     * @param string $sortColumn
     * @return Paginate
     */
    public function search($request, $number = 10, $sort = 'asc', $sortColumn = 'name')
    {
        $keyword = $request->get('keyword');

        return Order::withoutGlobalScope(OwnerScope::class)
            ->when($keyword, function ($query) use ($keyword) {
                $query->where('name', 'like', "%{$keyword}%")->orWhere('address', 'like', "%{$keyword}%");
            })
            ->orderBy($sortColumn, $sort)
            ->paginate($number);
    }

    /**
     * Get the article record without draft scope.
     *
     * @param int $id
     * @return mixed
     */
    public function getById($id)
    {
        return Order::findOrFail($id);
    }

    /**
     * Update the article record without draft scope.
     *
     * @param int $id
     * @param array $input
     * @return boolean
     */
    public function update($id, $input)
    {
        $this->model = Order::findOrFail($id);

        return $this->save($this->model, $input);
    }

    /**
     * Delete the draft article.
     *
     * @param int $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->getById($id)->delete();
    }

    /**
     * Add Order to item to order.
     *
     * @param int $order_id
     * @param int $item_id
     * @param int $quantity
     * @param string $item_name
     * @param float $item_price
     * @return boolean
     */
    public function addItem(
        $order_id,
        $item_id,
        $quantity,
        $item_name,
        $item_price,
        $item_options,
        $addons,
        $item_type,
        $category_name,
        $price_with_discount
    ) {
        OrderItem::create([
            'order_id' => $order_id,
            'item_id' => $item_id,
            'quantity' => $quantity,
            'item_name' => $item_name,
            'item_price' => $item_price,
            'options' => $item_options,
            'addons' => $addons,
            'item_type' => $item_type,
            'category_name' => $category_name,
            'item_price_with_discount' => $price_with_discount
        ]);
    }
}
