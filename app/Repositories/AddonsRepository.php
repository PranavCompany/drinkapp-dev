<?php

namespace App\Repositories;

use App\Addon;
use Auth;

class AddonsRepository
{
    use BaseRepository;

    /**
     * Option Model
     *
     * @var Addon
     */
    protected $model;

    /**
     * Constructor
     *
     * @param Addon $option
     */
    public function __construct(Addon $option)
    {
        $this->model = $option;
    }

    /**
     * Get the list of all the option without myself.
     *
     * @return mixed
     */
    public function getList()
    {
        return Addon::orderBy('name', 'asc')->where('user_id', auth()->user()->id)->get();
    }

    /**
     * Get the option by name.
     *
     * @param  string $name
     * @return mixed
     */
    public function getByName($name)
    {
        return Addon::where('name', $name)->where('user_id', auth()->user()->id)->first();
    }

    /**
     * Get number of the records
     *
     * @param  Request $request
     * @param  int $number
     * @param  string $sort
     * @param  string $sortColumn
     * @return Paginate
     */
    public function pageWithRequest($request, $number = 10, $sort = 'asc', $sortColumn = 'id')
    {
        $keyword = $request->get('keyword');

        return Addon::when($keyword, function ($query) use ($keyword) {
            $query->where('name', 'like', "%{$keyword}%");
        })
            ->orderBy($sortColumn, $sort)->where('user_id', auth()->user()->id)
            ->paginate($number);
    }

    /**
     * Get number of the records
     *
     * @param  int $number
     * @param  string $sort
     * @param  string $sortColumn
     * @return Paginate
     */
    public function page($number = 10, $sort = 'asc', $sortColumn = 'id')
    {
        return Addon::orderBy($sortColumn, $sort)->where('user_id', auth()->user()->id)->paginate($number);
    }

    /**
     * Get the article record without draft scope.
     *
     * @param  int $id
     * @return mixed
     */
    public function getById($id)
    {
        return Addon::findOrFail($id);
    }

    /**
     * Update the article record without draft scope.
     *
     * @param  int $id
     * @param  array $input
     * @return boolean
     */
    public function update($id, $input)
    {
        $this->model = Addon::findOrFail($id);

        return $this->save($this->model, $input);
    }

    /**
     * Delete the draft article.
     *
     * @param int $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->getById($id)->delete();
    }

}
