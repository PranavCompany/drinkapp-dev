<?php

namespace App\Repositories;

use Auth;
use App\Item;

use App\Scopes\StatusScope;
use App\Scopes\OwnerScope;

class ItemRepository
{
    use BaseRepository;

    /**
     * Item Model
     *
     * @var Item
     */
    protected $model;

    /**
     * Constructor
     *
     * @param Item $item
     */
    public function __construct(Item $item)
    {
        $this->model = $item;
    }

    /**
     * Get the list of all the item without myself.
     *
     * @return mixed
     */
    public function getList()
    {
        return Item::orderBy('name', 'asc')->get();
    }

    /**
     * Get the item by name.
     *
     * @param  string $name
     * @return mixed
     */
    public function getByName($name)
    {
        return Item::where('name', $name)->first();
    }

    /**
     * Get number of the records
     *
     * @param  Request $request
     * @param  int $number
     * @param  string $sort
     * @param  string $sortColumn
     * @return Paginate
     */
    public function pageWithRequest($request, $number = 10, $sort = 'asc', $sortColumn = 'name')
    {
        $keyword = $request->get('keyword');

        $user = $request->user();
        if ($user->hasRole('admin')) {
            return Item::withoutGlobalScope(OwnerScope::class)
                    ->when($keyword, function ($query) use ($keyword) {
                        $query->where('name', 'like', "%{$keyword}%");
                    })
                    ->orderBy($sortColumn, $sort)
                    ->paginate($number);
        }
        else {
            return Item::when($keyword, function ($query) use ($keyword) {
                        $query->where('name', 'like', "%{$keyword}%");
                    })
                    ->where('user_id', '=', $user->id)
                    ->orderBy($sortColumn, $sort)
                    ->paginate($number);
        }
    }

    /**
     * Get number of the records
     *
     * @param  int $number
     * @param  string $sort
     * @param  string $sortColumn
     * @return Paginate
     */
    public function page($number = 10, $sort = 'asc', $sortColumn = 'name')
    {
        // return $this->model->withoutGlobalScope(StatusScope::class)->orderBy($sortColumn, $sort)->paginate($number);
        return Item::orderBy($sortColumn, $sort)->paginate($number);
    }

    /**
     * Get the article record without draft scope.
     *
     * @param  int $id
     * @return mixed
     */
    public function getById($id)
    {
        return Item::findOrFail($id);
    }

    /**
     * Update the article record without draft scope.
     *
     * @param  int $id
     * @param  array $input
     * @return boolean
     */
    public function update($id, $input)
    {
        $this->model = Item::findOrFail($id);

        return $this->save($this->model, $input);
    }

    /**
     * Delete the draft article.
     *
     * @param int $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->getById($id)->delete();
    }

    /**
     * Check the auth and the model without global scope when user is the admin.
     *
     * @return Model
     */
    public function withoutOwnerScope()
    {
        if (auth()->check() && auth()->user()->is_superadmin) {
            $this->model = $this->model->withoutGlobalScope(OwnerScope::class);
        }

        return $this->model;
    }
}
