<?php

namespace App\Repositories;

use App\Discounts;
use Auth;

/**
 * Class DiscountRepository
 * @package App\Repositories
 */
class DiscountRepository
{
    use BaseRepository;

    /**
     * Discounts Model
     *
     * @var Discounts
     */
    protected $model;

    /**
     * Constructor
     *
     * @param Discounts $discount
     */
    public function __construct(Discounts $discount)
    {
        $this->model = $discount;
    }

    /**
     * Get the list of all the discount without myself.
     *
     * @param $location_id
     * @return mixed
     */
    public function getList($location_id)
    {
        return Discounts::orderBy('id', 'desc')->where('location_id', $location_id)->get();
    }

    /**
     * Get the discount by code.
     *
     * @param  string $code
     * @return mixed
     */
    public function getByCode($code, $location_id)
    {
        return Discounts::where('code', $code)->where('location_id', $location_id)->first();
    }

    /**
     * Get number of the records
     *
     * @param Request $request
     * @param int $location_id
     * @param int $number
     * @param string $sort
     * @param string $sortColumn
     * @return Paginate
     */
    public function pageWithRequest($request, $location_id, $number = 10, $sort = 'asc', $sortColumn = 'id')
    {
        $keyword = $request->get('keyword');

        return Discounts::when($keyword, function ($query) use ($keyword) {
            $query->where('code', 'like', "%{$keyword}%");
        })->where('location_id', $location_id)
            ->orderBy($sortColumn, $sort)
            ->orderBy('id', 'asc')
            ->paginate($number);
    }

    /**
     * Get number of the records
     *
     * @param  int $number
     * @param  string $sort
     * @param  string $sortColumn
     * @return Paginate
     */
    public function page($number = 10, $sort = 'asc', $sortColumn = 'id')
    {
        return Discounts::orderBy($sortColumn, $sort)->where('user_id', auth()->user()->id)->paginate($number);
    }

    /**
     * Get the article record without draft scope.
     *
     * @param $location_id
     * @param int $id
     * @return mixed
     */
    public function getById($location_id, $id)
    {
        return Discounts::where('location_id', $location_id)->findOrFail($id);
    }

    /**
     * Update the article record without draft scope.
     *
     * @param $location_id
     * @param int $id
     * @param array $input
     * @return User
     */
    public function update($location_id, $id, $input)
    {
        $this->model = Discounts::where('location_id', $location_id)->findOrFail($id);

        return $this->save($this->model, $input);
    }

    /**
     * Delete the draft article.
     *
     * @param $location_id
     * @param int $id
     * @return boolean
     */
    public function destroy($location_id, $id)
    {
        return $this->getById($location_id, $id)->delete();
    }

}
