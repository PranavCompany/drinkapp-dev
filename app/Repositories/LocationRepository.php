<?php

namespace App\Repositories;

use App\Location;
use App\Scopes\OwnerScope;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationRepository
{
    use BaseRepository;

    /**
     * Location Model
     *
     * @var Location
     */
    protected $model;

    /**
     * Constructor
     *
     * @param Location $location
     */
    public function __construct(Location $location)
    {
        $this->model = $location;
    }

    /**
     * Get the list of all the location without myself.
     *
     * @return mixed
     */
    public function getList()
    {
        return $this->model
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * Get the location by name.
     *
     * @param string $name
     * @return mixed
     */
    public function getByName($name)
    {
        return $this->model
            ->where('name', $name)
            ->first();
    }

    /**
     * Get number of the records
     *
     * @param Request $request
     * @param int $number
     * @param string $sort
     * @param string $sortColumn
     * @return Paginate
     */
    public function pageWithRequest($request, $number = 10, $sort = 'asc', $sortColumn = 'name')
    {
        $keyword = $request->get('keyword');

        $user = $request->user();
        if ($user->hasRole('admin')) {
            return Location::withoutGlobalScope(OwnerScope::class)
                ->when($keyword, function ($query) use ($keyword) {
                    $query->where('name', 'like', "%{$keyword}%")->orWhere('address', 'like', "%{$keyword}%");
                })
                ->orderBy($sortColumn, $sort)
                ->paginate($number);
        } else {
            return Location::when($keyword, function ($query) use ($keyword) {
                $query->where('name', 'like', "%{$keyword}%")->orWhere('address', 'like', "%{$keyword}%");
            })
                ->orderBy($sortColumn, $sort)
                ->paginate($number);
        }
    }

    /**
     * Get number of the records
     *
     * @param int $number
     * @param string $sort
     * @param string $sortColumn
     * @return Paginate
     */
    public function page($number = 10, $sort = 'desc', $sortColumn = 'created_at')
    {
        return Location::orderBy($sortColumn, $sort)->paginate($number);
    }

    /**
     * Get number of the records
     *
     * @param Request $request
     * @param int $number
     * @param string $sort
     * @param string $sortColumn
     * @return Paginate
     */
    public function search($request, $number = 20, $sort = 'asc', $sortColumn = 'name')
    {
        $keyword = $request->get('keyword');
        $lat = $request->get('latitude');
        $lng = $request->get('longitude');

        $locations = Location::withoutGlobalScope(OwnerScope::class)
            ->when($keyword, function ($query) use ($keyword) {
                $query->where(function($q) use ($keyword) {
                    $q->where('name', 'like', "%{$keyword}%")->orWhere('address', 'like', "%{$keyword}%");
                });
            })->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('location_has_items')
                    ->whereRaw('locations.id = location_has_items.location_id');
            })->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('users')
                    ->whereRaw('locations.user_id = users.id')
                    ->whereRaw('(users.stripe_acc_verified=5 or users.stripe_standard_account_verified=5)');
            })->whereRaw('exists(Select 1
             from location_has_categories
                      inner join location_has_items on location_has_items.location_id
             where location_has_items.location_id = locations.id
               and item_id in (
                 select id
                 from items
                 where items.category_id = location_has_categories.category_id
                 and location_has_categories.location_id=locations.id
                   and items.deleted_at is null
             ))');
//        if (!empty($lat) && !empty($lng)) {
//            $locations = $locations->whereRaw('ST_Distance_Sphere(
//                            point(?, ?),
//                            point(locations.latitude, locations.longitude)
//                        ) < 50000', [$lat, $lng]);
//        }
        return $locations->orderBy($sortColumn, $sort)->paginate($number);
    }

    /**
     * Get ordered by geo location
     *
     * @param int $number
     * @param string $sort
     * @param string $sortColumn
     * @return Paginate
     */
    public function closest($request, $number = 20)
    {
        $lat = $request->get('latitude');
        $lng = $request->get('longitude');
        $keyword = $request->get('keyword');
        $locations = Location::withoutGlobalScope(OwnerScope::class)
            //todo return???
//            ->when($keyword, function ($query) use ($keyword) {
//                $query->where(function($q) use ($keyword) {
//                    $q->where('name', 'like', "%{$keyword}%")->orWhere('address', 'like', "%{$keyword}%");
//                });
//            })
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('users')
                    ->whereRaw('locations.user_id = users.id')
                    ->whereRaw('(
                    (users.custom_account_disconnected = 0 and users.stripe_acc_verified=5)
                    or (users.custom_account_disconnected = 1 and users.stripe_standard_account_verified=5))');
            })->whereRaw('exists(Select 1
             from location_has_categories
             inner join categories on categories.id = location_has_categories.category_id
             inner join location_has_items on location_has_items.location_id = location_has_categories.location_id
             where location_has_items.location_id = locations.id
             and categories.deleted_at is null
             and location_has_categories.location_id=locations.id
               and item_id in (
                 select id
                 from items
                 where items.category_id = location_has_categories.category_id
                   and items.deleted_at is null
             ))');

//        if (!empty($lat) && !empty($lng)) {
//            $locations = $locations->whereRaw('ST_Distance_Sphere(
//                            point(?, ?),
//                            point(locations.latitude, locations.longitude)
//                        ) < 50000', [$lat, $lng]);
//        } else {
//            $locations = $locations->limit($number);
//        }
        return $locations->orderByRaw('(latitude-?)*(latitude-?) + (longitude-?)*(longitude-?) asc', [$lat, $lat, $lng, $lng])
            ->get();
    }

    public function allTypes(Request $request)
    {
        $lat = $request->input('latitude');
        $lng = $request->input('longitude');
        $types = Location::withoutGlobalScope(OwnerScope::class)->select('location_type')
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('location_has_items')
                    ->whereRaw('locations.id = location_has_items.location_id');
            })->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('users')
                    ->whereRaw('locations.user_id = users.id')
                    ->whereRaw('(
                    (users.custom_account_disconnected = 0 and users.stripe_acc_verified=5)
                    or (users.custom_account_disconnected = 1 and users.stripe_standard_account_verified=5))');
            })->whereRaw('exists(Select 1
             from location_has_categories
             inner join categories on categories.id = location_has_categories.category_id
             inner join location_has_items on location_has_items.location_id = location_has_categories.location_id
             where location_has_items.location_id = locations.id
             and categories.deleted_at is null
             and location_has_categories.location_id=locations.id
               and item_id in (
                 select id
                 from items
                 where items.category_id = location_has_categories.category_id
                   and items.deleted_at is null
             ))');
        $return = [];
        if (!empty($lat) && !empty($lng)) {
            $types = $types->orderByRaw('(latitude-?)*(latitude-?) + (longitude-?)*(longitude-?) asc', [$lat, $lat, $lng, $lng])
                ->limit(10);
            foreach ($types->get() as $type) {
                $return[$type->location_type] = true;
            }
        } else {
            foreach ($types->distinct()->get() as $type) {
                $return[$type->location_type] = true;
            }
        }
        return $return;
    }

    /**
     * Get the article record without draft scope.
     *
     * @param int $id
     * @return mixed
     */
    public function getById($id)
    {
        return Location::withTrashed()->findOrFail($id);
    }

    /**
     * Update the article record without draft scope.
     *
     * @param int $id
     * @param array $input
     * @return boolean
     */
    public function update($id, $input)
    {
        $this->model = Location::findOrFail($id);

        return $this->save($this->model, $input);
    }

    /**
     * Delete the draft article.
     *
     * @param int $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->getById($id)->delete();
    }
}
