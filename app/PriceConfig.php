<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceConfig extends Model
{
    protected $fillable = [
        'location_type',
        'customer_fee_percent',
        'customer_fee_number',
        'retailer_fee_percent',
        'retailer_fee_number',
        'max_percent'
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'location_type'
    ];

    public function calculateFee($total, $location)
    {
        $fee_number = $this->customer_fee_number;
        return round($total * $this->customer_fee_percent / 100 + $fee_number, 2);
    }

    public function calculateAmount($total, $standard = false)
    {
        return (int)(($total - $this->retailer_fee_number) * (100 - $this->retailer_fee_percent));
    }

    public function calculateApplicationFee($currency, $total)
    {
        $customer_fee = round($total * $this->customer_fee_percent / 100 + $this->customer_fee_number, 2);
        $retailer_fee = round($total * $this->retailer_fee_percent / 100 + $this->retailer_fee_number, 2);
        $grand_total = $total + $customer_fee;
        switch ($currency) {
            case 'eur':
                $stripe_fee = $grand_total * 1.4 / 100 + 0.25;
                break;
            case 'gbp':
                $stripe_fee = $grand_total * 1.4 / 100 + 0.20;
                break;
            case 'aed':
                $stripe_fee = $grand_total * 1.4 / 100 + 0.25 * 4.7;
                break;
            default:
                $stripe_fee = $grand_total * 2.9 / 100 + 0.25;
                break;
        }
        $vat = round($stripe_fee * 0.21, 2);
        $stripe_fee = round($stripe_fee, 2);
        $stripe_total_fee = $stripe_fee + $vat;
        $total_fees = $stripe_total_fee + $retailer_fee;
        $application_fee = $customer_fee + $retailer_fee;
        if ($this->max_percent > 0 && ($total_fees * 100 / $total > $this->max_percent)) {
            $new_total_fee = $total_fees * $this->max_percent / ($total_fees*100/$total);
            $application_fee -= $total_fees - $new_total_fee;
            if ($application_fee < 0) {
                $application_fee = 0.01;
            }
        }
        return round($application_fee, 2);
    }
}
