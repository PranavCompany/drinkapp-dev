<?php

namespace App;

use App\Scopes\OrderOwnerScope;
use App\Scopes\OwnerScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['paid_at', 'shipped_at', 'completed_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'payment_id',
        'retailer_id',
        'location_id',
        'queue',
        'total_price',
        'grandtotal_price',
        'total_fee',
        'is_delivery',
        'table_number',
        'description',
        'charge_type',
        'status',
        'tips',
        'paid_at',
        'customer_fee_percent',
        'customer_fee_number',
        'retailer_fee_percent',
        'retailer_fee_number',
        'currency',
        'delivery_address',
        'phone_number',
        'home_delivery_price',
        'discount',
        'grandtotal_price_before_discount',
        'total_price_before_discount',
        'discount_sum',
        'shipped_at',
        'completed_at'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrderOwnerScope());
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePaid($query)
    {
        return $query->where('paid', 1);
    }

    /**
     * Get the user for the item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->withTrashed();
    }

    /**
     * Get the user for the item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function retailer()
    {
        return $this->belongsTo('App\User', 'retailer_id')->withTrashed();
    }

    /**
     * Get the location for the item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo('App\Location')->withoutGlobalScope('App\Scopes\OwnerScope')->withTrashed();
    }

    /**
     * Get order items for the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order_items()
    {
        return $this->hasMany('App\OrderItem')->with('item')->withTrashed();
    }

    /**
     * Get the created_at and return formatted date value
     *
     * @param string $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateTimeString();
    }

    public function getCreatedAtPrintAttribute($value)
    {
        return Carbon::parse($value)->format('M j g:iA');
    }

    public function getCreatedAtObjectAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at);
    }

    public function getCurrencySymbolAttribute($value)
    {
        return Location::currencyToSymbol($this->currency);
    }

    public static function handleDate($date, $offset, $format = 'Y/m/d', $start = false)
    {
        if (empty($date)) {
            return $date;
        }
        if ($start) {
            return Carbon::createFromFormat($format, $date)
                    ->format('Y-m-d') . ' ' . Carbon::createFromTime(3, 0, 0)
                    ->addMinutes((int)$offset)->format('H:i:s');
        } else {
            return Carbon::createFromFormat($format, $date)
                    ->addDay()->format('Y-m-d') . ' ' . Carbon::createFromTime(3, 0, 0)
                    ->addMinutes((int)$offset)->format('H:i:s');
        }
    }

    public static function validateOrder(
        $items,
        $total_price,
        $grand_total_price,
        $tip,
        $total_fee,
        $location,
        $delivery_price,
        $discount
    ) {
        $summ = 0;
        $price_name = 'price_' . $location->currency;
        $discount_sum = 0;
        foreach ($items as $item) {
            /** @var $item_obj Item */
            $item_obj = Item::withoutGlobalScope(OwnerScope::class)->findOrFail($item['id']);
            $categories_check = DB::table('location_has_categories')
                ->selectRaw('1')
                ->where('category_id', $item_obj->category_id)
                ->whereNull('time_start')
                ->whereNull('time_end')
                ->where('location_id', $location->id)->exists();
            $summ += $item['quantity'] * $item_obj->$price_name;
            if ($categories_check && $discount) {
                $discount_sum += $item['quantity'] * round($item_obj->$price_name * $discount / 100, 2);
            }
            if (isset($item['addons'])) {
                foreach ($item['addons'] as $addon) {
                    $summ += $item['quantity'] * $addon['price'];
                    if ($categories_check && $discount) {
                        $discount_sum += $item['quantity'] * round($addon['price'] * $discount / 100, 2);
                    }
                }
            }
        }

        $valid_total = $summ + $tip + $delivery_price - $discount_sum;
        $valid_total_fee = $location->real_price_config->calculateFee($valid_total, $location);
        $valid_grand_total = $valid_total + $valid_total_fee;
        $return = [
            'valid' => true,
            'discount_sum' => $discount_sum,
            'valid_total' => $valid_total,
            'valid_grand_total' => $valid_grand_total,
            'valid_total_fee' => $valid_total_fee
        ];
        if (round($total_price, 2) != round($valid_total, 2)
            || round($valid_grand_total, 2) != round($grand_total_price, 2)
            || round($valid_total_fee, 2) != round($total_fee, 2)
        ) {
            $return['valid'] = false;
        }
        return $return;
    }

    public static function generateReportSoldQuery($user_id, $location, $date_start, $date_end, $sort_key, $sort_order)
    {
        $items = DB::table('order_items')
            ->selectRaw('categories.name as category,
             orders.currency,
              item_name as item,
               JSON_ARRAYAGG(json_object("addons", addons -> "$.*", "quantity", quantity)) as addons,
               sum(order_items.quantity) as quantity,
                avg(item_price) as price, orders.currency, sum(item_price*order_items.quantity) as total  ')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('items', 'items.id', '=', 'order_items.item_id')
            ->leftJoin('categories', 'items.category_id', '=', 'categories.id')
            ->join('locations', 'locations.id', '=', 'orders.location_id')
            ->where('orders.retailer_id', '=', $user_id)
            ->where('orders.status', '!=', 6)
            ->where('orders.status', '!=', 1)
            ->groupBy('order_items.item_id');
        if ($location) {
            $items = $items->where('orders.location_id', '=', $location);
        } else {
            $items = $items->whereRaw('0=1');
        }
        $items = $items->when(!empty($date_start) && !empty($date_end), function ($query) use ($date_start, $date_end) {
            $query->whereBetween('orders.created_at', [$date_start, $date_end]);
        });
        if (isset($sort_order) && isset($sort_key)
            && !empty($sort_key) && !empty($sort_order)) {
            $items->orderBy($sort_key, $sort_order);
        } else {
            $items->orderBy('orders.created_at', 'desc');
        }
        return $items;
    }

    public function getStatusName()
    {
        $map = [
            0 => 'pending',
            1 => 'failed',
            2 => 'ready for collection',
            3 => 'ready for delivery',
            4 => 'completed',
            5 => 'home delivery',
            6 => 'refund',
        ];
        return $map[$this->status];
    }

    public function getDrinkAppFeeAttribute()
    {
        if ($this->charge_type === 'custom') {
            return floor(($this->total_price * $this->retailer_fee_percent / 100 + $this->retailer_fee_number) * 100) / 100;
        } else {
            return floor(($this->total_price * $this->retailer_fee_percent / 100 + $this->retailer_fee_number) * 100) / 100;
        }
    }

    public function getOrderRevenueAttribute()
    {
        if ($this->charge_type === 'custom') {
            return floor(($this->total_price - ($this->total_price * $this->retailer_fee_percent / 100 + $this->retailer_fee_number)) * 100) / 100;
        } else {
            if ($this->net) {
                if (empty($this->net_in_order_currency)) {
                    return $this->net / 100;
                } else {
                    return $this->net_in_order_currency / 100;
                }
            } else {
                return $this->total_price;
            }
        }
    }

    public function getStripeFeeAttribute()
    {
        if ($this->charge_type === 'custom') {
            return floor(($this->total_price * $this->retailer_fee_percent / 100 + $this->retailer_fee_number) * 100) / 100 + $this->total_fee;
        } else {
            if ($this->fee_in_order_currency) {
                if (empty($this->fee_in_order_currency)) {
                    return round($this->total_price - $this->net / 100, 2);
                } else {
                    return $this->fee_in_order_currency / 100;
                }
            } else {
                return 0;
            }
        }
    }

    public function getNetCurrencySymbolAttribute()
    {
        if (empty($this->net_currency)) {
            return '';
        }
        return Location::currencyToSymbol($this->net_currency);
    }

    public static function drinkAppFeeQuery()
    {
        return ' floor((orders.total_price * orders.retailer_fee_percent / 100 + orders.retailer_fee_number)*100)/100 ';
    }

    public static function orderRevenueQuery()
    {
        return 'if(orders.charge_type = "custom",floor((orders.total_price - orders.tips - (orders.total_price * orders.retailer_fee_percent / 100 + orders.retailer_fee_number))*100)/100, if (orders.net, orders.net/100,orders.total_price - orders.tips))';
    }
}
