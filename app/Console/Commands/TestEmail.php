<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use Mail;
use App\Mail\RetailerInvited;

class TestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drink:testemail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::find(5);

        Mail::to($user->email)->queue(new RetailerInvited($user, '123456'));
    }
}
