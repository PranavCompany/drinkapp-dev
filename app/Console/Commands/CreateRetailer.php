<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App;
use App\User;
use Illuminate\Support\Facades\Hash;

class CreateRetailer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drink:createretailer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User;
        $user->name = "Test01";
        $user->email = "retailer@ddrink.com";
        $user->password = Hash::make('123456');
        $user->status = 1;

        $user->save();
        $user->assignRole('retailer');
    }
}
