<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Location;

class UpdateAverageOrderTimes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drink:updateordertimes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the average order times for all locations.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all locations
        $locationsAll = Location::all();
        foreach ($locationsAll as $location) {

            // see if there are any recent orders for this location
            $sql = "select count(id) as count from orders where location_id = '$location->id' and completed_at >= DATE_SUB(NOW(),INTERVAL 2 HOUR)";
            $recentOrdersFound = DB::select($sql);
            if ($recentOrdersFound[0]->count == 0) {
                $location->average_order_time = null;
                $location->save();
            }
        }

        // get list of all locations with orders in the past 2 hours, where the order time is less than 45 minutes
        $sql = "select location_id, avg(TIMESTAMPDIFF(MINUTE, paid_at, completed_at)) as average from orders where completed_at >= DATE_SUB(NOW(),INTERVAL 2 HOUR) and TIMESTAMPDIFF(MINUTE, paid_at, completed_at) < 45 group by location_id";
        $locations = DB::select($sql);

        if (count($locations) > 0) {
            foreach ($locations as $location) {
                $locationObject = Location::find($location->location_id);
                $locationObject->average_order_time = "~".intval($location->average)." minutes";
                $locationObject->save();
            }
        }

    }
}
