<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App;
use App\User;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drink:createadmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User;
        $user->name = "Admin";
        $user->email = "admin@drink.com";
        $user->password = Hash::make('admin');
        $user->status = 1;

        $user->save();
        $user->assignRole('admin');
    }
}
