<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class PolicyController extends Controller
{
    /**
     * Show the profile for the given user.
     * @return View
     */
    public function show()
    {
        return view('policy');
    }
}