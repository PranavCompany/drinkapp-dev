<?php

namespace App\Http\Controllers\Webhooks;

use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use Carbon\Carbon;
use Stripe\Stripe;
use OneSignal;

class StripeController extends Controller
{
    public function handle_internal()
    {
        info("============ Webhook Stripe Internal =========");

        $input = @file_get_contents('php://input');
        $event = json_decode($input);
        switch ($event->type) {
            case 'application_fee.created':
            case 'application_fee.refunded':
                try {
                    Stripe::setApiKey(env('STRIPE_SECRET'));
                    $charge = \Stripe\Charge::retrieve($event->data->object->charge,
                        ['stripe_account' => $event->data->object->account]);
                    $order = Order::where('payment_id', '=', $charge->payment_intent)->first();
                    if ($order) {
                        if ($event->type === 'application_fee.created') {
                            $order->paid = true;
                            $order->status = 0;
                            $order->paid_at = Carbon::now();
                            $order->payment_status = 'succeeded';
                            $balance = \Stripe\BalanceTransaction::retrieve($charge->balance_transaction,
                                ['stripe_account' => $event->data->object->account]);
                            $order->net = $balance->net;
                            $order->net_currency = $balance->currency;
                            $order->stripe_fees = $balance->fee;
                            $net = $balance->net;
                            $fee = $balance->fee;
                            if (isset($balance->exchange_rate) && !empty($balance->exchange_rate)) {
                                $net = round($balance->net / $balance->exchange_rate);
                                $fee = round($balance->fee / $balance->exchange_rate);
                            }
                            $order->net_in_order_currency = $net;
                            $order->fee_in_order_currency = $fee;
                            $order->save();
                            $charge->updateAttributes(['description' => 'order id: ' . $order->id]);
                            $charge->save();
                        } else {
                            $order->status = 6;
                            $order->save();
                            $user = $order->user()->first();
                            if ($user->player_id) {
                                \OneSignal::sendNotificationToUser($user->name . ", Your order was refunded",
                                    $user->player_id, null,
                                    ["order_id" => $order->id]);
                            }
                        }
                    }
                } catch (\ErrorException $e) {

                }
                break;
            case 'charge.refunded':
                Stripe::setApiKey(env('STRIPE_SECRET'));
                $charge = \Stripe\Charge::retrieve($event->data->object->id);
                $order = Order::where('payment_id', '=', $charge->payment_intent)->first();
                if ($order) {
                    $order->status = 6;
                    $order->save();
                    $user = $order->user()->first();
                    if ($user->player_id) {
                        \OneSignal::sendNotificationToUser($user->name . ", Your order was refunded",
                            $user->player_id, null,
                            ["order_id" => $order->id]);
                    }
                }
//                    $order = Order::where('payment_id', '=',  $event->data->object->id)->first();
//                    if (!empty($order)) {
//                        if ($event->type === 'payment_intent.succeeded') {
//                            $order->paid = true;
//                            $order->paid_at = Carbon::now();
//                            $order->payment_status = 'succeeded';
//                            $order->save();
//                        } else {
//                            $order->status = 1;
//                            $order->payment_status = 'payment_failed';
//                            $order->save();
//                            $order->delete();
//                        }
//                    }
                break;
            case 'payment_intent.succeeded':
            case 'payment_intent.payment_failed':
//                if (isset($event->data->object->metadata)
//                    && isset($event->data->object->metadata->order_id)) {
//                    $order_id = $event->data->object->metadata->order_id;
//                    if (!empty($order_id)) {
//                        $order = Order::findOrFail($event->data->object->metadata->order_id);
//                        if ($event->type === 'payment_intent.succeeded') {
//                            $order->paid = true;
//                            $order->paid_at = Carbon::now();
//                            $order->payment_status = 'succeeded';
//                            $order->save();
//                        } else {
//                            $order->status = 1;
//                            $order->payment_status = 'payment_failed';
//                            $order->save();
//                            $order->delete();
//                        }
//                    }
//                }
                $order = Order::where('payment_id', '=', $event->data->object->id)->first();
                if (!empty($order)) {
                    if ($event->type === 'payment_intent.succeeded') {
                        $order->paid = true;
                        $order->status = 0;
                        $order->paid_at = Carbon::now();
                        $order->payment_status = 'succeeded';
                        $order->save();
                    } else {
                        $order->status = 1;
                        $order->payment_status = 'payment_failed';
                        $order->save();
                        $order->delete();
                    }
                }
                break;
            default:
                # code...
                break;
        }

        return response("Webhook handled successfully ", 200);
    }

    public function handle_connections()
    {
        info("============ Webhook Stripe Connections =========");

        $input = @file_get_contents('php://input');
        $event = json_decode($input);
        /** @var $user User */
        switch ($event->type) {
            case 'account.updated':
                /*
                 * 0: not concected
                 * 1: restricted
                 * 2: restricted soon
                 * 3: pending
                 * 4: enabled
                 * 5: complete
                 * 6: rejected
                 */
                if (!isset($event->metadata->env) || $event->metadata->env === env('APP_ENV')) {
                    $account_obj = $event->data->object;
                    if ($account_obj->type === 'standard') {
                        $user = User::where('stripe_standard_account', '=', $account_obj->id)->get()->first();
                        if (!empty($user->id)) {
                            if ($account_obj->charges_enabled) {
                                $user->stripe_standard_account_verified = 5;
                                $user->save();
                            }
                        }
                    } else {
                        $user = User::where('stripe_acc_id', '=', $account_obj->id)->get()->first();
                        if (!empty($user->id)) {
                            if (empty($account_obj->requirements->eventually_due) && $user->stripe_acc_verified < 4) {
                                $user->stripe_acc_verified = 4;
                                $user->save();
                            }
                        }
                    }
                }

                break;
            case 'person.updated':
                if (!isset($event->metadata->env) || $event->metadata->env === env('APP_ENV')) {
                    $user = User::where('stripe_acc_id', '=', $event->account)->get()->first();
                    if (!empty($user->id)) {
                        if ($event->data->object->verification->status === 'verified'
                            && $user->stripe_acc_verified < 5) {
                            $user->stripe_acc_verified = 5;
                            $user->save();
                        }
                    }
                    $user = User::where('stripe_standard_account', '=', $event->account)->get()->first();
                    if (!empty($user->id)) {
                        if ($event->data->object->verification->status === 'verified'
                            && $user->stripe_standard_account_verified < 5) {
                            $user->stripe_standard_account_verified = 5;
                            $user->custom_account_disconnected = 1;
                            $user->save();
                        }
                    }
                }
                break;
            case 'payment_intent.succeeded':
            case 'payment_intent.payment_failed':
                $order = Order::where('payment_id', '=', $event->data->object->id)->first();
                if (!empty($order)) {
                    if ($event->type === 'payment_intent.succeeded') {
                        $order->paid = true;
                        $order->paid_at = Carbon::now();
                        $order->payment_status = 'succeeded';
                        if ($order->charge_type === 'standard') {
                            try {
                                $balance = \Stripe\BalanceTransaction::retrieve($event->data->object->charges->data[0]->balance_transaction,
                                    ['stripe_account' => $order->user->stripe_standard_account]);
                                $order->net = $balance->net;
                                $order->net_currency = $balance->currency;
                            } catch (\Stripe\Exception\ApiErrorException $e) {

                            }
                        }
                    } else {
                        $order->payment_status = 'payment_failed';
                    }
                    $order->save();
                }
                break;
        }
        return response("Webhook handled successfully", 200);
    }
}
