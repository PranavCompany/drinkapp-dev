<?php

namespace App\Http\Controllers;

use App\Order;
use App\Scopes\OrderOwnerScope;
use App\Scopes\OwnerScope;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class OrderController extends Controller
{
    function toNumber($number, $currency)
    {
        return $number;
    }

    public function export(Request $request)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=export.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $user = $request->user();
        $keyword = $request->input('keyword');
        $date_start = Order::handleDate($request->input('dateStart'), $request->input('offset'), 'Y/m/d', true);
        $date_end = Order::handleDate($request->input('dateEnd'), $request->input('offset'));
        if ($user->hasRole('admin')) {
            $orders = Order::withoutGlobalScope(OwnerScope::class)->withoutGlobalScope(OrderOwnerScope::class);
        } else {
            $orders = $user->accepted_orders();
        }
        if (isset($request->sortOrder) && isset($request->sortKey)
            && !empty($request->sortKey) && !empty($request->sortOrder)) {
            $orders->orderBy($request->sortKey, $request->sortOrder);
        } else {
            $orders->orderBy('created_at', 'desc');
        }
        $orders->when($keyword, function ($query) use ($keyword) {
            $query->where('orders.id', '=', $keyword);
        });
        $data = $orders->when(!empty($date_start) && !empty($date_end), function ($query) use ($date_start, $date_end) {
            $query->whereBetween('created_at', [$date_start, $date_end]);
        })->get();
        $offset = -1 * (int)$request->input('offset');
        $map = [
            [
                'name' => 'Id Order',
                'callback' => function ($order) {
                    return '#' . $order->id;
                },
            ],
            [
                'name' => 'Location',
                'callback' => function ($order) {
                    return $order->location()->first()->name;
                },
            ],
            [
                'name' => 'Name',
                'callback' => function ($order) {
                    return $order->user()->first()->name;
                },
            ],
            [
                'name' => 'Table',
                'callback' => function ($order) {
                    return $order->table_number;
                },
            ],
            [
                'name' => 'Items',
                'callback' => function ($order) {
                    $ret = [];
                    foreach ($order->order_items as $item) {
                        $str = $item->quantity . ' X ' . $item->item_name;
                        if (!empty($item->addons)) {
                            $str .= '(' . implode(', ', array_map(function ($addon) use ($item) {
                                    return $item->quantity . ' X ' . $addon['name'];
                            }, $item->addons)) . ')';
                        }
                        $ret[] = $str;
                    }
                    return implode($ret, ', ');
                },
            ],
            [
                'name' => 'Total items price',
                'callback' => function ($order) {
                    $sum = 0;
                    foreach ($order->order_items as $item) {
                        $sum += $item->quantity * $item->item_price;
                        if (!empty($item->addons)) {
                            foreach ($item->addons as $addon) {
                                $sum += (float)$addon['price'] * $item->quantity;
                            }
                        }
                    }
                    return $this->toNumber($sum, $order->currency_symbol);
                },
            ],
            [
                'name' => 'Total order Price',
                'callback' => function ($order) {
                    return $this->toNumber($order->grandtotal_price, $order->currency_symbol);
                },
            ],
            [
                'name' => 'Customer fee',
                'callback' => function ($order) {
                    return $this->toNumber($order->total_fee, $order->currency_symbol);
                },
            ],
            [
                'name' => 'Tips',
                'callback' => function ($order) {
                    return !$order->tips ? '' : $this->toNumber($order->tips, $order->currency_symbol);
                },
            ],
            [
                'name' => 'Delivery',
                'callback' => function ($order) {
                    return !$order->home_delivery_price ? '' : $this->toNumber($order->home_delivery_price,
                        $order->currency_symbol);
                },
            ],
            [
                'name' => 'DrinkApp fee',
                'callback' => function ($order) {
                    return $this->toNumber($order->drink_app_fee, $order->currency_symbol);
                },
            ],
            [
                'name' => 'Total fees',
                'callback' => function ($order) {
                    return $this->toNumber($order->stripe_fee, $order->currency_symbol);
                },
            ],
            [
                'name' => 'Order revenue',
                'callback' => function ($order) {
                    return $this->toNumber($order->order_revenue, $order->currency_symbol);
                },
            ],
            [
                'name' => 'Currency',
                'callback' => function ($order) {
                    return $order->currency_symbol;
                },
            ],
            [
                'name' => 'Create Time',
                'callback' => function ($order) use ($offset) {
                    if (!empty($order->created_at)) {
                        return Carbon::createFromFormat('Y-m-d H:i:s', $order->created_at,
                            'UTC')->addMinutes($offset)->format('H:i:s d/m/Y');
                    }
                    return null;
                }
            ],

            [
                'name' => 'Status',
                'callback' => function ($order) {
                    $mp = [
                        0 => 'Pending',
                        1 => 'Fail',
                        2 => 'Ready for collection',
                        3 => 'Ready for collection',
                        4 => 'Completed',
                        5 => 'Home Delivery',
                        6 => 'Refund',
                    ];
                    return $mp[$order->status];
                },
            ],
        ];
        $titles = [];
        foreach ($map as $field) {
            $titles[] = $field['name'];
        }
        $return = [$titles];
        foreach ($data as $order) {
            $temp = [];
            foreach ($map as $field) {
                $temp[] = $field['callback']($order);
            }
            $return[] = $temp;
        }
        $callback = function () use ($return) {
            $file = fopen('php://output', 'w');
            foreach ($return as $column) {
                fputcsv($file, $column);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function print(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        return view('order', ['order' => $order]);
    }
}
