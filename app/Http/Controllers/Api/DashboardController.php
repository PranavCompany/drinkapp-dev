<?php

namespace App\Http\Controllers\API;
use App\Location;
use App\Order;
use App\Support\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends ApiController
{

    public function income(Request $request)
    {
        $selected_option = $request->input('selected_option');
        $total = DB::table('orders')
            ->select(DB::raw('locations.id, users.id, locations.name as location,
                                    orders.currency,
                                        users.name as retailer, round(sum('.Order::orderRevenueQuery().'), 2) as total'))
            ->join('locations', 'orders.location_id', 'locations.id')
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('users as u', 'orders.user_id', '=', 'u.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('orders.deleted_at');
        Response::addTimeCondition('orders.created_at', $selected_option,$total, $request->input('offset'));
        $total->where('orders.paid', '=', true)->whereNull('orders.deleted_at')
            ->where('orders.status', '!=', 6)
            ->where('locations.user_id', '=', $request->user()->id)
            ->groupBy(['locations.id', 'users.id', 'orders.currency']);
        if (isset($request->sortOrder) && isset($request->sortKey)
            && !empty($request->sortKey) && !empty($request->sortOrder)) {
            $total->orderBy($request->sortKey, $request->sortOrder);
        } else {
            $total->orderBy('total', 'desc');
        }
        return $this->response->json($total->paginate(5));
    }

    public function customers(Request $request)
    {
        $selected_option = $request->input('selected_option');
        $total = DB::table('orders')
            ->select(DB::raw(' locations.id, users.id, locations.name as location,
                                        users.name as retailer, count(distinct orders.user_id, orders.location_id) as total'))
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('users as u', 'orders.user_id', '=', 'u.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->where('locations.user_id', '=', $request->user()->id)
            ->groupBy(['locations.id', 'users.id']);
        Response::addTimeCondition('orders.created_at', $selected_option,$total, $request->input('offset'));
        if (isset($request->sortOrder) && isset($request->sortKey)
            && !empty($request->sortKey) && !empty($request->sortOrder)) {
            $total->orderBy($request->sortKey, $request->sortOrder);
        } else {
            $total->orderBy('total', 'desc');
        }
        return $this->response->json($total->paginate(5));
    }
    public function new_customers(Request $request)
    {
        $selected_option = $request->input('selected_option');
        $total = DB::table('orders')
            ->select(DB::raw(' locations.id, users.id, locations.name as location,
                                        users.name as retailer, count(distinct orders.user_id, orders.location_id) as total'))
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('users as u', 'orders.user_id', '=', 'u.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->where('locations.user_id', '=', $request->user()->id)
            ->groupBy(['locations.id', 'users.id']);
        Response::addTimeCondition('u.created_at', $selected_option,$total, $request->input('offset'));
        if (isset($request->sortOrder) && isset($request->sortKey)
            && !empty($request->sortKey) && !empty($request->sortOrder)) {
            $total->orderBy($request->sortKey, $request->sortOrder);
        } else {
            $total->orderBy('total', 'desc');
        }
        return $this->response->json($total->paginate(5));
    }
    public function orders(Request $request)
    {
        $selected_option = $request->input('selected_option');
        $total = DB::table('orders')
            ->select(DB::raw('locations.id, users.id, locations.name as location,
                                        users.name as retailer, count(*) as total'))
            ->join('locations', 'orders.location_id', 'locations.id')
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('users as u', 'orders.user_id', '=', 'u.id')
            ->whereRaw('orders.retailer_id='.$request->user()->id)
            ->where('orders.paid', '=', true)->whereNull('orders.deleted_at')
            ->where('orders.status', '!=', 6)
            ->groupBy(['locations.id', 'users.id']);
            Response::addTimeCondition('orders.created_at', $selected_option,$total, $request->input('offset'));
        if (isset($request->sortOrder) && isset($request->sortKey)
            && !empty($request->sortKey) && !empty($request->sortOrder)) {
            $total->orderBy($request->sortKey, $request->sortOrder);
        } else {
            $total->orderBy('total', 'desc');
        }
        return $this->response->json($total->paginate(5));
    }

    public function locations(Request $request)
    {
        $selected_option = $request->input('selected_option');
        $total = DB::table('orders')
            ->select(DB::raw('locations.id, users.id, locations.name as location,
                                        users.name as retailer, count(*) as total'))
            ->whereRaw('orders.retailer_id='.$request->user()->id);
            Response::addTimeCondition('orders.created_at', $selected_option,$total, $request->input('offset'));
        if (isset($request->sortOrder) && isset($request->sortKey)
            && !empty($request->sortKey) && !empty($request->sortOrder)) {
            $total->orderBy($request->sortKey, $request->sortOrder);
        } else {
            $total->orderBy('total', 'desc');
        }
        return $this->response->json($total->paginate(5));
    }

    public function orders_chart(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('orders')
            ->select(DB::raw('locations.name as category, count(orders.id) as data'))
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('orders.deleted_at')
            ->where('orders.paid', '=', true)->whereNull('orders.deleted_at')
            ->where('orders.status', '!=', 6)
            ->where('orders.retailer_id', '=', $request->user()->id)
            ->groupBy(['locations.id'])
            ->orderBy('data', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $data = [];
        $total = 0;
        foreach ($array as $key => $value) {
            $data[] = [
                'x' => $value->category,
                'y' => $value->data,
            ];
            $total += (int)$value->data;
        }
        return $this->response->json([
            'data' => [['data' => $data]],
            'total' => 'Total: '.$total
        ]);
    }
    public function unique_customers_chart(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $total = DB::table('orders')
            ->select(DB::raw(' locations.id, users.id, locations.name as location,
                                        users.name as retailer, count(distinct orders.user_id, orders.location_id) as total'))
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('users as u', 'orders.user_id', '=', 'u.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->where('locations.user_id', '=', $request->user()->id)
            ->orderBy('total', 'asc')
            ->groupBy(['locations.id', 'users.id']);
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $total, $request->input('offset'));
        $array = $total->get();
        $data = [];
        foreach ($array as $key => $value) {
            $data[] = [
                'x' => $value->location,
                'y' => $value->total,
            ];
        }
        return $this->response->json([['data' => $data]]);
    }
    public function new_customers_chart(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $total = DB::table('orders')
            ->select(DB::raw(' locations.id, users.id, locations.name as location,
                                        users.name as retailer, count(distinct orders.user_id, orders.location_id) as total'))
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('users as u', 'orders.user_id', '=', 'u.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->where('locations.user_id', '=', $request->user()->id)
            ->orderBy('total', 'asc')
            ->groupBy(['locations.id', 'users.id']);
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $total, $request->input('offset'));
        $data = [];
        foreach ($total->get() as $key => $value) {
            $data[] = [
                'x' => $value->location,
                'y' => $value->total,
            ];
        }
        return $this->response->json([['data' => $data]]);
    }

    private function prepare_data($array) {
        $return = [];
        $temp_data = [];
        $user = auth()->user();
        $currenies = array_map(function ($a) {
            return strtoupper($a);
        }, $user->currencies_check);
        $total = [];
        $empty_data = [];
        foreach ($currenies as $currency) {
            $total[$currency] = 0;
            $empty_data[] = [
                'name' => $currency,
                'data' => [],
            ];
        }
        foreach ($array as $key => $value) {
            foreach ($currenies as $currency) {
                if (!isset($temp_data[$currency][$value->id])) {
                    $temp_data[$currency][$value->id] = [
                        'y' => 0,
                        'x' => $value->location,
                    ];
                }
            }
            $temp_data[strtoupper($value->currency)][$value->id] = [
                'y' => $value->income,
                'x' => $value->location,
            ];
            $total[strtoupper($value->currency)] += (float)$value->income;
        }
        foreach ($temp_data as $currency => $value) {
            $return[] = [
                'name' => $currency,
                'data' => array_values($value)
            ];
        }
        if (!count($temp_data)) {
            $return = $empty_data;
        };
        $total_str = "Total: ";
        foreach (array_keys($total) as $index => $key) {
            if ($index !== 0) {
                $total_str .= ', ';
            }
            $total_str .= Location::currencyToSymbol(strtolower($key)).$total[$key];
        }
        return ['data' => $return, 'total' => $total_str];
    }
    public function income_location(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('orders')
            ->select(DB::raw(' locations.name as location, locations.id, orders.currency, round(sum('.Order::orderRevenueQuery().'), 2) as income'))
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('locations', 'locations.id', '=', 'orders.location_id')
            ->where('orders.paid', '=', true)
            ->where('orders.status', '!=', 6)
            ->whereNull('orders.deleted_at')
            ->where('orders.retailer_id', '=', $request->user()->id)
            ->groupBy(['locations.id', 'orders.currency'])
            ->orderBy('locations.id', 'asc')
            ->orderBy('income', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        return $this->response->json($this->prepare_data($array));
    }
    public function tips(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('orders')
            ->select(DB::raw(' locations.name as location, locations.id, orders.currency, sum(ifnull(orders.tips, 0)) as income'))
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('locations', 'locations.id', '=', 'orders.location_id')
            ->where('orders.paid', '=', true)->whereNull('orders.deleted_at')
            ->where('orders.status', '!=', 6)
            ->where('orders.retailer_id', '=', $request->user()->id)
            ->groupBy(['locations.id', 'orders.currency'])
            ->orderBy('locations.id', 'asc')
            ->orderBy('income', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        return $this->response->json($this->prepare_data($array));
    }

    public function age(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $condition = "year(now())-users.yob";
        $locations = DB::table('orders')
            ->select(DB::raw('locations.id, locations.name as category, count(distinct orders.user_id, orders.location_id) as total,
             sum(if ('.$condition.' between 18 and 25, 1, 0 )) as `18-25`,
             sum(if ('.$condition.' between 26 and 35, 1, 0 )) as `26-35`,
             sum(if ('.$condition.' between 36 and 45, 1, 0 )) as `36-45`,
             sum(if ('.$condition.' > 45, 1, 0 )) as `45+`'
            ))
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->where('orders.retailer_id', '=', $request->user()->id)
            ->whereNull('orders.deleted_at')
            ->groupBy(['locations.id'])
            ->orderBy('locations.id', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $dict = ['18-25', '26-35', '36-45', '45+'];
        foreach ($dict as $val) {
            $data[$val] = ['name' => $val];
        }
        if (!count($array)) {
            foreach ($dict as $val) {
                $data[$val]['data'] = [];
            }
        } else {
            foreach ($array as $key => $value) {
                foreach ($dict as $val) {
                    $value = (array)$value;
                    $data[$val]['data'][] = [
                        'x' => $value['category'],
                        'y' => $value[$val],
                    ];
                }
            }
        }
        return $this->response->json(array_values($data));
    }
    public function gender(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('orders')
            ->select(DB::raw('locations.id, locations.name as category, sum(if (users.gender = \'male\', 1, 0 )) as male,
             count(distinct orders.user_id, orders.location_id) as total,
             sum(if (users.gender = \'female\', 1, 0 )) as female'))
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->whereNull('orders.deleted_at')
            ->where('orders.retailer_id', '=', $request->user()->id)
            ->groupBy(['locations.id'])
            ->orderBy('locations.id', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $data = [
            'male' => [
                'name' => 'male',
            ],
            'female' => [
                'name' => 'female',
            ]
        ];
        if (!count($array)) {
            $data['male']['data'] = [];
            $data['female']['data'] = [];
        } else {
            foreach ($array as $key => $value) {
                $data['male']['data'][] = [
                    'x' => $value->category,
                    'y' => $value->male,
                ];
                $data['female']['data'][] = [
                    'x' => $value->category,
                    'y' => $value->female,
                ];
            }
        }
        return $this->response->json(array_values($data));
    }
    public function age_retailer(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $condition = "year(now())-users.yob";
        $locations = DB::table('orders')
            ->select(DB::raw('distinct u.id, u.name as category,
            count(distinct orders.user_id, orders.retailer_id) as total,
                 sum(if ('.$condition.' between 18 and 25, 1, 0 )) as `18-25`,
                 sum(if ('.$condition.' between 26 and 35, 1, 0 )) as `26-35`,
                 sum(if ('.$condition.' between 36 and 45, 1, 0 )) as `36-45`,
                 sum(if ('.$condition.' > 45, 1, 0 )) as `45+`'
            ))
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('users as u', 'orders.retailer_id', '=', 'u.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->where('orders.retailer_id', '=', $request->user()->id)
            ->groupBy(['u.id'])
            ->orderBy('u.id', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $dict = ['18-25', '26-35', '36-45', '45+'];
        $return = [];
        if (!count($array)) {
            return $this->response->json([]);
        }
        foreach ($dict as $val) {
            $return[] = (int)$array[0]->$val;
        }
        return $this->response->json($return);

    }
    public function gender_retailer(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('orders')
            ->select(DB::raw('distinct u.id, u.name as category, sum(if (users.gender = \'male\', 1, 0 )) as male,
            count(distinct orders.user_id, orders.retailer_id) as total,
             sum(if (users.gender = \'female\', 1, 0 )) as female'))
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('users as u', 'orders.retailer_id', '=', 'u.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->where('orders.retailer_id', '=', $request->user()->id)
            ->groupBy(['u.id'])
            ->orderBy('u.id', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        if (count($array)) {
            return $this->response->json([(int)$array[0]->male, (int)$array[0]->female]);
        } else {
            return $this->response->json([]);
        }
    }
}
