<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\ApiController;

use Storage;
use App\Image;

class ImageController extends ApiController
{
    public function upload(Request $request) {
    	$this->validate($request, [
            'image' => 'required|image'
        ]);

 		$path = $request->file('image')->store('images');
    	$url = '/public_uploads/' . $path;
    	return $this->response->json(['url' => $url]);
    }
}
