<?php

namespace App\Http\Controllers\API;

use App\Image;
use App\Location;
use App\Order;
use App\Repositories\LocationRepository;
use App\Scopes\OrderOwnerScope;
use App\Scopes\OwnerScope;
use App\Transformers\OrderTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationController extends ApiController
{
    protected $location;

    public function __construct(LocationRepository $location)
    {
        parent::__construct();

        $this->location = $location;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response->collection($this->location->pageWithRequest($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => ['nullable', 'regex:/^\+?(?:[0-9] ?){6,14}[0-9]$/'],
            'address' => 'nullable|string|max:255',
            'city' => 'nullable|string|max:255',
            'state' => 'nullable|string|max:255',
            'country' => 'nullable|string|max:255',
            'location_type' => 'nullable|string|max:255',
            'latitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
        ]);

        $data = array_merge($request->only('name', 'description', 'phone',
            'location_type',
            'tips_type',
            'tips_list',
            'country', 'state', 'city', 'address', 'latitude', 'longitude', 'currency', 'to_table', 'collection'), [
            'user_id' => $request->user()->id,
            'status' => 1
        ]);

        $location = $this->location->store($data);

        if ($request->has('image') && count($request->input('image')) > 0) {
            foreach ($request->input('image') as $image) {
                $location->images()->save(new Image(['url' => $image['url']]));
            }
        }

        return $this->response->item($location);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->response->item($this->location->getById($id));
    }

    private function setTimeFields($request, &$location, $start, $end)
    {
        if ($request->input($start) && $request->input($end)) {
            $time_start = strtotime('2020-01-01 '.$request->input($start).$request->input('offset').' minutes');
            $time_end = strtotime('2020-01-01 '.$request->input($end).$request->input('offset').' minutes');
            $location->$start = date('H:i', $time_start);
            $location->$end =  date('H:i', $time_end);
        } else if ((!$request->input($start) && $request->input($end)) || ($request->input($start) && !$request->input($end))) {
            return false;
        } else {
            $location->$start = '';
            $location->$end = '';
        }
        return true;
    }

    public function settings(Request $request, $id)
    {
        $this->validate($request, [
            'time_start' => ['nullable', 'regex:/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/'],
            'time_end' => ['nullable', 'regex:/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/'],
            'collection_time_start' => ['nullable', 'regex:/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/'],
            'collection_time_end' => ['nullable', 'regex:/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/'],
            'home_delivery_time_start' => ['nullable', 'regex:/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/'],
            'home_delivery_time_end' => ['nullable', 'regex:/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/'],
        ]);

        $location = $this->location->getById($id);
        if ($location) {
            if (!$this->setTimeFields($request, $location, 'time_start', 'time_end')
                || !$this->setTimeFields($request, $location, 'collection_time_start', 'collection_time_end')
                || !$this->setTimeFields($request, $location, 'home_delivery_time_start', 'home_delivery_time_end')) {
                return $this->response->withBadRequest('Fill in start and end time');
            }
            $location->to_table = $request->input('to_table');
            $location->collection = $request->input('collection');
            $location->to_home = $request->input('to_home');
            if ($request->has('categories')) {
                $categories = [];
                foreach ($request->input('categories') as $category) {
                    if (!empty($category['time_start']) && !empty($category['time_end'])) {
                        if (is_array($category['time_start']) && is_array($category['time_end'])) {
                            if (!empty($category['time_start']['HH']) && !empty($category['time_start']['mm'])
                                && !empty($category['time_end']['HH']) && !empty($category['time_end']['mm'])
                            ) {
                                $time_start = strtotime('2020-01-01 '.$category['time_start']['HH'].':'.$category['time_start']['mm'].$request->input('offset').' minutes');
                                $time_end = strtotime('2020-01-01 '.$category['time_end']['HH'].':'.$category['time_end']['mm'].$request->input('offset').' minutes');
                                $categories[$category['id']] = [
                                    'time_start' => date('H:i', $time_start),
                                    'time_end' => date('H:i', $time_end),
                                ];
                            } else {
                                return $this->response->withBadRequest('Incorrect date given');
                            }
                        } else {
                            if (!empty($category['time_start']) && !empty($category['time_start'])) {
                                $time_start = strtotime('2020-01-01 '.$category['time_start'].$request->input('offset').' minutes');
                                $time_end = strtotime('2020-01-01 '.$category['time_end'].$request->input('offset').' minutes');
                                $categories[$category['id']] = [
                                    'time_start' => date('H:i', $time_start),
                                    'time_end' => date('H:i', $time_end),
                                ];
                            } else {
                                return $this->response->withBadRequest('Incorrect date given');
                            }
                        }

                    } else {
                        $categories[$category['id']] = [
                            'time_start' => null,
                            'time_end' => null,
                        ];
                    }
                }
                $location->categories()->syncWithoutDetaching($categories);
            }
            if ($location->to_home) {
                $location->home_delivery_minimum_order = $request->input('home_delivery_minimum_order');
                $settings = $request->input('home_delivery_radius_settings');
                if ($settings) {
                    $settings = array_map(function ($a) {
                        return ['price' => (float)$a['price'], 'radius' => (float)$a['radius']];
                    }, $settings);
                    usort($settings, function ($a, $b) {
                        return (float)$a['radius'] - (float)$b['radius'];
                    });
                }
                $radiuses = [];
                $location->home_delivery_radius_settings = $settings;
                foreach ($location->home_delivery_radius_settings as $value) {
                    $radius = (float)$value['radius'];
                    if (in_array($radius, $radiuses)) {
                        return $this->response->withBadRequest("2 radiuses with same value");
                    } else {
                        $radiuses[] = $radius;
                    }
                    if ((float)$value['price'] < 0) {
                        return $this->response->withBadRequest('Delivery price should be greater then 0');
                    } elseif ((float)$value['radius'] < 0 || (float)$value['radius'] > 100) {
                        return $this->response->withBadRequest('Delivery radius should be greater then 0 and lower then 100');
                    }
                }

                if ((float)$location->home_delivery_minimum_order < 0) {
                    return $this->response->withBadRequest('Minimum order should be greater then 0');
                }
                $location->home_delivery_days = $request->input('home_delivery_days');
            }
            $location->save();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => ['nullable', 'regex:/^\+?(?:[0-9] ?){6,14}[0-9]$/'],
            'address' => 'nullable|string|max:255',
            'city' => 'nullable|string|max:255',
            'state' => 'nullable|string|max:255',
            'country' => 'nullable|string|max:255',
            'latitude' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
        ]);

        $updated_location = $this->location->update($id, $request->only('name', 'description', 'phone',
            'location_type',
            'tips_type',
            'tips_list',
            'country', 'state', 'city', 'address', 'latitude', 'longitude', 'currency', 'to_table', 'collection'));

        if ($request->has('image') && $request->input('image') != '' && $request->input('image_old') == false) {
            $ids = [];
            foreach ($request->input('image') as $image) {
                $image_url = $image['url'];
                if (isset($image_url) && $image_url != '' && !isset($image['id'])) {
                    $image_url = str_replace(env('APP_URL'), '', $image_url);
                    $ids[] = $updated_location->images()->save(new Image(['url' => $image_url]))->id;
                }
                if (isset($image['id'])) {
                    $ids[] = $image['id'];
                }
            }
            if (!empty($ids)) {
                $updated_location->images()->whereNotIn('id', $ids)->delete();
            }
        }

        return $this->response->item($updated_location);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->location->destroy($id);
        return $this->response->withNoContent();
    }

    /**
     * Get menu categories & items of location
     *
     */
    public function menu(Request $request, $id)
    {
        $user = $request->user();
        if ($user->hasRole('admin')) {
            $location = Location::withoutGlobalScope(OwnerScope::class)->withTrashed()->findOrFail($id);
            return $this->response->json(['categories' => $location->categories()->withoutGlobalScope(OwnerScope::class)->get(),
                'items' => $location->items()->withoutGlobalScope(OwnerScope::class)->get()]);
        } else {
            $location = $this->location->getById($id);
            if ($location) {
                return $this->response->json(['categories' => $location->categories, 'items' => $location->items]);
            }
        }
        return  $this->response->withBadRequest();
    }

    /**
     * Update menu categories & items of location
     *
     */
    public function update_menu(Request $request, $id)
    {
        $location = $this->location->getById($id);
        if ($location) {
            $categories = $request->input('categories');
            $items = $request->input('items');
            $out_of_stock = $request->input('out_of_stock');
            $special_offers = $request->input('special_offers');
            $location->categories()->detach();
            $location->categories()->attach($categories);

            $location->items()->detach();
            $items_list = [];
            foreach ($items as $item) {
                $items_list[$item] = [
                    'out_of_stock' => (int)in_array($item, $out_of_stock),
                    'special_offer' => (int)in_array($item, $special_offers),
                ];
            }
            $location->items()->attach($items_list);

            return $this->response->withNoContent();
        } else {
            $this->response->withBadRequest();
        }
    }

    public function homeDelivery(Request $request, $id)
    {
        $location = Location::find($id);
        if ($location && $request->has('value')) {
            $location->to_home = (bool)$request->input('value');
            $location->save();
        }
    }


    public function homeDeliveryGet(Request $request, $id)
    {
        $location = Location::find($id);
        if ($location) {
            return $this->response->item($location);
        } else {
            return $this->response->withBadRequest();
        }
    }

    /**
     * Get all categories of location
     *
     */
    public function categories(Request $request, $id)
    {
        $location = $this->location->getById($id);
        if ($location) {
            return $this->response->collection($location->categories);
        } else {
            $this->response->withBadRequest();
        }
    }

    /**
     * Get all items of location
     *
     */
    public function items(Request $request, $id)
    {
        $location = $this->location->getById($id);
        if ($location) {
            return $this->response->collection($location->items);
        } else {
            $this->response->withBadRequest();
        }
    }

    /**
     * Get all orders of location
     *
     */
    public function orders(Request $request, $id)
    {
        $location = $this->location->getById($id);
        if ($location) {
            return $this->response->json($location->ordersAtWork());
        } else {
            $this->response->withBadRequest();
        }
    }


    /**
     * Get all orders of location
     *
     */
    public function orders_to_print(Request $request, $id)
    {
        $location = $this->location->getById($id);
        if ($location) {
            return $this->response->json($location->ordersToPrint());
        } else {
            $this->response->withBadRequest();
        }
    }

    /**
     * Get all orders of location
     *
     */
    public function orders_list(Request $request, $id)
    {
        $location = Location::withoutGlobalScope(OwnerScope::class)->findOrFail($id);
        $user = $location->user;
        if ($request->user()->id == $user->id || $request->user()->hasRole('admin')) {
            $keyword = $request->input('keyword');
            $date_start = $request->input('dateStart');
            $date_end = $request->input('dateEnd');
            $orders = Order::withoutGlobalScope(OrderOwnerScope::class)->withoutGlobalScope(OwnerScope::class)
                ->where('retailer_id', '=', $user->id)->where('location_id', '=', $id);
            if (isset($request->sortOrder) && isset($request->sortKey)
                && !empty($request->sortKey) && !empty($request->sortOrder)) {
                $orders->orderBy($request->sortKey, $request->sortOrder);
            } else {
                $orders->orderBy('created_at', 'desc');
            }
            $orders->when($keyword, function ($query) use ($keyword) {
                $query->where('orders.id', '=', $keyword);
            });
            $orders->when(!empty($date_start) && !empty($date_end), function ($query) use ($date_start, $date_end) {
                $query->whereBetween('created_at', [$date_start, $date_end]);
            });
            return $this->response->collection($orders->paginate(10), new OrderTransformer);
        } else {
            return $this->response->withBadRequest();
        }
    }
    public function setPrinter(Request $request, $id)
    {
        $location = Location::find($id);
        $location->printer = $request->input('printer');
        $location->save();
    }
    public function options(Request $request)
    {
        $options = DB::table('locations')
            ->select(DB::raw('locations.id, locations.printer, locations.name, to_home, count(orders.id) as total_orders'))
            ->leftJoin('orders', function ($join) {
                $join->on('orders.location_id', '=', 'locations.id')
                    ->whereIn('orders.status', [0, 2, 3])
                    ->where('orders.paid', '=', true)
                    ->whereNull('orders.deleted_at');
            })->whereNull('locations.deleted_at')
            ->where('locations.user_id', '=', $request->user()->id)
            ->groupBy('locations.id')->get();
        return $this->response->json($options);
    }
}
