<?php

namespace App\Http\Controllers\API\Settings;

use App\Http\Controllers\API\ApiController;
use App\Location;
use Illuminate\Http\Request;

class ProfileController extends ApiController
{
    public function profile(Request $request)
    {
        return $this->response->item($request->user());
    }

    /**
     * Update the user's profile information.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = $request->user();

        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);

        return tap($user)->update($request->only('name'));
    }

    public function currencies(Request $request)
    {
        $user = $request->user();
        $currencies = $request->input('currencies');
        if (!empty($currencies)) {
            $currencies = array_map(function ($a) {
                return strtolower($a);
            }, $currencies);
        }
        $resp = Location::whereNotIn('currency', $currencies)->get();
        $exists = [];
        foreach ($resp as $location) {
            if (!in_array($location->currency, $exists)) {
                $exists[] = $location->currency;
            }
        }
        if (!empty($exists)) {
            return $this->response->withBadRequest(['You have location(s) with currencies: ' . implode(',',
                    array_map(function ($a) {
                        return strtoupper($a);
                    }, $exists))]);
        }
        $data = tap($user)->update(['currencies' => $currencies]);
        $data->currencies = $data->currencies_check;
        return $data;
    }

}
