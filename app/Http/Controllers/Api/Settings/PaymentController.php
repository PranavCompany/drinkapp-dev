<?php

namespace App\Http\Controllers\API\Settings;

use App\Http\Controllers\API\ApiController;
use App\Tools\IP;
use Illuminate\Http\Request;
use Stripe\Account;
use Stripe\Exception\InvalidRequestException;
use Stripe\File;
use Stripe\Stripe;
use Stripe\AccountLink;

class PaymentController extends ApiController
{
    /**
     * Get stripe connect account
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $user = $request->user();
        if ($user->custom_account_disconnected) {
            Stripe::setApiKey(env('STRIPE_SECRET'));
            if ($user->stripe_standard_account) {
                $account = Account::retrieve($user->stripe_standard_account);
            } else {
                $account = [];
            }
            return $this->response->json(['account' => $account]);
        } elseif ($user->stripe_acc_id && $user->stripe_acc_id != '') {
            Stripe::setApiKey(env('STRIPE_SECRET'));
            $account = Account::retrieve($user->stripe_acc_id);
            $persons = Account::allPersons($user->stripe_acc_id);
            return $this->response->json(['account' => $account, 'persons' => $persons]);
        }
        else {
            return $this->response->withNoContent();
        }
    }

    public function getLink(Request $request)
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = $request->user();
        if ($user->stripe_acc_id && $user->stripe_acc_id != '') {
            $link = AccountLink::create([
                'account' => $user->stripe_acc_id,
                'failure_url' => env('APP_URL').'/settings/payment',
                'success_url' => env('APP_URL').'/settings/payment',
                'type' => 'custom_account_verification',
            ]);
            return $this->response->json(['user' => $user, 'link' => $link]);
        }
    }

    public function connectStandardAccount(Request $request) {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = $request->user();
        if (empty($user->stripe_standard_account)) {
            $account = Account::create([
                'type' => 'standard',
                'metadata' => [
                    'user_id' => $user->id,
                    'env' => env('APP_ENV'),
                ],
            ]);
            $user->stripe_standard_account = $account->id;
            $user->custom_account_disconnected = 1;
            $user->save();
        }
        $link = AccountLink::create([
            'account' => $user->stripe_standard_account,
            'failure_url' => env('APP_URL').'/settings/payment',
            'success_url' => env('APP_URL').'/settings/payment',
            'type' => 'account_onboarding',
        ]);
        return $this->response->json(['user' => $user, 'link' => $link]);
    }
    public function disconnect(Request $request) {
        $user = $request->user();
        $user->custom_account_disconnected = 1;
        $user->stripe_acc_verified = 1;
        $user->save();
        return $this->response->json(['user' => $user]);
    }

    /**
     * Create stripe connect account
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'business_type' => 'required|string'
        ]);

        $user = $request->user();

        Stripe::setApiKey(env('STRIPE_SECRET'));

        try {
            $ip = new IP($request);
            info("IP: " . $ip->get());

            $account_data = array_merge($request->all(), [
                "type" => "custom",
                "email" => $user->email,
                "country" => $request->input('company.address.country'),
                "tos_acceptance" => [
                    "date" => time(),
                    "ip" => $ip->get()
                ],
                'requested_capabilities' => [
                    'card_payments',
                    'transfers',
                ],
                'business_profile' => [
                    'mcc' => 5813,
                    'url' => $request->input('business_profile.url'),
                ],
                'metadata' => [
                    'user_id' => $user->id,
                    'env' => env('APP_ENV'),
                ],
            ]);
            if (!empty($user->stripe_acc_id)) {
                $account = Account::update($user->stripe_acc_id, array_merge($request->all()));
            } else {
                $account = Account::create($account_data);
                $user->stripe_acc_id = $account['id'];
                $user->stripe_acc_verified = 1;
                $user->save();
            }
            return $this->response->json(['user' => $user, 'account' => $account]);
        }
        catch(\Stripe\Error\InvalidRequest $e) {
            info('Stripe InvalidRequest');
            return $this->response->withBadRequest($e->getMessage());
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            return $this->response->withBadRequest($e->getMessage());
        }
        catch(Exception $e) {
            info('Exception');
            return $this->response->withBadRequest($e->getMessage());
        }
    }
    private static function addFiles($request, &$person) {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $fp = fopen(substr($request->input('images.front'), 1), 'r');
        $front = File::create([
            'purpose' => 'identity_document',
            'file' => $fp
        ]);
        fclose($fp);
        $fp = fopen(substr($request->input('images.back'), 1), 'r');
        $back = File::create([
            'purpose' => 'identity_document',
            'file' => $fp
        ]);
        fclose($fp);
        $person['verification'] = [
            'document' => [
                'front' => $front->id,
                'back' => $back->id,
            ]
        ];
    }
    public function update(Request $request, $type)
    {
        $user = $request->user();
        Stripe::setApiKey(env('STRIPE_SECRET'));

        try {
            if ($type === 'bank') {
                $currency = $request->input('external_account.currency');
                $data = $request->all('external_account');
                $acc_list = Account::allExternalAccounts($user->stripe_acc_id, ['object' => 'bank_account']);
                $bank_id = '';
                foreach ($acc_list['data'] as $ext_acc) {
                    if($ext_acc['currency'] == $currency) {
                        $bank_id = $ext_acc['id'];
                        break;
                    }
                }
                $data['external_account']['default_for_currency'] = true;
                $account = Account::createExternalAccount($user->stripe_acc_id, $data);
                if (!empty($bank_id)) {
                    Account::deleteExternalAccount($user->stripe_acc_id, $bank_id);
                }
                return $this->response->json(['user' => $user, 'account' => $account]);
            }
            if ($type === 'person') {
                if (!isset($request->input('person')['id'])) {
                    $person = $request->input('person');
                    $person['metadata'] = ['env' => env('APP_ENV')];
                    self::addFiles($request, $person);
                    Account::createPerson($user->stripe_acc_id, $person);
                } else {
                    $person = [
                        'address' => $request->input('person.address'),
                        'phone' => $request->input('person.phone'),
                    ];
                    $stripe_person = Account::retrievePerson($user->stripe_acc_id, $request->input('person.id'));
                    if ($stripe_person['verification']->status === 'unverified') {
                        if (!empty($stripe_person['requirements']->eventually_due)
                            && in_array('verification.document', $stripe_person['requirements']->eventually_due)) {
                            self::addFiles($request, $person);
                        }
                    }
                    Account::updatePerson($user->stripe_acc_id, $request->input('person.id'), $person);
                }
            }
        }
        catch(\Stripe\Error\InvalidRequest $e) {
            info('Stripe InvalidRequest');
            return $this->response->withBadRequest($e->getMessage());
        }
        catch(\Stripe\Exception\InvalidRequestException $e) {
            info('Stripe InvalidRequest');
            return $this->response->withBadRequest($e->getMessage());
        }
        catch(Exception $e) {
            info('Exception');
            return $this->response->withBadRequest($e->getMessage());
        }
    }
}
