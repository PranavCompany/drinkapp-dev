<?php

namespace App\Http\Controllers\API;

use App\Item;
use App\Repositories\AddonsRepository;
use App\Repositories\ItemRepository;
use Illuminate\Http\Request;

class AddonController extends ApiController
{
    protected $addon;
    protected $item;

    public function __construct(AddonsRepository $addon, ItemRepository $item)
    {
        parent::__construct();

        $this->addon = $addon;
        $this->item = $addon;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response->collection($this->addon->pageWithRequest($request));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->response->collection($this->addon->getList());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);
        $data = array_merge($request->only('name'), [
            'price_eur' => $request->price_eur ?? 0,
            'price_usd' => $request->price_usd ?? 0,
            'price_aed' => $request->price_aed ?? 0,
            'price_gbp' => $request->price_gbp ?? 0,
            'user_id' => $request->user()->id
        ]);
        $addon = $this->addon->store($data);

        return $this->response->item($addon);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $addon = $this->addon->getById($id);

        if ($request->user()->can('update', $addon)) {
            return $this->response->item($addon);
        } else {
            return $this->response->withBadRequest('You have no permission to edit this addon');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);

        $updated_addon = $this->addon->update($id,
            array_merge($request->only('name'), [
                'price_eur' => $request->price_eur ?? 0,
                'price_usd' => $request->price_usd ?? 0,
                'price_aed' => $request->price_aed ?? 0,
                'price_gbp' => $request->price_gbp ?? 0
            ]));

        return $this->response->item($updated_addon);
    }


    /**
     * Update item addons.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update_item_addons(Request $request, $id)
    {
        $item = Item::find($id);
        $item->addons()->sync($request->input('addons'));
        if (!$item) {
            return $this->response->withBadRequest('Item not foind');
        }
        return $this->response->item($item);
    }
}
