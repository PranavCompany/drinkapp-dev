<?php

namespace App\Http\Controllers\API;

use App\Item;
use App\Repositories\ItemRepository;
use App\Repositories\OptionsRepository;
use App\Transformers\OptionTransformer;
use Illuminate\Http\Request;

class OptionController extends ApiController
{
    protected $option;
    protected $item;

    public function __construct(OptionsRepository $option, ItemRepository $item)
    {
        parent::__construct();

        $this->option = $option;
        $this->item = $option;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response->collection($this->option->pageWithRequest($request));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->response->collection($this->option->getList());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'values' => 'required',
        ]);

        $data = array_merge($request->only('name', 'values'), [
            'user_id'   => $request->user()->id
        ]);

        $option = $this->option->store($data);

        return $this->response->item($option, new OptionTransformer());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $option = $this->option->getById($id);

        if ($request->user()->can('update', $option)) {
            return $this->response->item($option);
        }
        else {
            return $this->response->withBadRequest('You have no permission to edit this option');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'values' => 'required',
        ]);

        $updated_option = $this->option->update($id, $request->only('name', 'values'));

        return $this->response->item($updated_option, new OptionTransformer());
    }

    /**
     * Update item options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_item_options(Request $request, $id)
    {
        $item = Item::find($id);
        $item->options()->sync($request->input('options'));
        if (!$item) {
            return $this->response->withBadRequest('Item not foind');
        }
        return $this->response->item($item);
    }
}
