<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\API\ApiController;
use App\Item;
use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;

class ItemController extends ApiController
{
    /**
     *  @OA\Tag(
     *      name="Location",
     *      description=""
     *  )
     */


    public function __construct()
    {
        parent::__construct();
    }

    /**
	 * @OA\Post(
	 *     path="/api/v1/items",
	 *	   summary="Get Items prices",
	 *     tags={"Location"},
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="location_id",
     *                      type="integer",
     *                      description="",
     *                  ),
     *                  @OA\Property(
     *                      property="items",
     *                      type="string",
     *                      description="[1, 2, 3, ...]",
     *                  ),
     *              )
     *          )
     *      ),
     *      @OA\Response(response="200", description="{data: []}")
	 * )
	 */
    public function index(Request $request)
    {
        $items_ids = $request->input('items');
        if (!$request->input('location_id')) {
            return $this->response->withBadRequest("Provide location_id");
        }
        if (!empty($items_ids)) {
            $items = Item::select('items.id', 'price_eur', 'price_gbp', 'price_aed', 'price_usd', 'out_of_stock',
                DB::raw('
                if(locations.currency = "gbp", price_gbp,
                 if(locations.currency = "aed", price_aed,
                 if(locations.currency = "usd", price_usd,
                  price_eur))) as price'),
                Item::canUseDiscount()
            )->join('location_has_items', 'location_has_items.item_id', '=', 'items.id')
                ->join('locations', 'locations.id', '=', 'location_has_items.location_id')
                ->whereIn('items.id', $items_ids)
                ->where('location_has_items.location_id', $request->input('location_id'))
                ->get();
        } else {
            return $this->response->withBadRequest("Provide items ids");
        }
    	return $this->response->json(['data' => $items]);
    }
}
