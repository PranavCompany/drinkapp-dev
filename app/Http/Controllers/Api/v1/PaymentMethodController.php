<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\ApiController;

use App\Repositories\UserRepository;

use Validator;
use App\User;
use Log;

class PaymentMethodController extends ApiController
{
    /**
     *  @OA\Tag(
     *      name="Payment Method",
     *      description=""
     *  )
     */

    protected $user;

    public function __construct(UserRepository $user)
    {
        parent::__construct();

        $this->user = $user;
    }

    /**
     * @OA\Get(
     *     path="/api/v1/payment-method",
     *     summary="Get all cards",
     *     tags={"Payment Method"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(response="200", description="")
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if (empty($user->stripe_id)) {
            $user->createAsStripeCustomer();
        }
        $cards = $user->paymentMethods()->map(function($paymentMethod){
            return $paymentMethod->asStripePaymentMethod();
        });
        $default = $user->defaultPaymentMethod();
        if ($default) {
            if (method_exists($default, 'asStripePaymentMethod')) {
                $default = $default->asStripePaymentMethod();
            }
        }
        return $this->response->json(['cards' => $cards, 'default' => $default]);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/payment-method",
     *     summary="Add new payment method card",
     *     tags={"Payment Method"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="token",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="200", description="{data: []}")
     * )
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token'       => 'required|string'
        ]);

        Log::info('Checking validation for adding a card.');

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        else {

            $stripeToken = $request->input('token');
            Log::info('Stripe token is: '.$stripeToken);

            if (!$request->user()->stripe_id) {
                Log::info('No Stripe ID :(');
                $request->user()->createAsStripeCustomer();
                Log::info('Stripe ID :'.$request->user()->stripe_id);
            }

            try {
                Log::info('Trying to update card:'.$request->user()->stripe_id);
                $request->user()->addPaymentMethod($stripeToken);
                Log::info('After updating card.');
            } catch (\Exception $e) {
                $body = $e->getJsonBody();
                $err = $body['error'];
                $error = $err['message'];
                return $this->response->withBadRequest($error);
            }
            Log::info('Before updateDefaultPaymentMethodFromStripe()');
            $request->user()->updateDefaultPaymentMethodFromStripe();
            Log::info('After updateDefaultPaymentMethodFromStripe()');

            return $this->response->withNoContent();
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/payment-method/{id}/set_default",
     *     summary="Set default payment method",
     *     tags={"Payment Method"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(response="200", description="")
     * )
     */

    /**
     * Set default resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function set_default(Request $request, $id)
    {
        $request->user()->updateDefaultPaymentMethod($id);
        $request->user()->updateDefaultPaymentMethodFromStripe();

        return $this->response->withNoContent();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/payment-method/{id}",
     *     summary="Delete payment method",
     *     tags={"Payment Method"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(response="200", description="")
     * )
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        foreach ($request->user()->paymentMethods() as $paymentMethod) {
            if ($id == $paymentMethod->id) {
                $paymentMethod->delete();
            }
        }

        $request->user()->updateDefaultPaymentMethodFromStripe();
    }
}
