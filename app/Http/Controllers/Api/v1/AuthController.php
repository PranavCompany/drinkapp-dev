<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\API\ApiController;
use App\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Laravel\Socialite\Facades\Socialite;
use Validator;

class AuthController extends ApiController
{
    /**
     * @OA\Info(
     *     version="1.0",
     *     title="DrinkApp APIs"
     * )
     */

    /**
     *  @OA\Tag(
     *      name="Auth",
     *      description=""
     *  )
     */

	/**
	 * @OA\Post(
	 *      path="/api/v1/login",
     *      summary="Login and get access token",
     *      tags={"Auth"},
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="email",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="password",
     *                      type="string"
     *                  ),
     *                  example={"email": "abc@drink.com", "password": "Password123"}
     *              )
     *          )
     *      ),
	 *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(type="json")
     *      ),
     *      @OA\Response(response=400, description="Invalid username/password supplied")
	 * )
	 */

    /**
     * Login
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
    	$credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if (auth()->attempt($credentials)) {
            if (auth()->user()->hasRole('customer')) {
                $token = auth()->user()->createToken('DrinkApp')->accessToken;
                $user = auth()->user();
                return response()->json(['token' => $token,
                                         'name' => $user->name], 200);
            }
            else {
                return response()->json(['error' => 'UnAuthorised'], 401);
            }
        } else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }

    /**
	 * @OA\Post(
     *      path="/api/v1/register",
     *      tags={"Auth"},
     *      summary="Register",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="name",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="email",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="yob",
     *                      type="integer",
     *                  ),
     *                  @OA\Property(
     *                      property="gender",
     *                      type="string",
     *                      description="male/female"
     *                  ),
     *                  @OA\Property(
     *                      property="password",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="password_confirmation",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="200", description="{'result': 'success'}"),
	 *      @OA\Response(response=400, description="{'result': 'error', 'messages': [...]}")
     * )
	 */

    /**
     * Register Customer
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                  => ['required','regex:/^[\pL\s\-]+$/u', 'max:255'],
            'email'                 => 'required|email:rfc,dns|unique:users,email',
            'password'              => 'required|confirmed|min:6|max:255',
            'password_confirmation' => 'required',
            'yob' => ['regex:/^[0-9]{4}$/'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        } else {
            $data = array_merge($request->only('name', 'email', 'password', 'gender', 'yob'), [
                'status' => 1
            ]);
            if (isset($data['gender']) && !empty($data['gender'])) {
                if (!in_array($data['gender'], ['male', 'female'])) {
                    $data['gender'] = null;
                }
            }
            $data['password'] = bcrypt($data['password']);
            try {
                $user = User::create($data);
            } catch (QueryException $exception)
            {
                return $this->response->withBadRequest('This email is already in use.');
            }

            $user->assignRole('customer');

            $user->createAsStripeCustomer();

            $token = $user->createToken('DrinkApp')->accessToken;

            return $this->response->json(['result' => 'success', 'token' => $token]);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/v1/login_facebook",
     *      tags={"Auth"},
     *      summary="Login",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="api_token",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="200", description="{'result': 'success'}"),
     *      @OA\Response(response=400, description="{'result': 'error', 'messages': [...]}")
     * )
     */

    /**
     * Register Customer
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login_facebook(Request $request) {
        $validator = Validator::make($request->all(), [
            'api_token' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        try {
            $social_user = Socialite::driver('facebook')->userFromToken($request->input('api_token'));
        } catch (ClientException $exception) {
            return $this->response->withBadRequest('Bad request');
        }
        if ($social_user) {
            $user = User::where('facebook_id', $social_user->id);
            if ($social_user->email) {
                $user = $user->orWhere('email', '=', $social_user->email);
            }
            $user = $user->get()->first();
            if (!$user) {
                try {
                    $user = User::create(
                        [
                            'name' => $social_user->name,
                            'email' => empty($social_user->email) ? "{$social_user->id}id@fb.com": $social_user->email,
                            'facebook_id' => $social_user->id,
                            'status' => 1,
                        ]
                    );
                } catch (QueryException $exception)
                {
                    return $this->response->withBadRequest('This email is already in use.');
                }

                $user->assignRole('customer');
                $user->createAsStripeCustomer();
                $token = $user->createToken('DrinkApp')->accessToken;
            } else {
                if ($user->hasRole('customer')) {
                    if (empty($user->fecebook_id)) {
                        $user->facebook_id = $social_user->id;
                        $user->save();
                    }
                    $token = $user->createToken('DrinkApp')->accessToken;
                    return $this->response->json([
                        'result' => 'success',
                        'token' => $token,
                        'user' => $user,
                        'email'=> $social_user->email
                    ]);
                } else {
                    return response()->json(['error' => 'UnAuthorised'], 401);
                }
            }
            return $this->response->json(['result' => 'success', 'token' => $token, 'user' => $user, 'email'=> $social_user->email]);
        }
        return response()->json(['error' => 'UnAuthorised'], 401);
    }

    /**
     * @OA\Post(
         *      path="/api/v1/login_google",
     *      tags={"Auth"},
     *      summary="Register",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="name",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="email",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="google_id",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="secret",
     *                      type="string"
     *                  ),
     *              )
     *          )
     *      ),
     *      @OA\Response(response="200", description="{'result': 'success'}"),
     *      @OA\Response(response=400, description="{'result': 'error', 'messages': [...]}")
     * )
     */

    /**
     * Register Customer
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login_google(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'google_id' => 'required',
            'secret' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        $check = md5($request->input('name').$request->input('google_id').$request->input('email').'drink-salt');
        if ($check !== $request->input('secret')) {
            return $this->response->withBadRequest('Incorrect secret');
        }
        $user = User::where('google_id', $request->input('google_id'))
            ->orWhere('email', '=', $request->input('email'))->get()->first();
        if (!$user) {
            try {
                $user = User::create(
                    [
                        'name' => $request->input('name'),
                        'email' => $request->input('email'),
                        'google_id' => $request->input('google_id'),
                        'status' => 1,
                    ]
                );
            } catch (QueryException $exception)
            {
                return $this->response->withBadRequest('This email is already in use.');
            }
            $user->assignRole('customer');
            $user->createAsStripeCustomer();
            $token = $user->createToken('DrinkApp')->accessToken;
        } else {
            if ($user->hasRole('customer')) {
                if (empty($user->gogle_id)) {
                    $user->google_id =$request->input('google_id');
                    $user->save();
                }
                $token = $user->createToken('DrinkApp')->accessToken;
                return $this->response->json(['result' => 'success', 'token' => $token, 'user' => $user]);
            } else {
                return response()->json(['error' => 'UnAuthorised'], 401);
            }
        }
        return $this->response->json(['result' => 'success', 'token' => $token, 'user' => $user]);
    }

    /**
     * @OA\Post(
     *      path="/api/v1/login_apple",
     *      tags={"Auth"},
     *      summary="Register",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="name",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="email",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="apple_id",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="secret",
     *                      type="string"
     *                  ),
     *              )
     *          )
     *      ),
     *      @OA\Response(response="200", description="{'result': 'success'}"),
     *      @OA\Response(response=400, description="{'result': 'error', 'messages': [...]}")
     * )
     */

    /**
     * Register Customer
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login_apple(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'apple_id' => 'required',
            'secret' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        $check = md5($request->input('name').$request->input('apple_id').$request->input('email').'drink-salt');
        if ($check !== $request->input('secret')) {
            return $this->response->withBadRequest('Incorrect secret');
        }
        $user = User::where('apple_id', $request->input('apple_id'))
            ->orWhere('email', '=', $request->input('email'))->get()->first();
        if (!$user) {
            try {
                $user = User::create(
                    [
                        'name' => $request->input('name'),
                        'email' => $request->input('email'),
                        'apple_id' => $request->input('apple_id'),
                        'status' => 1,
                    ]
                );
            } catch (QueryException $exception)
            {
                return $this->response->withBadRequest('This email is already in use.');
            }
            $user->assignRole('customer');
            $user->createAsStripeCustomer();
            $token = $user->createToken('DrinkApp')->accessToken;
        } else {
            if ($user->hasRole('customer')) {
                if (empty($user->apple_id)) {
                    $user->apple_id =$request->input('apple_id');
                    $user->save();
                }
                $token = $user->createToken('DrinkApp')->accessToken;
                return $this->response->json(['result' => 'success', 'token' => $token, 'user' => $user]);
            } else {
                return response()->json(['error' => 'UnAuthorised'], 401);
            }
        }
        return $this->response->json(['result' => 'success', 'token' => $token, 'user' => $user]);
    }
    /**
     * @OA\Post(
     *      path="/api/v1/forgot-password",
     *      summary="Forgot password - send reset password email",
     *      tags={"Auth"},
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="email",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(type="json")
     *      ),
     *      @OA\Response(response=400, description="")
     * )
     */

    /**
     * Forgot password
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function forgot_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email:rfc,dns',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        else {
            // We will send the password reset link to this user. Once we have attempted
            // to send the link, we will examine the response then see the message we
            // need to show to the user. Finally, we'll send out a proper response.
            $response = Password::broker()->sendResetLink(
                $request->only('email')
            );

            if ($response == Password::RESET_LINK_SENT) {
                return $this->response->json(['result' => 'success', 'status' => trans($response)]);
            }
            else {
                return $this->response->withBadRequest(['errors' => ['email' => trans($response)]]);
            }
        }
    }

    /**
     * @OA\Post(
     *      path="/api/v1/password",
     *      summary="Change password",
     *      tags={"Auth"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="password",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="password_confirmation",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="{'result': 'success'}",
     *          @OA\Schema(type="json")
     *      ),
     *      @OA\Response(response=400, description="{'result': 'error', 'messages': [...]}")
     * )
     */

     /**
     * Update the user's password.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function change_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        else {
            $request->user()->update([
                'password' => bcrypt($request->password),
            ]);

            return $this->response->json(['result' => 'success']);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/v1/profile",
     *      summary="Change profile information",
     *      tags={"Auth"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="name",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="email",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="{'result': 'success'}",
     *          @OA\Schema(type="json")
     *      ),
     *      @OA\Response(response=400, description="{'result': 'error', 'messages': [...]}")
     * )
     */

     /**
     * Update the user's password.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function change_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                  => 'required|min:3|max:255',
            'email'                 => 'required|email:rfc,dns|unique:users,email,'.$request->user()->id
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        else {

            $request->user()->update([
                'name' => $request->name,
                'email' => $request->email
            ]);

            return $this->response->json(['result' => 'success']);
        }
    }
}
