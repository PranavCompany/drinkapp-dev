<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\ApiController;

use App\Repositories\UserRepository;

use Validator;
use App\User;

class SettingController extends ApiController
{
	/**
     *  @OA\Tag(
     *      name="Settings",
     *      description=""
     *  )
     */

	protected $user;

    public function __construct(UserRepository $user)
    {
        parent::__construct();

        $this->user = $user;
    }

    public function index(Request $request) 
    {
    	return $this->response->collection($this->order->pageWithRequest($request));
    }

    public function get(Request $request, $id)
    {
        $order = $this->order->getById($id);
        if ($order) {
            return $this->response->item($order);
        }
        else {
            return $this->response->withBadRequest($order);
        }
    }    

    public function updateOneSignal(Request $request)
    {
        $user = $request->user();

        $this->validate($request, [
            'player_id' => 'required|string|max:255',
        ]);

        return tap($user)->update($request->only('player_id'));
    }

      /**
     * @OA\Post(
     *     path="/api/v1/settings/onesignal",
     *     summary="Update OneSignal player id",
     *     tags={"Settings"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="player_id",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="200", description="{data: []}")
     * )
     */
}
