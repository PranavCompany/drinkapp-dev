<?php

namespace App\Http\Controllers\API\v1;

use App\Discounts;
use App\Http\Controllers\API\ApiController;
use App\Item;
use App\Location;
use App\Rating;
use App\Repositories\LocationRepository;
use App\Scopes\OwnerScope;
use App\Transformers\CategoryListTransformer;
use App\Transformers\CategoryTransformer;
use App\Transformers\CategoryTransformerGrouped;
use App\Transformers\LocationTransformerClosest;
use App\Transformers\LocationTransformerGet;
use App\Transformers\LocationTransformerList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;

class LocationController extends ApiController
{
    /**
     *  @OA\Tag(
     *      name="Location",
     *      description=""
     *  )
     */

    protected $location;

    public function __construct(LocationRepository $location)
    {
        parent::__construct();

        $this->location = $location;
    }

    /**
	 * @OA\Get(
	 *     path="/api/v1/location",
	 *	   summary="Get all locations",
	 *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="keyword",
     *          in="query",
     *          description="keyword to search name or address",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
	 *     @OA\Response(response="200", description="{data: [], meta: {pagination: {total: 4,count: 4,per_page: 10,current_page: 1,total_pages: 1,links: []}}}")
	 * )
	 */
    public function index(Request $request)
    {
        $key = 'empty';
        if ($user = $request->user()) {
            $key = sha1($user->getAuthIdentifier());
        } else {
            if ($route = $request->route()) {
                $key = sha1($route->getDomain() . '|' . $request->ip());
            }
        }
        Log::info('LocationController:index ---> '.$key);
        return $this->response->collection($this->location->search($request));
    }

    /**
     * @OA\Get(
     *     path="/api/v1/location/types",
     *	   summary="Get all locations types",
     *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="lat",
     *          in="query",
     *          description="latitude",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="lng",
     *          in="query",
     *          description="longitude",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(response="200", description="{data: [], meta: {pagination: {total: 4,count: 4,per_page: 10,current_page: 1,total_pages: 1,links: []}}}")
     * )
     */
    public function types(Request $request)
    {
        return $this->response->json($this->location->allTypes($request));
    }

    /**
	 * @OA\Get(
	 *     path="/api/v1/location/closest",
	 *	   summary="Get the closest 5 locations",
	 *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="latitude",
     *          in="query",
     *          description="Latitude",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="longitude",
     *          in="query",
     *          description="Longitude",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
	 *     @OA\Response(response="200", description="{data: [], meta: {pagination: {total: 4,count: 4,per_page: 10,current_page: 1,total_pages: 1,links: []}}}")
	 * )
	 */
    public function closest(Request $request)
    {
        return $this->response->collection($this->location->closest($request), new LocationTransformerClosest);
    }

    /**
	 * @OA\Get(
	 *     path="/api/v1/location/{id}",
	 *	   summary="Get a location data",
	 *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
	 *     @OA\Response(response="200", description="{data:{id:3,...}}")
	 * )
	 */
    public function get(Request $request, $id)
    {
        if ($request->input('device_id')) {
            $discounts = Discounts::where('device_id', $request->input('device_id'))
                ->when(auth()->user(), function ($query) {
                    $query->where('user_id', auth()->user()->id);
                })
                ->where('location_id', $id)
                ->get();
        } else {
            $discounts = [];
        }
        return $this->response->item($this->location->getById($id), new LocationTransformerGet($discounts));
    }


    /**
     * @OA\Get(
     *     path="/api/v1/location/{id}/delivery",
     *     summary="Get all delivery status (home, collection, to table) of a location",
     *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(response="200", description="")
     * )
     */
    public function delivery(Request $request, $id)
    {
        $location = $this->location->getById($id);
        $deliveryMethods["to_table"] = $location->to_table_auto;
        $deliveryMethods["collection"] = $location->collection_auto;
        $deliveryMethods["to_home"] = $location->to_home_auto;
        return $this->response->json(["data" => $deliveryMethods]);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/location/{id}/discount",
     *     summary="Set user discount on device",
     *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="code",
     *                      type="float",
     *                  ),
     *                   @OA\Property(
     *                      property="device_id",
     *                      type="string",
     *                  ),
     *              )
     *          )
     *      ),
     *     @OA\Response(response="200", description="")
     * )
     */

    public function discount(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'device_id' => 'required',
            'code' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        $discount = Discounts::where('location_id', $id)
            ->where('code', $request->input('code'))
            ->whereRaw('(user_id is null or user_id='.auth()->user()->id.')')
            ->first();
        if (!empty($discount)) {
            $discount->device_id = $request->input('device_id');
            $discount->user_id = auth()->user()->id;
            $discount->save();
        } else {
            return $this->response->withBadRequest('Code doesn\'t exist for selected location');
        }
        return $this->response->json($discount);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/location/{id}/remove-discount",
     *     summary="Remove user discount from device",
     *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="code",
     *                      type="float",
     *                  ),
     *                   @OA\Property(
     *                      property="device_id",
     *                      type="string",
     *                  ),
     *              )
     *          )
     *      ),
     *     @OA\Response(response="200", description="")
     * )
     */
    public function remove_discount(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'device_id' => 'required',
            'code' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        $discount = Discounts::where('location_id', $id)
            ->where('code', $request->input('code'))
            ->whereRaw('user_id='.auth()->user()->id)
            ->first();
        if (!empty($discount)) {
            $discount->device_id = null;
            $discount->user_id = auth()->user()->id;
            $discount->save();
        } else {
            return $this->response->withBadRequest('Code doesn\'t exist for selected location');
        }
        return $this->response->json($discount);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/location/{id}/delivery-price",
     *     summary="Get delivery price for user location",
     *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="lat",
     *                      type="float",
     *                  ),
     *                  @OA\Property(
     *                      property="lng",
     *                      type="float",
     *                  ),
     *              )
     *          )
     *      ),
     *     @OA\Response(response="200", description="")
     * )
     */
    public function delivery_price(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        $location = $this->location->getById($id);
        $lat1 = (float)$request->input('lat');
        $lng1 = (float)$request->input('lng');
        $distance = Location::distance($lat1, $lng1, (float)$location->latitude, (float)$location->longitude);
        if (!$location->to_home_auto || !$location->home_delivery_radius_settings || !is_array($location->home_delivery_radius_settings)) {
            return $this->response->withBadRequest('Home delivery unavailable atm.');
        }
        $price = $location->getPriceByDistance($distance);
        if ($price !== false) {
            $data = [
                'status' => 'success',
                'price' => round($price, 2),
                'home_delivery_max_radius' => $location->home_delivery_max_radius,
                'latitude' => $location->latitude,
                'longitude' => $location->longitude,
            ];
        } else {
            $data = [
                'status' => 'error',
                'price' => 0,
                'home_delivery_max_radius' => $location->home_delivery_max_radius,
                'latitude' => $location->latitude,
                'longitude' => $location->longitude,
            ];
        }
        return $this->response->json($data);
    }
	/**
	 * @OA\Get(
	 *     path="/api/v1/location/{id}/categories",
	 *	   summary="Get all categories of a location",
	 *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
	 *     @OA\Response(response="200", description="")
	 * )
	 */
    public function categories(Request $request, $id)
    {
        $location = $this->location->getById($id);
    	$categories = $location->categories;
        $special_offers_ids = [];
        $special_offers = [];
        $out_of_stock_ids= [];
        $item_ids = [];
        foreach ($location->items as $item) {
            if ($item->pivot->out_of_stock) {
                $out_of_stock_ids[] = $item->id;
            }
            if ($item->pivot->special_offer) {
                $special_offers_ids[] = $item->id;
            }
            $item_ids[] = $item->id;
        }

        $new_categories = [];
        foreach($categories as $category)
        {
            $category_items = [];
            foreach($category->items as $item) {
                info($item->id);
                if ($location->currency === 'eur') {
                    $item->price = $item->price_eur;
                } else if ($location->currency === 'aed') {
                    $item->price = $item->price_aed;
                }  else if ($location->currency === 'usd') {
                    $item->price = $item->price_usd;
                } else {
                    $item->price = $item->price_gbp;
                }
                if (in_array($item->id, $item_ids) && !in_array($item->id, $special_offers_ids)) {
                    $item->out_of_stock = (int)in_array($item->id, $out_of_stock_ids);
                    $category_items[] = $item;
                } elseif (in_array($item->id, $special_offers_ids)) {
                    $item->out_of_stock = (int)in_array($item->id, $out_of_stock_ids);
                    $special_offers[] = $item;
                }
            }
            info($category_items);

            $category->items = $category_items;
            if (!empty($category->items)) {
                $new_categories[] = $category;
            }
        }
        $special_offers_category = [
            'id' => 100500,
            'name' => 'Special Offers',
            'description' => '',
            'images' => [],
            'items' => $special_offers,
        ];
        $resp = $this->response->collection_items($new_categories, new CategoryTransformer);
        if (!empty($special_offers)) {
            $resp['data'] = array_merge([$special_offers_category], $resp['data']);
        }
    	return $this->response->json($resp);
    }/**
 * @OA\Post(
 *     path="/api/v1/location/rating/{id}",
 *     summary="Set location rating",
 *     tags={"Location"},
 *      @OA\Parameter(
 *          name="Accept",
 *          in="header",
 *          description="application/json",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="Authorization",
 *          in="header",
 *          description="Bearer {token}",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\RequestBody(
 *          @OA\MediaType(
 *              mediaType="application/x-www-form-urlencoded",
 *              @OA\Schema(
 *                  @OA\Property(
 *                      property="rating",
 *                      type="integer",
 *                  ),
 *              )
 *          )
 *      ),
 *     @OA\Response(response="200", description="{rating:{id:3,...}}")
 * )
 */
    public function addrating(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'rating' => 'required|integer|max:5',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        }
        $location = Location::withoutGlobalScope(OwnerScope::class)->findOrFail($id);
        $check = Rating::where('user_id', '=', $request->user()->id)->where('location_id', '=', $id)->first();
        if ($check !== null) {
            return $this->response->withBadRequest('Location already been rated by this user');
        }
        if ($location) {
            $rating = new Rating();
            $rating->user_id = $request->user()->id;
            $rating->location_id = $id;
            $rating->rating = (int)$request->input('rating');
            $rating->save();
            return $this->response->json(['rating' => $rating]);
        } else {
            return $this->response->withBadRequest($location);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v1/location/{id}/categories/list",
     *	   summary="Get categories of a location",
     *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="type",
     *                      type="string",
     *                  ),
     *              )
     *          )
     *      ),
     *     @OA\Response(response="200", description="")
     * )
     */

    public function categories_list(Request $request, $id)
    {
        return $this->getCategories($request, $id, true);
    }
    /**
     * @OA\Get(
     *     path="/api/v1/location/{id}/categories/list-grouped",
     *	   summary="Get categories tree of a location",
     *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="type",
     *                      type="string",
     *                  ),
     *              )
     *          )
     *      ),
     *     @OA\Response(response="200", description="")
     * )
     */
    public function categories_list_grouped(Request $request, $id)
    {
        return $this->getCategoriesGrouped($request, $id);
    }

    private function getCategoriesGrouped(Request $request, $id)
    {
        $location = $this->location->getById($id);
        $special_offers_category = [
            'id' => 100500,
            'name' => 'Special Offers',
            'description' => '',
            'type' => '',
        ];

        $special_offers = Db::table('items')->select('type')
            ->join('location_has_items', 'location_has_items.item_id', '=', 'items.id')
            ->join('categories', 'categories.id', '=', 'items.category_id')
            ->where('location_has_items.location_id', $id)
            ->whereRaw('exists(select 1 from location_has_categories
                where location_has_categories.location_id=location_has_items.location_id
             and location_has_categories.category_id = categories.id)')
            ->where('location_has_items.special_offer', '=', 1)->distinct()->get()->toArray();
        $categories = $location->categories_with_items_or_parent();

        if (!empty($request->input('type')) && in_array($request->input('type'),['food', 'drinks'])) {
            $categories = $categories->where('type', '=', $request->input('type'));
        }
        $resp = $this->response->collection_items($categories->get(), new CategoryTransformerGrouped($id));
        if (!empty($special_offers)) {
            if (count($special_offers) === 1) {
                $special_offers_category['type'] = $special_offers[0]->type;
            }
            $resp['data'] = array_merge([$special_offers_category], $resp['data']);
        }
        return $this->response->json($resp);
    }

    private function getCategories(Request $request, $id, $all = true)
    {
        $location = $this->location->getById($id);
        $special_offers_category = [
            'id' => 100500,
            'name' => 'Special Offers',
            'description' => '',
            'type' => '',
        ];

        $special_offers = Db::table('items')->select('type')
            ->join('location_has_items', 'location_has_items.item_id', '=', 'items.id')
            ->join('categories', 'categories.id', '=', 'items.category_id')
            ->where('location_has_items.location_id', $id)
            ->whereRaw('exists(select 1 from location_has_categories
                where location_has_categories.location_id=location_has_items.location_id
             and location_has_categories.category_id = categories.id)')
            ->where('location_has_items.special_offer', '=', 1)->distinct()->get()->toArray();
        if ($all) {
            $categories = $location->all_categories_with_items();
        } else {
            $categories = $location->categories_with_items();
        }

        if (!empty($request->input('type')) && in_array($request->input('type'),['food', 'drinks'])) {
            $categories = $categories->where('type', '=', $request->input('type'));
        }

        $resp = $this->response->collection_items($categories->get(), new CategoryListTransformer);
        if (!empty($special_offers)) {
            if (count($special_offers) === 1) {
                $special_offers_category['type'] = $special_offers[0]->type;
            }
            $resp['data'] = array_merge([$special_offers_category], $resp['data']);
        }
        return $this->response->json($resp);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/location/{id}/location_category_items/{category_id}",
     *	   summary="Get all items of location category",
     *     tags={"Location"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(response="200", description="")
     * )
     */
    public function location_category_items(Request $request, $id, $category_id)
    {

        $location = Location::findOrFail($id);
        $items = Item::select(
            'items.id',
            'items.name',
            'items.description',
            'items.price_eur',
            'items.price_gbp',
            'items.price_aed',
            'items.price_usd',
            'out_of_stock',
            'category_id',
            DB::raw('location_has_items.special_offer as is_special'),
            DB::raw('if(locations.currency = "gbp", price_gbp,
            if(locations.currency = "aed", price_aed,
            if(locations.currency = "usd", price_usd,
             price_eur))) as price'),
            DB::raw('if( locations.currency = "gbp", "£",
             if(locations.currency = "aed", "د.إ",
             if(locations.currency = "usd", "$",
              "€"))) as currency'),
            Item::canUseDiscount(),
            DB::raw('if(locations.currency = "gbp", "price_gbp",
             if(locations.currency = "aed", "price_aed",
             if(locations.currency = "usd", "price_usd",
               "price_eur"))) as currency_field')
        )->join('location_has_items', 'location_has_items.item_id', '=', 'items.id')
            ->join('locations', 'locations.id', '=', 'location_has_items.location_id')
            ->with('images')
            ->with('options')
            ->with(['addons' => function ($query) use ($location) {
                if ($location->currency === 'gbp') {
                    $field = 'addons.price_gbp';
                } else if ($location->currency === 'aed') {
                    $field = 'addons.price_aed';
                } else if ($location->currency === 'usd') {
                    $field = 'addons.price_usd';
                } else  {
                    $field = 'addons.price_eur';
                }
                return $query->select('id', 'name')->selectRaw($field.' as price');
            }]);
        if ((int)$category_id === 100500) {
            $items = $items->where('location_has_items.special_offer', '=', 1)->with('category');
        } else {
            $items = $items->where('category_id', '=', $category_id);
        }
        $data = $items->where('location_has_items.location_id', '=', $id)->paginate(15);
        if ((int)$category_id === 100500) {
            foreach ($data as $k => &$item) {
                $item['category_id'] = 100500;
            }
        }
        return $this->response->json(['data' => $data]);
    }
}
