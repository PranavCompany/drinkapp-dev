<?php

namespace App\Http\Controllers\API\v1;

use App\Category;
use App\Discounts;
use App\Http\Controllers\API\ApiController;
use App\Item;
use App\Location;
use App\Order;
use App\Repositories\OrderRepository;
use App\Scopes\OwnerScope;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Validator;

class OrderController extends ApiController
{
    /**
     * @OA\Tag(
     *      name="Order",
     *      description=""
     *  )
     */

    protected $order;

    public function __construct(OrderRepository $order)
    {
        parent::__construct();

        $this->order = $order;
    }

    /**
     * @OA\Get(
     *     path="/api/v1/order",
     *       summary="Get all orders",
     *     tags={"Order"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(response="200", description="{data: [], meta: {pagination: {total: 4,count: 4,per_page: 10,current_page: 1,total_pages: 1,links: []}}}")
     * )
     */
    public function index(Request $request)
    {
        return $this->response->collection($this->order->pageWithRequestPaid($request));
    }

    /**
     * @OA\Post(
     *     path="/api/v1/order",
     *     summary="Submit new order",
     *     tags={"Order"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="card_id",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="location_id",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="is_delivery",
     *                      type="boolean"
     *                  ),
     *                  @OA\Property(
     *                      property="table_number",
     *                      type="integer"
     *                  ),
     *                  @OA\Property(
     *                      property="grandtotal_price",
     *                      type="decimal"
     *                  ),
     *                  @OA\Property(
     *                      property="tips",
     *                      type="decimal"
     *                  ),
     *                  @OA\Property(
     *                      property="total_price",
     *                      type="decimal"
     *                  ),
     *                  @OA\Property(
     *                      property="total_fee",
     *                      type="decimal"
     *                  ),
     *                  @OA\Property(
     *                      property="description",
     *                      type="string"
     *                  ),
     *                     @OA\Property(
     *                      property="delivery_price",
     *                      type="decimal"
     *                  ),
     *                  @OA\Property(
     *                      property="items",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="200", description="{data: []}")
     * )
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'location_id' => 'required|exists:locations,id',
            'description' => 'nullable|string',
            'items.*.id' => 'required|exists:items,id',
            'items.*.quantity' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->response->withBadRequest($errors);
        } else {
            $location = Location::withoutGlobalScope(OwnerScope::class)
                ->with('price_config')
                ->findOrFail($request->input('location_id'));
            $out_of_stock_ids = [];
            foreach ($location->out_of_stock_items as $item_) {
                $out_of_stock_ids[] = $item_->id;
            }
            $retailer_id = $location->user_id;

            $table_number = $request->input('table_number');
            $delivery_address = $request->input('delivery_address');
            if (($table_number && !$location->to_table_auto)
                || (!$table_number && !$location->collection && !$delivery_address)
                || ($delivery_address && !$location->to_home_auto)) {
                return $this->response->withBadRequest('Error, selected delivery method unaivalable. Please pick another delivery method');
            }
            $items = $request->input('items');
            $out_of_stock_items = [];
            $category_ids = [];
            foreach ($items as $item) {
                $item_object = Item::withoutGlobalScope(OwnerScope::class)->findOrFail($item['id']);
                $items_objects[] = $item_object;
                if (in_array($item_object->id, $out_of_stock_ids)) {
                    $out_of_stock_items[] = $item_object;
                }
                $category_ids[] = $item_object->category_id;
                if ($item_object->options->count()) {
                    foreach ($item_object->options as $option) {
                        if (!isset($item['options'][$option->id])) {
                            return $this->response->withBadRequest("Please update your app in order to submit this order");
                        }
                    }
                }
            }
            $categories_check = DB::table('location_has_categories')
                ->selectRaw("distinct ".Location::$check_query." as check_")
                ->whereIn('category_id', $category_ids)
                ->where('location_id', $location->id)->get()->toArray();
            foreach ($categories_check as $cat_check) {
                if (!$cat_check->check_) {
                    return $this->response->withBadRequest('Unfortunately some of the items in your cart can\'t be ordered anymore. Please review your order');
                }
            }
            if (!empty($out_of_stock_items)) {
                return $this->response->setStatusCode(HttpResponse::HTTP_BAD_REQUEST)->json([
                    'result' => 'error',
                    'out_of_stock_items' => $out_of_stock_items,
                ]);
            }
            if (!empty($request->input('delivery_address'))) {
                if (empty($request->input('lat')) || empty($request->input('lng'))) {
                    return $this->response->withBadRequest('Error, empty lat or lng fields');
                } else {
                    $lat1 = (float)$request->input('lat');
                    $lng1 = (float)$request->input('lng');
                    $distance = Location::distance($lat1, $lng1, (float)$location->latitude, (float)$location->longitude);
                    if ($distance > (float)$location->home_delivery_max_radius) {
                        return $this->response->withBadRequest('Address is to far away');
                    }
                }
                if ((float)$request->input('total_price') < (float)$location->home_delivery_minimum_order) {
                    return $this->response->withBadRequest('Order sum must be greater then '.round($location->home_delivery_minimum_order, 2).Location::currencyToSymbol($location->currency));
                }
            }
            if (count($items) > 0) {
                $card_id = $request->input('card_id');
                Stripe::setApiKey(env('STRIPE_SECRET'));
                $error = "";
                Log::info('Order request'. print_r($request->all(), true));
                try {
                    if (!$card_id) {
                        $card =  $request->user()->defaultPaymentMethod();
                        if (method_exists($card, 'asStripePaymentMethod')) {
                            $card = $card->asStripePaymentMethod();
                        }
                        if ($card) {
                            $card_id = $card->id;
                        } else {
                            return $this->response->withBadRequest("Please add payment method");
                        }
                    }
                    $retailer = User::findOrFail($retailer_id);
                    $grandtotal_price = $request->input('grandtotal_price');
                    $total_price = $request->input('total_price');
                    $tips = $request->input('tips');
                    $total_fee = $request->input('total_fee');
                    if (isset($distance) && $request->input('delivery_address')) {
                        $delivery_price = $location->getPriceByDistance($distance)??0;
                    } else {
                        $delivery_price = 0;
                    }
                    $discount = null;
                    if ($request->has('device_id') && $request->has('discount_code')) {
                        $discount_object = Discounts::where('location_id', $location->id)
                            ->where('device_id', $request->input('device_id'))
                            ->where('code', $request->input('discount_code'))->first();
                        if ($discount_object) {
                            $discount = $discount_object->discount;
                        } else {
                            return $this->response->withBadRequest('Code '.$request->input('discount_code').' linked to another device');
                        }
                    }
                    $valid_result = Order::validateOrder($items, $total_price, $grandtotal_price, $tips, $total_fee, $location, $delivery_price, $discount);
                    if ($valid_result['valid'] !== true) {
                        if (env('APP_ENV') !== 'production') {
                            return $this->response->withBadRequest("Invalid summ, total:{$valid_result['valid_total']} != {$total_price} or
                         grand total: {$valid_result['valid_grand_total']} != {$grandtotal_price} or customer fee: {$valid_result['valid_total_fee']} != {$total_fee}
                         ");
                        }
                        return $this->response->withBadRequest("Invalid order totals. Please check for app updates or try again.");
                    }
                    $amount = $location->real_price_config->calculateAmount($total_price);
                    if ($retailer->stripe_standard_account_verified !== 5 && $retailer->stripe_acc_verified !== 5) {
                        return $this->response->withBadRequest("You can't order from current location atm. Please try again later.");
                    }
                    if ($retailer->stripe_standard_account_verified !== 5) {
                        $payment_array = [
                            "amount" => round($grandtotal_price * 100),
                            "currency" => $location->currency,
                            'customer' => $request->user()->stripe_id,
                            'payment_method' => $card_id,
                            'payment_method_types' => ['card'],
                            'confirmation_method' => 'automatic',
                            'on_behalf_of' => $retailer->stripe_acc_id,
                            'confirm' => true,
                            'transfer_data' => [
                                'destination' => $retailer->stripe_acc_id,
                                'amount' => $amount,
                            ],
                        ];
                        Log::info('Payment array'. print_r($payment_array, true));
                        try {
                            $payment = PaymentIntent::create($payment_array);
                        } catch (\Stripe\Exception\InvalidRequestException $e) {
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $error = $err['message'];
                        }
                    } else {
                        $payment_method_ = $card_id;
                        try {
                            $payment_method = \Stripe\PaymentMethod::create([
                                'customer' => $request->user()->stripe_id,
                                'payment_method' => $card_id,
                            ], [
                                'stripe_account' => $retailer->stripe_standard_account,
                            ]);
                            $payment_method_ = $payment_method->id;
                        } catch (\Stripe\Exception\ApiErrorException $e) {
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $error = $err['message'];
                        }
                        $amount = $location->real_price_config->calculateApplicationFee($location->currency, $total_price);
                        $payment_array = [
                            "amount" => round($grandtotal_price * 100),
                            "currency" => $location->currency,
                            'application_fee_amount' => $amount*100,
                            'payment_method' => $payment_method_,
                            'payment_method_types' => ['card'],
                            'confirmation_method' => 'automatic',
                            'confirm' => true,
                        ];
                        Log::info('Payment array'. print_r($payment_array, true));
                        try {
                            $payment = PaymentIntent::create($payment_array, ['stripe_account' => $retailer->stripe_standard_account]);
                        } catch (\Stripe\Exception\InvalidRequestException $e) {
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $error = $err['message'];
                        }
                    }
                } catch (\Stripe\Exception\CardException $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $error = $err['message'];
                }

                if ($error != "") {
                    return $this->response->withBadRequest($error);
                }

                if (isset($payment)) {
                    $queue = (int)DB::table('orders')
                            ->where('location_id', '=', $request->location_id)
                            ->where('retailer_id', '=', $retailer_id)
                            ->where('paid', '=', 1)
                            ->max('queue') + 1;
                    $order_data = $data = array_merge(
                        $request->only('location_id',
                            'is_delivery',
                            'phone_number',
                            'delivery_address',
                            'grandtotal_price_before_discount',
                            'total_price_before_discount',
                            'table_number',
                            'tips',
                            'total_fee'),
                        [
                            'grandtotal_price' => $grandtotal_price,
                            'total_price' => $total_price,
                            'user_id' => $request->user()->id,
                            'retailer_id' => $retailer_id,
                            'discount' => $discount,
                            'discount_sum' => $valid_result['discount_sum'],
                            'payment_id' => $payment->id,
                            'queue' => $queue,
                            "currency" => $location->currency,
                            "charge_type" => $retailer->stripe_standard_account_verified !== 5 ? 'custom' : 'standard',
                        ]);

                    $order = $this->order->store($order_data);

                    $price_name = 'price_' . $location->currency;
                    foreach ($items as $item) {
                        /** @var $item_obj Item */
                        $item_obj = Item::withoutGlobalScope(OwnerScope::class)->findOrFail($item['id']);
                        $options = null;
                        $addons = null;
                        if (isset($item['options']) && !empty($item['options']) && $item_obj->options->count()) {
                            $options = $item['options'];
                        }
                        if (isset($item['addons'])  &&! empty($item['addons']) && $item_obj->addons->count()) {
                            $addons = $item['addons'];
                            foreach ($addons as $key => &$value) {
                                if ($discount && DB::table('location_has_categories')
                                        ->selectRaw('1')
                                        ->where('category_id', $item_obj->category_id)
                                        ->whereNull('time_start')
                                        ->whereNull('time_end')
                                        ->where('location_id', $location->id)->exists()) {
                                    $value['price_with_discount'] = $value['price'] - round(($value['price'] * $discount / 100), 2);
                                } else {
                                    $value['price_with_discount'] = $value['price'];
                                }
                            }
                        }
                        $item_category = Category::withoutGlobalScope(OwnerScope::class)->findOrFail($item_obj->category_id);
                        if ($item_category) {
                            $item_type = $item_category->type;
                            $category_name = $item_category->display_name;
                        } else {
                            $item_type = 'drinks';
                            $category_name = '';
                        }
                        if ($discount && DB::table('location_has_categories')
                                ->selectRaw('1')
                                ->where('category_id', $item_obj->category_id)
                                ->whereNull('time_start')
                                ->whereNull('time_end')
                                ->where('location_id', $location->id)->exists()) {
                            $price_with_discount = $item_obj->$price_name - round($item_obj->$price_name*$discount/100, 2);
                        } else {
                            $price_with_discount = $item_obj->$price_name;
                        }
                        $this->order->addItem(
                            $order->id,
                            $item['id'],
                            $item['quantity'],
                            $item_obj->name,
                            $item_obj->$price_name,
                            $options,
                            $addons,
                            $item_type,
                            $category_name,
                            $price_with_discount
                        );
                    }
                    if ($payment->status === 'succeeded') {
                        $order->paid = true;
                        $order->paid_at = Carbon::now();
                    } else {
                        $order->paid = false;
                    }
                    if (!empty($order->phone_number) && !empty($order->delivery_address) && isset($distance)) {
                        $order->home_delivery_price = $location->getPriceByDistance($distance)??0;
                    }
                    $order->description = $request->input('description');
                    //If order made from old app
                    $fee_number = $location->real_price_config->customer_fee_number;
                    $order->customer_fee_percent = $valid_result !== true ? 4 : $location->real_price_config->customer_fee_percent;
                    $order->customer_fee_number = $valid_result !== true ? 0.20 : $fee_number;
                    //endif
                    $order->retailer_fee_number = $location->real_price_config->retailer_fee_number;
                    $order->retailer_fee_percent = $location->real_price_config->retailer_fee_percent;
                    $order->latitude = $request->input('lat');
                    $order->longitude = $request->input('lng');
                    $order->payment_status = $payment->status;
                    $order->status = $payment->status != 'requires_action' ? 0 : 1 ;
                    $order->client_secret = $payment->client_secret;
                    $order->currency = $location->currency;
                    $order->payment_method_id = $payment->payment_method;
                    $order->save();
                    $payment->updateAttributes( [
                        'metadata' => ['order_id' => $order->id],
                        'description' => "order id: {$order->id}",
                    ]);
                    $player_id = $request->input('player_id');
                    $user = $request->user();
                    if (!empty($player_id) && $request->user()->player_id !== $player_id) {
                        $user->player_id = $player_id;
                        $user->save();
                    }
                    try {
                        $payment->save();
                    } catch (\Stripe\Exception\ApiErrorException $e) {

                    }
//                    $request->user()->notify(new NewOrderNotification($order, $request->user()));
                }
                return $this->response->item($order);
            } else {
                return $this->response->withBadRequest();
            }
        }
    }
    /**
     * @OA\Get(
     *     path="/api/v1/order/{id}",
     *     summary="Get a order data",
     *     tags={"Order"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(response="200", description="{data:{id:3,...}}")
     * )
     */
    public function get(Request $request, $id)
    {
        $order = $this->order->getById($id);
        if ($order) {
            return $this->response->item($order);
        } else {
            return $this->response->withBadRequest($order);
        }
    }


    /**
     * @OA\Get(
     *     path="/api/v1/order/last",
     *     summary="Get last order data",
     *     tags={"Order"},
     *      @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          description="application/json",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(response="200", description="{data:{id:3,...}}")
     * )
     */
    public function last(Request $request)
    {
        $last_order = $this->order->getLastOrder($request->user());
        if ($last_order) {
            return $this->response->item($last_order);
        } else {
            return $this->response->withBadRequest($last_order);
        }
    }
}
