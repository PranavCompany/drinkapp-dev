<?php

namespace App\Http\Controllers\API;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class CustomerController extends ApiController
{
    protected $user;

    public function __construct(UserRepository $user)
    {
        parent::__construct();

        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!empty($request->input('sortOrder')) && !empty($request->input('sortKey'))) {
            return $this->response->collection($this->user
                ->pageWithRequestCustomers($request, 10, $request->input('sortOrder'), $request->input('sortKey')));
        } else {
            return $this->response->collection($this->user->pageWithRequestCustomers($request));
        }

    }
}
