<?php


namespace App\Http\Controllers\API;


use App\Discounts;
use App\Repositories\DiscountRepository;
use App\Repositories\LocationRepository;
use App\Transformers\DiscountsTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use File;
use Illuminate\Validation\ValidationException;

/**
 * Class DiscountController
 * @package App\Http\Controllers\Api
 */
class DiscountController extends ApiController
{
    var $location;
    var $discount;

    public function __construct(LocationRepository $locationRepository, DiscountRepository $discountRepository)
    {
        parent::__construct();
        $this->location = $locationRepository;
        $this->discount = $discountRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $location_id
     * @return JsonResponse
     */
    public function index(Request $request, $location_id)
    {
        $discounts = $this->discount->pageWithRequest($request, $location_id, 10, 'asc', 'user_id');
        return $this->response->collection($discounts);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($location_id, $id)
    {
        $this->discount->destroy($location_id, $id);
        return $this->response->withNoContent();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request, $location_id)
    {
        $this->validate($request, [
            'code' => 'required',
            'discount' => 'required',
        ]);

        $data = array_merge(['location_id' => $location_id], $request->only('code', 'discount'));

        $discount = $this->discount->store($data);

        return $this->response->item($discount, new DiscountsTransformer());
    }

    public function edit(Request $request, $location_id, $id)
    {
        $discount = $this->discount->getById($location_id, $id);
        return $this->response->item($discount);
    }

    public function generate(Request $request, $location_id)
    {
        $this->validate($request, [
            'codes' => 'required|numeric',
            'discount' => 'required|numeric|min:1|max:100',
        ]);
        $codes = (int)$request->input('codes');
        $discount = (int)$request->input('discount');
        Discounts::generateCodes($codes, $discount, $location_id);
        return $this->response->withNoContent();
    }
    public function import(Request $request, $location_id) {
        $file = $request->file('file');
        $content = File::get($file->getRealPath());
        $rows = explode("\r\n", $content);
        $counter = 0;
        foreach ($rows as $row) {
            $columns = explode(',', $row);
            if (isset($columns[1]) && (float)$columns[1] > 0 && (float)$columns[1] <= 100) {
                if (!Discounts::codeExists($columns[0], $location_id)) {
                    $discount = new Discounts();
                    $discount->code = $columns[0];
                    $discount->discount = (float)$columns[1];
                    $discount->location_id = $location_id;
                    $discount->save();
                    $counter += 1;
                }
            }
        }
        return $this->response->json([
            'codes' => $counter
        ]);
    }
    public function update(Request $request, $location_id, $id)
    {
        $this->validate($request, [
            'code' => 'required|string|max:255',
            'discount' => 'required|numeric|between:0,100',
        ]);
        $updated_discount = $this->discount->update($location_id, $id, $request->only('code', 'discount'));

        return $this->response->item($updated_discount);
    }
}
