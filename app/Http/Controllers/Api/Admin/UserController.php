<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\API\ApiController;
use App\Mail\RetailerInvited;
use App\PriceConfig;
use App\Repositories\UserRepository;
use App\UsersPriceConfig;
use Illuminate\Http\Request;
use Mail;
use Auth;

class UserController extends ApiController
{
    protected $user;

    public function __construct(UserRepository $user)
    {
        parent::__construct();

        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $sortColumn = $request->input('sortKey')??'name';
        $sort = $request->input('sortOrder')??'asc';
        return $this->response->collection($this->user->pageWithRequest($request, 10, $sort, $sortColumn));
    }
    public function check_pin(Request $request)
    {
        $resp = ['status' => 'error'];
        if (Auth::user()->pin === $request->input('pin')) {
            $resp['status'] = 'success';
        }
        return $this->response->json($resp);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'role' => 'required|in:admin,retailer',
            'name' => ['required', 'regex:/^[\pL\s\-]+$/u', 'max:255'],
            'email' => 'required|email:rfc,dns|unique:users,email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'gender' => 'in:male,female',
            'pin' => ['regex:/^[0-9]{4}$/'],
            'yob' => ['regex:/^[0-9]{4}$/'],
        ]);

        $data = array_merge($request->only('name', 'email', 'password', 'yob', 'gender', 'pin'), [
            'password' => bcrypt($request->input('password')),
            'status' => 1
        ]);
        $user = $this->user->store($data);
        $price_config = $request->input('price_config');
        $this->setPriceConfig($price_config, $user);
        $role = $request->input('role');
        $user->assignRole($role);

        Mail::to($user->email)->queue(new RetailerInvited($user, $request->input('password')));

        return $this->response->item($user);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->response->item($this->user->getById($id));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'             => ['required','regex:/^[\pL\s\-]+$/u', 'max:255'],
            'pin' => ['regex:/^[0-9]{4}$/'],
        ]);

        $updated_user = $this->user->update($id, $request->only('name', 'pin'));
        return $this->response->item($updated_user);
    }
    public function change_prices(Request $request, $id)
    {
        $user = $this->user->getById($id);
         if ($user->hasRole('retailer')) {
            $price_config = $request->input('price_config');
            $this->setPriceConfig($price_config, $user);
        }
        return $this->response->item($user);
    }
    private function setPriceConfig($price_config, $user)
    {

        if ($user->prices->count() === 0) {
            foreach ($price_config as $k => $values) {
                $model = new UsersPriceConfig();
                $values['user_id'] = $user->id;
                $values['location_type'] = $k;
                $model->fill($values);
                $model->save();
            }
        } else {
            foreach ($user->prices as $price) {
                $price->fill($price_config[$price->location_type]);
                $price->save();
            }
        }
    }
    /**
     * Update User Status By User ID
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request, $id)
    {
        $status = $request->input('status');

        if (auth()->user()->id == $id) {
            return $this->response->withUnauthorized('You can\'t change status for yourself!');
        }

        $this->user->update($id, ['status' => $status]);

        return $this->response->json(['status' => $status]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->id == $id) {
            return $this->response->withUnauthorized('You can\'t delete yourself.');
        }

        $this->user->destroy($id);

        return $this->response->withNoContent();
    }

    public function change_password(Request $request, $id)
    {
        $user = $this->user->getById($id);

        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        $user->update([
            'password' => bcrypt($request->password),
        ]);

        return $this->response->withNoContent();
    }
}
