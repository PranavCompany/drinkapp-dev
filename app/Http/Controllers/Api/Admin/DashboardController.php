<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\API\ApiController;
use App\Order;
use App\Support\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends ApiController
{
    public function locations(Request $request)
    {
        $selected_option = $request->input('selected_option');
        $locations = DB::table('orders')
            ->select(DB::raw(' locations.id, users.id, locations.name as location,
            locations.currency,
                                        users.name as retailer, count(distinct orders.user_id, orders.location_id) as customers'))
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('users as u', 'orders.user_id', '=', 'u.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->groupBy(['locations.id', 'users.id']);
        Response::addTimeCondition('orders.created_at', $selected_option, $locations, $request->input('offset'));
        if (isset($request->sortOrder) && isset($request->sortKey)
            && !empty($request->sortKey) && !empty($request->sortOrder)) {
            $locations->orderBy($request->sortKey, $request->sortOrder);
        } else {
            $locations->orderBy('customers', 'desc');
        }
        return $this->response->json($locations->paginate(5));
    }
    public function locations_charts(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('users')
            ->select(DB::raw('concat(users.name, ": ", locations.name) as category, count(orders.id) as data'))
            ->join('locations', 'users.id', '=', 'locations.user_id')
            ->leftJoin('orders', 'orders.location_id', '=', 'locations.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->groupBy(['locations.id', 'users.name'])
            ->having('data', '>', 0)
            ->orderBy('data', 'desc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $data = [];
        foreach ($array as $key => $value) {
            $data[] = [
                'x' => $value->category,
                'y' => $value->data,
            ];
        }
        return $this->response->json([['data' => $data]]);
    }

    public function retailer_charts(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('users')
            ->select(DB::raw('users.name as category, count(orders.id) as data'))
            ->join('locations', 'users.id', '=', 'locations.user_id')
            ->leftJoin('orders', 'orders.location_id', '=', 'locations.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->whereNull('orders.deleted_at')
            ->groupBy(['orders.retailer_id', 'users.name'])
            ->having('data', '>', 0)
            ->orderBy('data', 'desc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $data = [];
        foreach ($array as $key => $value) {
            $data[] = [
                'x' => $value->category,
                'y' => $value->data,
            ];
        }
        return $this->response->json([['data' => $data]]);
    }

    public function income_location(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('orders')
            ->select(DB::raw(' locations.name as location, locations.id, orders.currency, round(sum(total_fee+'.Order::drinkAppFeeQuery().'), 2) as income'))
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('locations', 'locations.id', '=', 'orders.location_id')
            ->where('orders.paid', '=', true)
            ->where('orders.status', '!=', 6)
            ->groupBy(['locations.id', 'orders.currency'])
//            ->orderBy('locations.id', 'asc')
            ->orderBy('income', 'desc');

        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $return = [];
        $temp_data = [];
        $currenies = ['GBP', 'EUR', 'AED', 'USD'];
        foreach ($array as $key => $value) {
            foreach ($currenies as $currency) {
                if (!isset($temp_data[$currency][$value->id])) {
                    $temp_data[$currency][$value->id] = [
                        'y' => 0,
                        'x' => $value->location,
                    ];
                }
            }
            $temp_data[strtoupper($value->currency)][$value->id] = [
                'y' => $value->income,
                'x' => $value->location,
            ];
        }
        foreach ($temp_data as $currency => $value) {
            $return[] = [
                'name' => $currency,
                'data' => array_values($value)
            ];
        }
        if (!count($temp_data)) {
            $return = [
                [
                    'name' => 'GBP',
                    'data' => [],
                ],
                [
                    'name' => 'EUR',
                    'data' => [],
                ],
                [
                    'name' => 'AED',
                    'data' => [],
                ],
                [
                    'name' => 'USD',
                    'data' => [],
                ],
            ];
        }
        return $this->response->json($return);
    }
    public function income_retailer(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('orders')
            ->select(DB::raw(' users.name as retailer, users.id, orders.currency, round(sum(total_fee+'.Order::drinkAppFeeQuery().'),2) as income'))
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->join('locations', 'locations.id', '=', 'orders.location_id')
            ->where('orders.paid', '=', true)->whereNull('orders.deleted_at')
            ->where('orders.status', '!=', 6)
            ->groupBy(['users.id', 'orders.currency'])
            ->orderBy('income', 'desc');

        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $return = [];
        $temp_data = [];
        $currenies = ['GBP', 'EUR', 'AED', 'USD'];
        foreach ($array as $key => $value) {
            foreach ($currenies as $currency) {
                if (!isset($temp_data[$currency][$value->id])) {
                    $temp_data[$currency][$value->id] = [
                        'y' => 0,
                        'x' => $value->retailer,
                    ];
                }
            }
            $temp_data[strtoupper($value->currency)][$value->id] = [
                'y' => $value->income,
                'x' => $value->retailer,
            ];
        }
        foreach ($temp_data as $currency => $value) {
            $return[] = [
                'name' => $currency,
                'data' => array_values($value)
            ];
        }
        if (!count($temp_data)) {
            $return = [
                [
                    'name' => 'GBP',
                    'data' => [],
                ],
                [
                    'name' => 'EUR',
                    'data' => [],
                ],
                [
                    'name' => 'AED',
                    'data' => [],
                ],
                [
                    'name' => 'USD',
                    'data' => [],
                ],
            ];
        }
        return $this->response->json($return);
    }


    public function location_gender(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('orders')
            ->select(DB::raw('locations.id, locations.name as category, sum(if (users.gender = \'male\', 1, 0 )) as male,
            count(distinct orders.user_id, orders.location_id) as total,
             sum(if (users.gender = \'female\', 1, 0 )) as female'))
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->whereNull('orders.deleted_at')
            ->groupBy(['locations.id'])
            ->orderBy('locations.id', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $data = [
            'male' => [
                'name' => 'male',
            ],
            'female' => [
                'name' => 'female',
            ]
        ];
        if (!count($array)) {
            $data['male']['data'] = [];
            $data['female']['data'] = [];
        } else {
            foreach ($array as $key => $value) {
                $data['male']['data'][] = [
                    'x' => $value->category,
                    'y' => $value->male,
                ];
                $data['female']['data'][] = [
                    'x' => $value->category,
                    'y' => $value->female,
                ];
            }
        }
        return $this->response->json(array_values($data));
    }
    public function retailer_gender(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $locations = DB::table('orders')
            ->select(DB::raw('distinct u.id, u.name as category, sum(if (users.gender = \'male\', 1, 0 )) as male,
            count(distinct orders.user_id, orders.location_id) as total,
             sum(if (users.gender = \'female\', 1, 0 )) as female'))
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('users as u', 'locations.user_id', '=', 'u.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->groupBy(['u.id'])
            ->orderBy('u.id', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $data = [
            'male' => [
                'name' => 'male',
            ],
            'female' => [
                'name' => 'female',
            ]
        ];
        if (!count($array)) {
            $data['male']['data'] = [];
            $data['female']['data'] = [];
        } else {
            foreach ($array as $key => $value) {
                $data['male']['data'][] = [
                    'x' => $value->category,
                    'y' => $value->male,
                ];
                $data['female']['data'][] = [
                    'x' => $value->category,
                    'y' => $value->female,
                ];
            }
        }

        return $this->response->json(array_values($data));
    }
    public function location_age(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $condition = "year(now())-users.yob";
        $locations = DB::table('orders')
            ->select(DB::raw('locations.id, locations.name as category,
            count(distinct orders.user_id, orders.location_id) as total,
             sum(if ('.$condition.' between 18 and 25, 1, 0 )) as `18-25`,
             sum(if ('.$condition.' between 26 and 35, 1, 0 )) as `26-35`,
             sum(if ('.$condition.' between 36 and 45, 1, 0 )) as `36-45`,
             sum(if ('.$condition.' > 45, 1, 0 )) as `45+`'
            ))
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->groupBy(['locations.id'])
            ->orderBy('locations.id', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $dict = ['18-25', '26-35', '36-45', '45+'];
        foreach ($dict as $val) {
            $data[$val] = ['name' => $val];
        }
        if (!count($array)) {
            foreach ($dict as $val) {
                $data[$val]['data'] = [];
            }
        } else {
            foreach ($array as $key => $value) {
                foreach ($dict as $val) {
                    $value = (array)$value;
                    $data[$val]['data'][] = [
                        'x' => $value['category'],
                        'y' => $value[$val],
                    ];
                }
            }
        }
        return $this->response->json(array_values($data));
    }
    public function retailer_age(Request $request)
    {
        $date_start = $request->input('dateStart');
        $date_end = $request->input('dateEnd');
        $condition = "year(now())-users.yob";
        $locations = DB::table('orders')
            ->select(DB::raw('distinct u.id, u.name as category,
                 count(distinct orders.user_id, orders.location_id) as total,
                 sum(if ('.$condition.' between 18 and 25, 1, 0 )) as `18-25`,
                 sum(if ('.$condition.' between 26 and 35, 1, 0 )) as `26-35`,
                 sum(if ('.$condition.' between 36 and 45, 1, 0 )) as `36-45`,
                 sum(if ('.$condition.' > 45, 1, 0 )) as `45+`'
            ))
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('users as u', 'orders.retailer_id', '=', 'u.id')
            ->whereNull('locations.deleted_at')
            ->whereNull('users.deleted_at')
            ->whereNull('orders.deleted_at')
            ->groupBy(['u.id'])
            ->orderBy('u.id', 'asc');
        Response::addTimeConditionCharts('orders.created_at', $date_start, $date_end, $locations, $request->input('offset'));
        $array = $locations->get();
        $dict = ['18-25', '26-35', '36-45', '45+'];
        foreach ($dict as $val) {
            $data[$val] = ['name' => $val];
        }
        if (!count($array)) {
            foreach ($dict as $val) {
                $data[$val]['data'] = [];
            }
        } else {
            foreach ($array as $key => $value) {
                foreach ($dict as $val) {
                    $value = (array)$value;
                    $data[$val]['data'][] = [
                        'x' => $value['category'],
                        'y' => $value[$val],
                    ];
                }
            }
        }
        return $this->response->json(array_values($data));
    }

    public function income(Request $request)
    {
        $selected_option = $request->input('selected_option');
        $locations = DB::table('orders')
            ->select(DB::raw(' users.name as retailer,orders.currency, round(sum(total_fee+'.Order::drinkAppFeeQuery().'), 2) as income'))
            ->join('users', 'orders.retailer_id', '=', 'users.id')
            ->where('orders.paid', '=', true)->whereNull('orders.deleted_at')
            ->where('orders.status', '!=', 6)
            ->groupBy(['users.id', 'orders.currency']);
        Response::addTimeCondition('orders.created_at', $selected_option, $locations, $request->input('offset'));
        if (isset($request->sortOrder) && isset($request->sortKey)
            && !empty($request->sortKey) && !empty($request->sortOrder)) {
            $locations->orderBy($request->sortKey, $request->sortOrder);
        } else {
            $locations->orderBy('income', 'desc');
        }
        return $this->response->json($locations->paginate(5));
    }
}
