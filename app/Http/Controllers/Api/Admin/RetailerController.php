<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\API\ApiController;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class RetailerController extends ApiController
{
    protected $retailer;

    public function __construct(UserRepository $user)
    {
        parent::__construct();

        $this->retailer = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response->collection($this->retailer->pageWithRequest($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'role' => 'required|in:admin,admin',
            'name' => 'required|string|max:255',
            'email' => 'required|email:rfc,dns|unique:users,email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'gender' => 'in:male,female',
            'yob' => ['regex:/^[0-9]{4}$/'],
        ]);

        $data = array_merge($request->only('name', 'email', 'password', 'gender', 'yob'), [
            'password' => bcrypt($request->input('password')),
            'status' => 1
        ]);
        $user = $this->user->store($data);

        $role = $request->input('role');
        $user->assignRole($role);

        // return $this->response->withNoContent();
        return $this->response->item($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->response->item($this->user->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => 'required|string|max:255',
//            'email' => 'required|email|unique:users,email,'.$id,
        ]);

        $updated_user = $this->user->update($id, $request->only('name'));

        // return $this->response->withNoContent();
        return $this->response->item($updated_user);
    }

    /**
     * Update User Status By User ID
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request, $id)
    {
        $status = $request->input('status');

        if (auth()->user()->id == $id) {
            return $this->response->withUnauthorized('You can\'t change status for yourself!');
        }

        $this->user->update($id, ['status' => $status]);

        return $this->response->json(['status' => $status]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->id == $id) {
            return $this->response->withUnauthorized('You can\'t delete yourself.');
        }

        $this->user->destroy($id);

        return $this->response->withNoContent();
    }
}
