<?php

namespace App\Http\Controllers\API\Admin;

use App\Location;
use App\Scopes\OwnerScope;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\API\ApiController;

use App\Repositories\LocationRepository;


class LocationController extends ApiController
{
    protected $location;

    public function __construct(LocationRepository $location)
    {
        parent::__construct();

        $this->location = $location;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortColumn = $request->input('sortKey')??'name';
        $sort = $request->input('sortOrder')??'asc';
        return $this->response->collection($this->location->pageWithRequest($request, 10, $sort, $sortColumn));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required|string|max:255',
            'description'   => 'string',
            'latitude'      => 'required|string',
            'longitude'     => 'required|string'
        ]);

        $data = array_merge($request->only('name', 'description', 'latitude', 'longitude'), [
            'status' => 1
        ]);
        $location = $this->location->store($data);

        // return $this->response->withNoContent();
        return $this->response->item($location);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user = $request->user();
        if ($user->hasRole('admin')) {
            return $this->response->item(Location::withoutGlobalScope(OwnerScope::class)->findOrFail($id));
        }
        return $this->response->item($this->location->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required|string|max:255',
            'description'   => 'string',
            'latitude'      => 'required|string|max:255',
            'longitude'     => 'required|string|max:255'
        ]);

        $location = $this->location->update($id, $request->only('name', 'description', 'latitude', 'longitude'));

        // return $this->response->withNoContent();
        return $this->response->item($location);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->location->destroy($id);
        return $this->response->withNoContent();
    }
}
