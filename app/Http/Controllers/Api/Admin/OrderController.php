<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\API\ApiController;
use App\Order;
use App\Repositories\OrderRepository;
use App\Transformers\AdminOrderTransformer;
use Illuminate\Http\Request;


class OrderController extends ApiController
{
    protected $order;

    public function __construct(OrderRepository $order)
    {
        parent::__construct();

        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortColumn = $request->input('sortKey') ?? 'created_at';
        $sort = $request->input('sortOrder') ?? 'desc';
        $date_start = Order::handleDate($request->input('dateStart'), $request->input('offset'));
        $date_end = Order::handleDate($request->input('dateEnd'), $request->input('offset'));
        return $this->response->collection($this->order->pageWithRequestAdmin($request, 10, $sort,
            $sortColumn, $date_start, $date_end),
            new AdminOrderTransformer());
    }

}
