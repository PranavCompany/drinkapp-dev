<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Image;
use App\Item;
use App\Location;
use App\Option;
use App\Repositories\ItemRepository;
use App\Repositories\LocationRepository;
use App\Repositories\OptionsRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ItemController extends ApiController
{
    protected $item;
    protected $locationRepository;
    protected $option;

    public function __construct(ItemRepository $item, LocationRepository $locationRepository, OptionsRepository $option)
    {
        parent::__construct();

        $this->item = $item;
        $this->option = $option;
        $this->locationRepository = $locationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = $this->item->pageWithRequest($request);
        return $this->response->collection($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required|string|max:255',
            'price_eur' => 'nullable|numeric|between:0,999999',
            'price_gbp' => 'nullable|numeric|between:0,999999',
            'price_aed' => 'nullable|numeric|between:0,999999',
            'price_usd' => 'nullable|numeric|between:0,999999'
        ]);

        $data = array_merge($request->only('name', 'description',
            'category_id'), [
            'price_eur' => $request->price_eur ?? 0,
            'price_usd' => $request->price_usd ?? 0,
            'price_aed' => $request->price_aed ?? 0,
            'price_gbp' => $request->price_gbp ?? 0,
            'user_id' => $request->user()->id
        ]);

        $item = $this->item->store($data);

        if ($request->has('image') && $request->input('image') != '') {
            $item->images()->save(new Image(['url' => $request->input('image')]));
        }

        return $this->response->item($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->response->item($this->item->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'price_eur' => 'nullable|numeric|between:0,999999',
            'price_gbp' => 'nullable|numeric|between:0,999999',
            'price_usd' => 'nullable|numeric|between:0,999999',
            'price_aed' => 'nullable|numeric|between:0,999999'
        ]);
        $updated_item = $this->item->update($id, array_merge($request->only('name', 'description',
            'price_eur', 'price_gbp', 'price_usd', 'price_aed', 'category_id'),
            [
                'price_eur' => $request->price_eur ?? 0,
                'price_usd' => $request->price_usd ?? 0,
                'price_aed' => $request->price_aed ?? 0,
                'price_gbp' => $request->price_gbp ?? 0
            ]
        ));

        if ($request->has('image') && $request->input('image_old') == false) {
            $updated_item->images()->delete();
            $image_url = $request->input('image');
            if (isset($image_url) && $image_url != '') {
                $image_url = str_replace(env('APP_URL'), '', $image_url);
                $updated_item->images()->save(new Image(['url' => $image_url]));
            }
        }

        return $this->response->item($updated_item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $this->item->destroy($id);
        return $this->response->withNoContent();
    }


    public function location_items(Request $request, $id)
    {
        $location = Location::find($id);
        if (!$location) {
            return $this->response->withBadRequest('Invalid location');
        }
        $items = Item::select(
            'items.id',
            'items.name',
            'items.description',
            'items.price_eur',
            'items.price_aed',
            'items.price_usd',
            'items.price_gbp',
            'locations.currency',
            DB::raw('if(location_has_items.item_id is null or not exists(select 1 from location_has_categories where location_has_categories.category_id = items.category_id and locations.id = location_has_categories.location_id), 0, 1) as is_active'),
            DB::raw('ifnull(location_has_items.special_offer, 0) as is_special'),
            DB::raw('ifnull(location_has_items.out_of_stock, 0) as out_of_stock'),
            DB::raw('if("' . $location->currency . '" = "gbp",
             items.price_gbp,
              if ("' . $location->currency . '" = "aed", items.price_aed,
              if ("' . $location->currency . '" = "usd", items.price_usd,
               items.price_eur))) as price'),
            DB::raw('if("' . $location->currency . '" = "gbp", "£", if("' . $location->currency . '" = "aed", "د.إ" ,if("' . $location->currency . '" = "usd", "$","€"))) as currency')
        )->leftJoin('location_has_items', function ($join) use ($location) {
            $join->on('location_has_items.item_id', '=', 'items.id');
            $join->on('location_has_items.location_id', '=', DB::raw($location->id));
        })
            ->leftJoin('locations', 'locations.id', '=', 'location_has_items.location_id')
            ->where('items.user_id', Auth::user()->id)
            ->where('items.category_id', (int)$request->input('category'));
        $map = [
            'yes' => 1,
            'no' => 0
        ];
        if (in_array($request->input('active'), array_keys($map))) {
            $items = $items->whereRaw('if(location_has_items.item_id is null or not exists(select 1 from location_has_categories where location_has_categories.category_id = items.category_id and locations.id = location_has_categories.location_id), 0, 1)=?',
                [$map[$request->input('active')]]);
        }
        if (in_array($request->input('out_of_stock'), array_keys($map))) {
            $items = $items->where('location_has_items.out_of_stock', '=', $map[$request->input('out_of_stock')]);
        }
        if (in_array($request->input('is_special'), array_keys($map))) {
            $items = $items->where('location_has_items.special_offer', '=', $map[$request->input('is_special')]);
        }
        if (!empty($request->input('name'))) {
            $items = $items->where('items.name', 'like', '%' . $request->input('name') . '%');
        }
        $items = $items->with('images')->with('options')->with('addons')->distinct();
        if (isset($request->sortOrder) && isset($request->sortKey)
            && !empty($request->sortKey) && !empty($request->sortOrder)) {
            $items = $items->orderBy($request->sortKey, $request->sortOrder);
        }
        return $this->response->json($items->paginate(15));
    }

    public function categories(Request $request)
    {
        $categories = Category::where('user_id', Auth::user()->id)->whereRaw('exists(Select 1
                                    from items
                                    where items.category_id = categories.id
                                    and items.deleted_at is null
                                    )')->get();
        return $this->response->json($categories);
    }

    public function options(Request $request)
    {
        $options = $this->option->pageWithRequest($request);
        return $this->response->collection($options);
    }

    public function options_all(Request $request)
    {
        $keyword = $request->get('keyword');
        $options = Option::when($keyword, function ($query) use ($keyword) {
            $query->where('name', 'like', "%{$keyword}%");
        })->where('user_id', auth()->user()->id)->orderBy('name', 'asc')->get();
        return $this->response->collection($options);
    }

    public function change_data(Request $request)
    {
        $location = $this->locationRepository->getById($request->input('location'));
        if ($location && $request->input('category')) {
            $category = $location->categories()->find($request->input('category'));
            if (!$category) {
                $location->categories()->attach($request->input('category'));
            }
            if ($request->input('field') === 'out_of_stock') {
                $location->items()->updateExistingPivot($request->input('item_id'),
                    ['out_of_stock' => (int)$request->input('value')]);
            }
            if ($request->input('field') === 'is_special') {
                $location->items()->updateExistingPivot($request->input('item_id'),
                    ['special_offer' => (int)$request->input('value')]);
            }
            if ($request->input('field') === 'is_active') {
                if ($request->input('value')) {
                    $location->items()->sync([$request->input('item_id')], false);
                } else {
                    $location->items()->detach($request->input('item_id'));
                }
            }
            return $this->response->withNoContent();
        } else {
            return $this->response->withBadRequest();
        }
    }

    public function mass_action(Request $request)
    {
        $location = $this->locationRepository->getById($request->input('location'));
        if ($location && $request->input('category')) {
            $category_id = $request->input('category');
            $category = $location->categories()->find($category_id);
            $action = $request->input('field');
            $value = $request->input('value');
            if (!$category) {
                $location->categories()->attach($category_id);
            }
            $items = $request->input('items');
            if ($request->input('select_all_in_category')) {
                $items = Item::select('id')->where('category_id', $category_id)->where('user_id',
                    Auth::user()->id)->get('id')->toArray();
                $items = array_map(function ($item) {
                    return $item['id'];
                }, $items);
            }
            if ($action === 'active') {
                if ($value) {
                    $location->items()->sync($items, false);
                } else {
                    $location->items()->detach($items);
                }
            } else {
                $update = [];
                if ($action === 'out_of_stock') {
                    foreach ($items as $item) {
                        $update[$item] = ['out_of_stock' => $value];
                    }
                } elseif ($action === 'is_special') {
                    foreach ($items as $item) {
                        $update[$item] = ['special_offer' => $value];
                    }
                }
                $location->items()->sync($update, false);
            }
        }
    }

    public function autocomplete(Request $request)
    {
        $q = urldecode($request->get('q'));
        $items = Item::where('user_id', '=', Auth::user()->id)
            ->where('name', 'like', "%{$q}%")->limit(10)->distinct()->get(['name', 'category_id']);
        return $this->response->json($items);
    }

    public function update_price(Request $request)
    {
        $item = Item::find($request->input('item_id'));
        $location = Location::find($request->input('location_id'));
        if ($item && $location) {
            $price = (float)$request->input('price');
            $field = 'price_' . $location->currency;
            $item->$field = $price;
            $item->save();
        } elseif ($item && $request->input('field_name') && in_array($request->input('field_name'),
                ['price_gbp', 'price_eur', 'price_aed', 'price_usd'])) {
            $field = $request->input('field_name');
            $item->$field = (float)$request->input('price');
            $item->save();
        } else {
            return $this->response->withBadRequest();
        }
    }
}
