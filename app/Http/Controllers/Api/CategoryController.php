<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Image;
use App\Item;
use App\Repositories\CategoryRepository;
use App\Scopes\OwnerScope;
use App\Transformers\CategoryTransformer;
use App\Transformers\CategoryTransformerEdit;
use App\Transformers\CategoryTransformerList;
use App\Transformers\CategoryTransformerNames;
use App\Transformers\ItemTransformerNames;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends ApiController
{
    /**
     * @var $category CategoryRepository
     **/
    protected $category;

    public function __construct(CategoryRepository $category)
    {
        parent::__construct();

        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->response->collection($this->category->pageWithRequest($request), new CategoryTransformerList());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        $user = $request->user();
        if ($user->hasRole('admin')) {
            $id = $request->input('location_id');
            if (!empty($id)) {
                return $this->response->collection(
                    Category::withoutGlobalScope(OwnerScope::class)->whereExists(function ($query) use ($id) {
                        $query->select(DB::raw(1))
                            ->from('location_has_categories')
                            ->where('location_id', '=', $id)
                            ->whereRaw('category_id = categories.id');
                    })->orderBy('name', 'asc')->get(), new CategoryTransformer());
            }
        }
        return $this->response->collection($this->category->getList());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function for_items(Request $request)
    {
        $user = $request->user();
        if ($user->hasRole('admin')) {
            $id = $request->input('location_id');
            if (!empty($id)) {
                return $this->response->collection(
                    Category::withoutGlobalScope(OwnerScope::class)->whereExists(function ($query) use ($id) {
                        $query->select(DB::raw(1))
                            ->from('location_has_categories')
                            ->where('location_id', '=', $id)
                            ->whereRaw('category_id = categories.id');
                    })->orderBy('name', 'asc')->get(), new CategoryTransformer());
            }
        }
        return $this->response->collection($this->category->getList(true));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function parent(Request $request)
    {
        $categories = Category::whereNull('parent_id')->whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('items')
                ->whereRaw('categories.id = items.category_id');
        })->get();
        return $this->response->collection($categories, new CategoryTransformerNames());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function child(Request $request)
    {
        $categories = Category::whereExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('items')
                ->whereRaw('categories.id = items.category_id');
        })->get();
        return $this->response->collection($categories, new CategoryTransformerNames());
    }

    public function items(Request $request)
    {
        $items = Item::where('user_id', '=', auth()->user()->id)->orderBy('category_id')->get();
        return $this->response->collection($items, new ItemTransformerNames());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255'
        ]);

        $data = array_merge($request->only('name', 'description', 'type', 'parent_id'), [
            'user_id' => $request->user()->id
        ]);
        if ($request->input('category_type') !== 'items') {
            $data['parent_id'] = null;
        }
        if (empty($data['type'])) {
            $data['type'] = 'drinks';
        }
        $category = $this->category->store($data);
        $this->handleRequest($request, $category->id);
        if ($request->has('image') && $request->input('image') != '') {
            $category->images()->save(new Image(['url' => $request->input('image')]));
        }
        Category::updateLinks($category);
        return $this->response->item($category);
    }

    private function handleRequest(Request $request, $id)
    {

        if ($request->input('category_type') === 'items') {
            Item::whereIn('id', $request->input('items'))
                ->where('user_id', '=', auth()->user()->id)
                ->update(['category_id' => $id]);
            Item::whereNotIn('id', $request->input('items'))
                ->where('category_id', '=', $id)
                ->where('user_id', '=', auth()->user()->id)
                ->update(['category_id' => null]);
            Category::where('parent_id', $id)
                ->where('user_id', '=', auth()->user()->id)
                ->update(['parent_id' => null]);
        } else {
            Category::whereIn('id', $request->input('children'))
                ->where('user_id', '=', auth()->user()->id)
                ->update(['parent_id' => $id]);
            Category::whereNotIn('id', $request->input('children'))
                ->where('user_id', '=', auth()->user()->id)
                ->where('parent_id', '=', $id)
                ->update(['parent_id' => null]);
            Item::where('category_id', $id)
                ->where('user_id', '=', auth()->user()->id)
                ->update(['category_id' => null]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $category = $this->category->getById($id);

        // $this->authorize('update', $category);
        if ($request->user()->can('update', $category)) {
            return $this->response->item($category, new CategoryTransformerEdit);
        } else {
            return $this->response->withBadRequest('You have no permission to edit this category');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255'
        ]);
        $data = $request->only('name', 'description', 'type', 'parent_id');
        if ($request->input('category_type') !== 'items') {
            $data['parent_id'] = null;
        }
        $updated_category = $this->category->update($id, $data);
        $this->handleRequest($request, $id);
        if ($request->has('image') && $request->input('image_old') == false) {
            $updated_category->images()->delete();
            $image_url = $request->input('image');
            if (isset($image_url) && $image_url != '') {
                $image_url = str_replace(env('APP_URL'), '', $image_url);
                $updated_category->images()->save(new Image(['url' => $image_url]));
            }
        }
        Category::updateLinks($updated_category);
        return $this->response->item($updated_category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->category->destroy($id);
        return $this->response->withNoContent();
    }
}
