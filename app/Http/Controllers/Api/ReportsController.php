<?php

namespace App\Http\Controllers\API;

use App\Location;
use App\Order;
use Illuminate\Http\Request;

class ReportsController extends ApiController
{

    public function locations(Request $request) {
        $locations = Location::where('locations.user_id', '=', $request->user()->id)->get();
        return $this->response->json($locations);
    }

    public function sold(Request $request)
    {
        $location = $request->input('location');
        $date_start = Order::handleDate($request->input('dateStart'), $request->input('offset'), 'Y-m-d', true);
        $date_end = Order::handleDate($request->input('dateEnd'), $request->input('offset'), 'Y-m-d');
        $user_id = Auth()->user()->id;
        $sort_key = $request->sortKey;
        $sort_order = $request->sortOrder;

        $items = Order::generateReportSoldQuery($user_id, $location, $date_start, $date_end, $sort_key, $sort_order);
        $items = $items->get();
        $total = [
            'eur' => 0,
            'gbp' => 0,
            'aed' => 0,
            'usd' => 0,
        ];
        foreach ($items as &$item) {
            $this->setAddons($item, $total);
        }
        $totals = [];
        foreach ($total as $k => $v) {
            if ($v > 0) {
                $totals[] = Location::currencyToSymbol($k).$v;
            }
        }
        return $this->response->json(['items' =>$items, 'total' => $totals]);
    }

    private function setAddons(&$item, &$total)
    {
        $addons = json_decode($item->addons, true);
        $item->total_addons = 0;
        $item_addons = [];
        if (!empty($addons)) {
            foreach ($addons as $order_addon) {
                if (!empty($order_addon['addons'])) {
                    foreach ($order_addon['addons'] as $item_addon) {
                        $key = $item_addon['name'].'_'.$item_addon['price'];
                        $item_addon['quantity'] = (int)$order_addon['quantity'];
                        if (!isset($item_addons[$key])) {
                            $item_addons[$key] = $item_addon;
                        } else {
                            $item_addons[$key]['quantity'] += (int)$order_addon['quantity'];
                        }
                        $addons_q = $item_addon['price']*$order_addon['quantity'];
                        $total[$item->currency] += $addons_q;
                        $item->total_addons += $addons_q;
                    }
                }
            }
        }
        $item->addons = array_values($item_addons);
        $total[$item->currency] += $item->total;
    }
    public function export(Request $request)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=export.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $location = $request->input('location');
        $date_start = Order::handleDate($request->input('dateStart'), $request->input('offset'), 'Y-m-d', true);
        $date_end = Order::handleDate($request->input('dateEnd'), $request->input('offset'), 'Y-m-d');
        $user_id = Auth()->user()->id;
        $sort_key = $request->sortKey;
        $sort_order = $request->sortOrder;

        $items = Order::generateReportSoldQuery($user_id, $location, $date_start, $date_end, $sort_key, $sort_order);
        $map = [
            [
                'name' => 'Category',
                'callback' => function ($item) {
                    return $item->category;
                },
            ],
            [
                'name' => 'Item',
                'callback' => function ($item) {
                    return $item->item;
                },
            ],
            [
                'name' => 'Quantity',
                'callback' => function ($item) {
                    return $item->quantity;
                },
            ],
            [
                'name' => 'Price',
                'callback' => function ($item) {
                    return (float)$item->price;
                },
            ],
            [
                'name' => 'Addons',
                'callback' => function ($item) {
                    if (empty($item->addons)) {
                        return '';
                    }
                    return implode("\n", array_map(function ($a) {
                        return $a['name'] . ' - ' . $a['price'] . ' X ' . $a['quantity'];
                    }, $item->addons));
                },
            ],
            [
                'name' => 'Addons Total',
                'callback' => function ($item) {
                    return (float)$item->total_addons;
                },
            ],
            [
                'name' => 'Total Items',
                'callback' => function ($item) {
                    return (float)$item->total;
                },
            ],
            [
                'name' => 'Total',
                'callback' => function ($item) {
                    return (float)$item->total+(float)$item->total_addons;
                },
            ],
            [
                'name' => 'Currency',
                'callback' => function ($item) {
                    return Location::currencyToSymbol($item->currency);
                },
            ],
        ];
        $titles = [];
        foreach ($map as $field) {
            $titles[] = $field['name'];
        }
        $return = [$titles];
        $total = [
            'eur' => 0,
            'gbp' => 0,
            'aed' => 0,
            'usd' => 0,
        ];
        foreach ($items->get() as $item) {
            $this->setAddons($item, $total);
            $temp = [];
            foreach ($map as $field) {
                $temp[] = $field['callback']($item);
            }
            $return[] = $temp;
        }
        $return[] = ['', '', '','Total',$total['eur'], Location::currencyToSymbol('eur')];
        $return[] = ['', '','','',$total['gbp'], Location::currencyToSymbol('gbp')];
        $return[] = ['', '','','',$total['aed'], Location::currencyToSymbol('aed')];
        $return[] = ['', '','','',$total['usd'], Location::currencyToSymbol('usd')];
        $callback = function() use ($return) {
            $file = fopen('php://output', 'w');
            foreach ($return as $column) {
                fputcsv($file, $column);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
