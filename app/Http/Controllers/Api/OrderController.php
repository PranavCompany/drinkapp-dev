<?php

namespace App\Http\Controllers\API;

use App\Location;
use App\Order;
use App\Repositories\OrderRepository;
use App\Transformers\OrderTransformer;
use Carbon\Carbon;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use OneSignal;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Refund;
use Stripe\Stripe;
use Stripe\Transfer;

class OrderController extends ApiController
{
    protected $order;

    public function __construct(OrderRepository $order)
    {
        parent::__construct();

        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $keyword = $request->input('keyword');
        $date_start = Order::handleDate($request->input('dateStart'), $request->input('offset'));
        $date_end = Order::handleDate($request->input('dateEnd'), $request->input('offset'));
        $orders = $user->accepted_orders();
        if (isset($request->sortOrder) && isset($request->sortKey)
            && !empty($request->sortKey) && !empty($request->sortOrder)) {
            $orders->orderBy($request->sortKey, $request->sortOrder);
        } else {
            $orders->orderBy('created_at', 'desc');
        }
        if ($date_start === $date_end && !empty($date_start)) {
            $date_start = Carbon::createFromFormat('Y-m-d H:i:s', $date_start)->addHours(-24)->format('Y-m-d H:i:s');
        }
        $orders->when($keyword, function ($query) use ($keyword) {
            if ((int)$keyword == $keyword && (int)$keyword > 0) {
                $query->where('orders.id', '=', $keyword);
            } else {
                $query->whereExists(function ($q) use($keyword) {
                    $q->select(DB::raw(1))
                        ->from('users')
                        ->whereRaw('orders.user_id = users.id')
                        ->where('email', 'like' ,'%'.$keyword.'%');
                });
            }
        });
        $orders->when(!empty($date_start) && !empty($date_end), function ($query) use ($date_start, $date_end) {
            $query->whereBetween('created_at', [$date_start, $date_end]);
        });
        return $this->response->collection($orders->paginate(10), new OrderTransformer);
    }
    public function total_today(Request $request)
    {
        $user = $request->user();
        if (Carbon::now()->startOfDay()->addHour(3) < Carbon::now()) {
            $date_start = Carbon::now()->startOfDay()->addHour(3);
            $date_end = Carbon::now()->startOfDay()->addDay(1)->addHour(3);
        } else {
            $date_start = Carbon::now()->startOfDay()->addDay(-1)->addHour(3);
            $date_end = Carbon::now()->startOfDay()->addHour(3);
        }
        $orders = DB::table('orders')
            ->selectRaw("round(sum(".Order::orderRevenueQuery()."), 2) as total, currency, sum(tips) as tips")
            ->where('retailer_id', '=', $user->id)
            ->where('paid', '=', 1)
            ->where('orders.status', '!=', 6)
            ->whereBetween('created_at', [$date_start, $date_end])
            ->groupBy('currency')->orderBy('currency')->get()->toArray();
        $data = '';
        if (!empty($orders)) {
            $data = 'Total takings for today: '.Location::currencyToSymbol($orders[0]->currency).' '.$orders[0]->total;
            if (isset($orders[1])) {
                $data .=' / '.Location::currencyToSymbol($orders[1]->currency).' '.$orders[1]->total;
            }
            $data .= '<br>';
            $data .= 'Total tips for today: '.Location::currencyToSymbol($orders[0]->currency).' '.$orders[0]->tips;
            if (isset($orders[1])) {
                $data .=' / '.Location::currencyToSymbol($orders[1]->currency).' '.$orders[1]->tips;
            }
        } else {
            $data = 'Total takings for today: '.Location::currencyToSymbol('eur').' 0'
                .' / '.Location::currencyToSymbol('gbp').' 0'
                .' / '.Location::currencyToSymbol('usd').' 0'.
                ' / 0'.Location::currencyToSymbol('aed');
        }
        return $this->response->json(['orders_total'=> $data]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'price' => 'required'
        ]);

        $data = array_merge($request->only('name', 'description', 'price', 'category_id'), [
            'user_id'   => $request->user()->id
        ]);

        $order = $this->order->store($data);

        return $this->response->order($order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->response->order($this->order->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'price' => 'required'
        ]);

        $updated_order = $this->order->update($id, $request->only('name', 'description', 'price', 'category_id'));

        return $this->response->order($updated_order);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->order->destroy($id);
        return $this->response->withNoContent();
    }


    public function can_print(Request $request, $id)
    {
        return $this->response->json(['status' => Order::where('id', $id)->where('printed', 0)->exists()]);
    }
    public function set_printed(Request $request, $id)
    {
        $order = $this->order->getById($id);
        $order->printed = true;
        $order->save();
        return $this->response->withNoContent();
    }

    public function change_status(Request $request, $id)
    {
        /** @var $order Order */
        $order = $this->order->getById($id);
        if ($request->status === 'ready' && (int)$order->status === 0) {
            if (!empty($order->delivery_address)) {
                $order->status = 5;
            } else {
                if (!empty($order->table_number)) {
                    $order->status = 3;
                } else {
                    $order->status = 2;
                }
            }
            if (env('APP_ENV') !== 'local') {
                if (!empty($order->user()->get()[0]->player_id)) {
                    if (env('APP_ENV') !== 'local') {
                        try {
                            if ((int)$order->status === 2) {
                                \OneSignal::sendNotificationToUser("Your order can now be collected at the DrinkApp collection point",
                                    $order->user()->get()[0]->player_id, null,
                                    ["order_id" => $order->id]);
                            } else {
                                if ($order->order_items()->where('item_type', '=', 'food')->exists()) {
                                    \OneSignal::sendNotificationToUser('Thank you for your order it will be with you soon',
                                        $order->user()->get()[0]->player_id, null,
                                        ["order_id" => $order->id]);
                                } else {
                                    \OneSignal::sendNotificationToUser('Your order will be ready in ' . (int)$request->minutes . ' minutes',
                                        $order->user()->get()[0]->player_id, null,
                                        ["order_id" => $order->id]);
                                }

                            }
                        } catch (ConnectException $exception) {

                        }
                    }
                }
            }
            $order->save();
        } elseif ($request->status === 'complete' && ((int)$order->status === 3 || (int)$order->status === 2 || (int)$order->status === 5)) {
            $old_status = (int)$order->status;
            $order->status = 4;
            $order->queue = 0;
            $order->completed_at = Carbon::now();
            $order->save();
            if (env('APP_ENV') !== 'local') {
                try {
                    if ($old_status === 3 && $order->user()->get()[0]->player_id) {
                        \OneSignal::sendNotificationToUser($order->user()->get()[0]->name . ", Your order was delivered, cheers!",
                            $order->user()->get()[0]->player_id, null,
                            ["order_id" => $order->id]);
                    }
                } catch (ConnectException $exception) {

                }
            }
        } elseif ($request->status === 'refund' && $order->payment_id && $order->paid && $order->status < 6) {
            $order->status = 6;
            Stripe::setApiKey(env('STRIPE_SECRET'));
            try {
                if ($order->charge_type === 'custom') {
                    $payment = PaymentIntent::retrieve($order->payment_id);
                    Refund::create(['payment_intent' => $order->payment_id]);
                    foreach ($payment->charges->data as $charge) {
                        if ($charge->paid && $charge->transfer) {
                            Transfer::retrieve($charge->transfer)->reversals->create();
                        }
                    }
                } else {
                    Refund::create(['payment_intent' => $order->payment_id, 'refund_application_fee' => true],
                        ['stripe_account' => auth()->user()->stripe_standard_account]);
                }

            } catch (ApiErrorException $e) {
                return $this->response->withBadRequest($e->getMessage());
            }
            $order->save();
            if (env('APP_ENV') !== 'local') {
                try {
                    if ($order->user()->get()[0]->player_id) {
                        \OneSignal::sendNotificationToUser($order->user()->get()[0]->name . ", Your order was refunded",
                            $order->user()->get()[0]->player_id, null,
                            ["order_id" => $order->id]);
                    }
                } catch (ConnectException $exception) {

                }
            }
            return $this->response->withNoContent();
        } else {
            return $this->response->withBadRequest('Order don\'t have payment id');
        }
    }
    public function notification(Request $request, $id)
    {
        /** @var $order Order*/
        $order = $this->order->getById($id);
        if ((int)$order->status === 0 && !empty($order->user()->get()[0]->player_id)) {
            if (env('APP_ENV') !== 'local') {
                try {
                    if ($request->minutes == 'confirm' || (int)$request->minutes == 0) {
                        \OneSignal::sendNotificationToUser(
                            'Your order will be ready soon.',
                            $order->user()->get()[0]->player_id,
                            null,
                            ["order_id" => $order->id]
                        );
                    } else {
                        \OneSignal::sendNotificationToUser(
                            'Your order will be ready in ' . (int)$request->minutes . ' minutes',
                            $order->user()->get()[0]->player_id,
                            null,
                            ["order_id" => $order->id]
                        );
                    }
                } catch (ConnectException $exception) {

                }
            }
        }
    }
}
