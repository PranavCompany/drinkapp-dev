<?php

namespace App\Http\Controllers;

use App\UsersPriceConfig;
use Illuminate\Http\Request;
use DB;
use App\User;
use Illuminate\Support\Facades\Log;
use Stripe\ApplicationFee;
use Stripe\Card as StripeCard;
use Stripe\Customer as StripeCustomer;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class TestController extends Controller
{
    public function getQueryString(Request $request)
    {
    	$user = $request->user();
    	$orders = DB::table('orders')
                    ->join('locations', function($join) use ($user){
                        $join->on('orders.location_id', '=', 'locations.id');
                    })
                    ->select('orders.*')
                    ->whereNull('orders.deleted_at')
                    ->whereNull('locations.deleted_at')
                    ->where('locations.user_id', $user->id)
                    ->paginate(10);

        $query = User::withoutGlobalScope()->find(2)->with('locations')->toSql();

    	echo $query;
    	// dd(DB::getQueryLog());
    }

    public function payment_methods(Request $request)
    {
//
//        $price = UsersPriceConfig::where('user_id', '1576')->where('location_type', 'off-license')->first();
//        var_dump($price->calculateApplicationFee('eur', $request->input('sum')));
//        exit();
//        $card_id = 'pm_1HTR8SAezvLQYwRiau4MJrLq';
//        $customer = 'cus_I3YEj4nHQkYsnd';
//        $payment_method_ = $card_id;
//        $acc = 'acct_1HTQudELzz5MAGhL';
//        try {
//            $payment_method = \Stripe\PaymentMethod::create([
//                'payment_method' => $card_id,
//                'customer' => $customer,
//            ], [
//                'stripe_account' => $acc,
//            ]);
//            $payment_method_ = $payment_method->id;
//        } catch (\Stripe\Exception\ApiErrorException $e) {
//            var_dump($e);
//            exit();
//        }
////        try {
////            $token = \Stripe\Token::create([
////                'customer' => $customer,
////                'card' => $card_id,
////            ], [
////                'stripe_account' => $acc,
////            ]);
////        } catch (\Stripe\Exception\ApiErrorException $e) {
////            var_dump($e);
////            exit();
////        }
////        try {
////            $customer = \Stripe\Customer::create([
////                'source' => $customer,
////            ], [
////                'stripe_account' => $acc,
////            ]);
////
////        } catch (\Stripe\Exception\ApiErrorException $e) {
////            var_dump($e);
////            exit();
////        }
//
//        $grandtotal_price = 110;
//        $total_fee = 10;
//        $payment_array = [
//            "amount" => round($grandtotal_price * 100),
//            "currency" => 'usd',
//            'application_fee_amount' => round($total_fee * 100),
//            'payment_method' => $payment_method_,
//            'payment_method_types' => ['card'],
//            'confirmation_method' => 'automatic',
//            'confirm' => true,
//        ];
//        Log::info('Payment array'. print_r($payment_array, true));
//        try {
//            $payment = PaymentIntent::create($payment_array, ['stripe_account' => $acc]);
//        } catch (\Stripe\Exception\InvalidRequestException $e) {
//            $body = $e->getJsonBody();
//            $err = $body['error'];
//            $error = $err['message'];
//            var_dump($error);
//            exit();
//        }
//        var_dump($payment);
    }

    public function add_payment_methods(Request $request)
    {
        $user = auth()->user();
        $user->updateCard($request->input('token'));
        $cards = $user->cards();
        info(json_encode($cards));

        return response()->json($cards);
    }

    public function order(Request $request)
    {
        return view('tests.order');
    }

    public function submit_order(Request $request)
    {

    }

    public function pay_order(Request $request)
    {

    }
}
