<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url',
        'mime_type',
    ];

    /**
     * Get all of the owning imageable models.
     */
    public function imageable()
    {
        return $this->morphTo();
    }

//    public function getAttribute env('APP_URL')
    public function getUrlAttribute($value)
    {
        if ($this->imageable_type === 'App\Item') {
            $file_name = str_replace('/public_uploads/', '', $value);
            $thumb_url = str_replace('images/',  'images/128x128_', $file_name);
            if (\Illuminate\Support\Facades\Storage::disk('local')->exists($file_name)) {
                if (!\Illuminate\Support\Facades\Storage::disk('local')->exists($thumb_url)) {
                    $img = \Intervention\Image\Facades\Image::make(\Illuminate\Support\Facades\Storage::disk('local')->get($file_name))->fit(128);
                    $str  = \Illuminate\Support\Facades\Storage::disk('local')->put($thumb_url, $img->encode());
                    return env('APP_URL').'/public_uploads/'.$thumb_url;
                } else {
                    return env('APP_URL').'/public_uploads/'.$thumb_url;
                }
            }
        }
        return env('APP_URL').$value;
    }

    public function getOriginalUrlAttribute($value) {
        return env('APP_URL').$value;
    }
}
