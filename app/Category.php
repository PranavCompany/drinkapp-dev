<?php

namespace App;

use App\Scopes\OwnerScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Category extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['parent'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'type',
        'description',
        'parent_id'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OwnerScope());
    }

    public static function updateLinks($model)
    {
        $locations_ids = [];
        foreach ($model->locations as $location) {
            $locations_ids[] = $location->id;
        }
        foreach (Location::get('id')->toArray() as $loc) {
            if (!in_array($loc['id'], $locations_ids)) {
                $model->locations()->attach($loc['id']);
            }
        }
    }

    /**
     * Get the user for the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function locations()
    {
        return $this->belongsToMany('App\Location', 'location_has_categories')->withPivot(['time_start', 'time_end']);
    }

    /**
     * Get items for the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\Item')->with('images')->orderBy('name', 'asc');
    }

    /**
     * Get the images for the category
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphMany
     */
    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id', 'id')->with('parent');
    }
    public function getChildrenExistsAttribute()
    {
        return $this->hasMany('App\Category', 'parent_id', 'id')->exists();
    }

    public function children_filtered($location_id)
    {
        $data = $this->hasMany('App\Category', 'parent_id', 'id')
            ->with('parent')
            ->with('locations')
            ->where('type', '=', $this->type)
            ->whereRaw('exists(Select 1
                                    from location_has_items
                                        where  location_has_items.location_id = ?
                                    and item_id in (
                                    select id from items where items.category_id = categories.id
                                    and items.deleted_at is null
                                    )
                                    )', [$location_id])
            ->whereRaw('
            exists(SELECT 1 from location_has_categories where location_id=? and category_id=categories.id)',
                $location_id)->get();
        $resp = $data->toArray();
        foreach ($resp as &$item) {
            foreach ($item['locations'] as $location) {
                if ((int)$location['id'] !== (int)$location_id) {
                    continue;
                }
                $item['time_start'] = $location['pivot']['time_start'];
                $item['time_end'] = $location['pivot']['time_end'];
            }
            unset($item['locations']);
        }
        return $resp;
    }

    public function parent()
    {
        return $this->hasOne('App\Category', 'id', 'parent_id');
    }

    /**
     * Get the created_at and return formatted date value
     *
     * @param string $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('j M Y');
    }

    public function getDisplayNameAttribute($value)
    {
        if ($this->parent) {
            return $this->parent->name . ': ' . $this->name;
        } else {
            return $this->name;
        }
    }

    public function toArray()
    {
        $array = parent::toArray();
        $array['display_name'] = $this->display_name;
        $array['children_exists'] = $this->children_exists;
        return $array;
    }
}
