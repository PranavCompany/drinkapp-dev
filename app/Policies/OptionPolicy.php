<?php

namespace App\Policies;

use App\Option;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OptionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the category.
     *
     * @param  \App\User  $user
     * @param  \App\Option  $option
     * @return mixed
     */
    public function update(User $user, Option $option)
    {
        return $user->hasRole('admin') || $user->id === $option->user_id;
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param  \App\User  $user
     * @param  \App\Option  $option
     * @return mixed
     */
    public function delete(User $user, Option $option)
    {
        return $user->hasRole('admin') || $user->id === $option->user_id;
    }
}
