<?php

namespace App\Policies;

use App\Addon;
use App\Option;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AddonPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the category.
     *
     * @param  \App\User  $user
     * @param  \App\Addon  $option
     * @return mixed
     */
    public function update(User $user, Addon $addon)
    {
        return $user->hasRole('admin') || $user->id === $addon->user_id;
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param  \App\User  $user
     * @param  \App\Addon  $addon
     * @return mixed
     */
    public function delete(User $user, Option $addon)
    {
        return $user->hasRole('admin') || $user->id === $addon->user_id;
    }
}
