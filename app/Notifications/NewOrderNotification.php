<?php

namespace App\Notifications;

use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class NewOrderNotification extends Notification
{
    use Queueable;

    public $order;
    public $user;
    /**
     * Create a new notification instance.
     * @param $order Order
     * @param $user User
     * @return void
     */
    public function __construct($order, $user)
    {
        $this->order = $order;
        $this->user = $user;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        $location = $this->order->location;
        $currency = $this->order->currency_symbol;
        $message = (new MailMessage)
            ->greeting("Hi " . $this->user->name . ",")
            ->line(Lang::getFromJson('Thanks for purchasing at “'.$this->order->location->name.'” in '.$this->order->location->city.'.'))
            ->subject(Lang::getFromJson('New order at “'.$this->order->location->name.'” in '.$this->order->location->city.'.'))
            ->action(Lang::getFromJson('Contact DrinkApp Support'), url(config('app.url')));
        foreach ($this->order->order_items as $order_item) {
            $message->line('|'.$order_item->item_name.'|x '.$order_item->quantity.'|'.$currency.$order_item->item_price.'|');
        }
        $message->line('| |Transaction Fee: 3%|'.$currency.$this->order->total_fee.'|');
        $message->line('| |TOTAL:|'.$currency.$this->order->grandtotal_price.'|');
        return $message->markdown('mail.neworder');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
