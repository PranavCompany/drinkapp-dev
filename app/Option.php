<?php

namespace App;

use App\Scopes\OwnerScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Option extends Model
{
    use SoftDeletes;

    protected $hidden = ['pivot','user_id' ,'created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $fillable = ['name', 'values', 'user_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'values' => 'json',
    ];

    /**
     * Get items for the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\Item');
    }

}
