<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use App\Scopes\OwnerScope;
use Illuminate\Support\Facades\DB;

class Item extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'category_id',
        'name',
        'description',
        'price_eur',
        'price_gbp',
        'price_aed',
        'price_usd',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        self::updated(function($model){
            self::updateLinks($model);
        });
        self::created(function($model){
            self::updateLinks($model);
        });
        // static::addGlobalScope(new OwnerScope());
    }

    private static function updateLinks($model)
    {
        $location_ids = [];
        foreach ($model->locations as $location) {
            $location_ids[] = $location->id;
        }
        if ($model->category) {
            foreach ($model->category->locations as $location) {
                if (($key = array_search($location->id, $location_ids)) !== false) {
                    unset($location_ids[$key]);
                }
            }
            $model->category->locations()->sync($location_ids, false);
        }
    }

    /**
     * Get the user for the item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function locations()
    {
        return $this->belongsToMany('App\Location', 'location_has_items');
    }

    /**
     * Get the category for the item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get the images for the item
     *
     * @return \Illuminate\Database\Eloquent\Relations\morphMany
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * Get items for the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function options()
    {
        return $this->belongsToMany('App\Option', 'item_option');
    }

    /**
     * Get items for the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function addons()
    {
        return $this->belongsToMany('App\Addon', 'item_addon');
    }

    /**
     * Get the created_at and return formatted date value
     *
     * @param string $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('j M Y');
    }


    public static function canUseDiscount()
    {
        return DB::raw('
                not exists(select 1 from location_has_categories
                where
                locations.id=location_has_categories.location_id and
                items.category_id=location_has_categories.category_id and
                location_has_categories.time_start is not null and
                location_has_categories.time_end is not null
                ) as can_use_discount');
    }

}
