<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'gender' => $user->gender,
            'yob' => $user->yob,
            'pin' => $user->pin,
            'role_names' => $user->role_names,
            'currencies' => $user->currencies_check,
            'price_config' => $user->price_config,
            // 'is_admin' => $user->is_admin,
            // 'is_retailer' => $user->is_retailer,
            'stripe_acc_verified' => $user->stripe_acc_verified,
            'custom_account_disconnected' => $user->custom_account_disconnected,
            'stripe_standard_account_verified' => $user->stripe_standard_account_verified,
            'status' => $user->status,
            'created_at' => $user->created_at,
        ];
    }
}
