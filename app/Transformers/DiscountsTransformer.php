<?php

namespace App\Transformers;

use App\Discounts;
use League\Fractal\TransformerAbstract;

class DiscountsTransformer extends TransformerAbstract
{
    public function transform(Discounts $discount)
    {
        return [
            'id' => $discount->id,
            'code' => $discount->code,
            'user' => $discount->user,
            'discount' => $discount->discount,
            'location' => $discount->location,
            'created_at' => $discount->created_at,
        ];
    }
}
