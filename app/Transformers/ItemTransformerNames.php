<?php

namespace App\Transformers;

use App\Item;
use League\Fractal\TransformerAbstract;

class ItemTransformerNames extends TransformerAbstract
{
    public function transform(Item $item)
    {
        return [
            'id' => $item->id,
            'name' => $item->name,
        ];
    }
}
