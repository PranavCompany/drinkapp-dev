<?php

namespace App\Transformers;

use App\Option;
use League\Fractal\TransformerAbstract;

class OptionTransformer extends TransformerAbstract
{
    public function transform(Option $option)
    {
        return [
            'id' => $option->id,
            'name' => $option->name,
            'values' => $option->values,
        ];
    }
}
