<?php

namespace App\Transformers;

use App\Location;
use League\Fractal\TransformerAbstract;

class LocationTransformerClosest extends TransformerAbstract
{
    public function transform(Location $location)
    {
        return [
            'id' => $location->id,
            'user_id' => $location->user_id,
            'name' => $location->name,
            'description' => $location->description,
            'phone' => $location->phone,
            'address' => $location->address,
            'city' => $location->city,
            'state' => $location->state,
            'country' => $location->country,
            'latitude' => $location->latitude,
            'longitude' => $location->longitude,
            'status' => $location->status,
            'images' => $location->images,
            'currency' => $location->currency,
            'to_table' => $location->to_table_auto,
            'to_home' => $location->to_home_auto,
            'collection' => $location->collection_auto,
            'location_type' => $location->location_type,
            'home_delivery_radius' => $location->home_delivery_max_radius,
            'home_delivery_min_radius' => $location->home_delivery_min_radius,
            'home_delivery_max_radius' => $location->home_delivery_max_radius,
            'home_delivery_price' => $location->home_delivery_price_old,
            'home_delivery_min_price' => $location->home_delivery_min_price,
            'home_delivery_max_price' => $location->home_delivery_max_price,
            'home_delivery_minimum_order' => $location->home_delivery_minimum_order,
            'price_config' => $location->real_price_config,
            'currency_symbol' => $location->currency_symbol,
            'average_order_time' => $location->average_order_time,
            'special_offers' => $location->special_offers,
            'rating' => $location->rating,
            'created_at' => $location->created_at_db,
            'tips_type' => $location->tips_type,
            'tips_list' => $location->tips_list_real,
            'updated_at' => $location->updated_at_db,
            'deleted_at' => $location->deleted_at,
        ];
    }
}
