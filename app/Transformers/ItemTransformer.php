<?php

namespace App\Transformers;

use App\Item;
use League\Fractal\TransformerAbstract;

class ItemTransformer extends TransformerAbstract
{
    public function transform(Item $item)
    {
        return [
            'id' => $item->id,
            // 'user' => $item->user,
            'name' => $item->name,
            'description' => $item->description,
            'price_gbp' => $item->price_gbp,
            'price_aed' => $item->price_aed,
            'price_usd' => $item->price_usd,
            'price_eur' => $item->price_eur,
            'images' => $item->images,
            'options' => $item->options,
            'addons' => $item->addons,
            'category_id' => $item->category_id,
            'category' => $item->category,
            // 'status' => $item->status,
            'created_at' => $item->created_at,
        ];
    }
}
