<?php

namespace App\Transformers;

use App\Addon;
use League\Fractal\TransformerAbstract;

class AddonTransformer extends TransformerAbstract
{
    public function transform(Addon $option)
    {
        return [
            'id' => $option->id,
            'name' => $option->name,
            'price_gbp' => $option->price_gbp,
            'price_eur' => $option->price_eur,
            'price_aed' => $option->price_aed,
            'price_usd' => $option->price_usd,
        ];
    }
}
