<?php

namespace App\Transformers;

use App\Order;
use League\Fractal\TransformerAbstract;

class AdminOrderTransformer extends TransformerAbstract
{
    public function transform(Order $order)
    {
        return [
            'id' => $order->id,
            'user' => $order->user,
            'queue' => $order->queue,
            // 'user' => $order->user,
            'retailer' => $order->retailer,
            'location' => $order->location,
            'total_price' => $order->total_price,
            'total_fee' => $order->total_fee,
            'grandtotal_price' => $order->grandtotal_price,
            'tips' => $order->tips,
            'home_delivery_price' => $order->home_delivery_price,
            'delivery_address' => $order->delivery_address,
            'drink_app_fee' => $order->drink_app_fee,
            'order_revenue' => $order->order_revenue,
            'stripe_fee' => $order->stripe_fee,
            'net_currency' => $order->net_currency,
            'net_currency_symbol' => $order->net_currency_symbol,
            'phone_number' => $order->phone_number,
            'is_delivery' => $order->is_delivery,
            'table_number' => $order->table_number,
            'currency' => $order->currency,
            'currency_symbol' => $order->currency_symbol,
            'description' => $order->description,
            'order_items' => $order->order_items,
            'longitude' => $order->longitude,
            'latitude' => $order->latitude,
            'status' => $order->status,
            'created_at' => $order->created_at,
            'created_at_object' => $order->created_at_object,
            'paid_at' => $order->paid_at,
            'shipped_at' => $order->shipped_at,
            'completed_at' => $order->completed_at,
            'discount' => $order->discount,
            'grandtotal_price_before_discount' => $order->grandtotal_price_before_discount,
            'total_price_before_discount' => $order->total_price_before_discount,
            'discount_sum' => $order->discount_sum,
        ];
    }
}
