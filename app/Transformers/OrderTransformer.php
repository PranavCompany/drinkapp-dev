<?php

namespace App\Transformers;

use App\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    public function transform(Order $order)
    {
        $retailer = $order->retailer()->first();
        return [
            'id' => $order->id,
            'user' => $order->user,
            'queue' => $order->queue,
            // 'user' => $order->user,
            // 'retailer' => $order->retailer,
            'location' => $order->location,
            'currency' => $order->currency,
            'currency_symbol' => $order->currency_symbol,
            'total_price' => $order->total_price,
            'total_fee' => $order->total_fee,
            'grandtotal_price' => $order->grandtotal_price,
            'tips' => $order->tips,
            'is_delivery' => $order->is_delivery,
            'table_number' => $order->table_number,
            'description' => $order->description,
            'order_items' => $order->order_items,
            'status' => $order->status,
            'drink_app_fee' => $order->drink_app_fee,
            'order_revenue' => $order->order_revenue,
            'client_secret' => $order->client_secret,
            'payment_status' => $order->payment_status,
            'home_delivery_price' => $order->home_delivery_price,
            'delivery_address' => $order->delivery_address,
            'phone_number' => $order->phone_number,
            'created_at' => $order->created_at,
            'latitude' => $order->latitude,
            'longitude' => $order->longitude,
            'created_at_object' => $order->created_at_object,
            'paid_at' => $order->paid_at,
            'discount' => $order->discount,
            'grandtotal_price_before_discount' => $order->grandtotal_price_before_discount,
            'total_price_before_discount' => $order->total_price_before_discount,
            'discount_sum' => $order->discount_sum,
            'payment_method_id' => $order->payment_method_id,
            'shipped_at' => $order->shipped_at,
            'customer_fee_percent' => $order->customer_fee_percent,
            'customer_fee_number' => $order->customer_fee_number,
            'completed_at' => $order->completed_at,
            'stripe_fee' => $order->stripe_fee,
            'net_currency' => $order->net_currency,
            'net_currency_symbol' => $order->net_currency_symbol,
            'payment_id' => $order->payment_id,
            'stripe_account_id' => $order->charge_type !== 'custom' ? $retailer->stripe_standard_account : null,
        ];
    }
}
