<?php

namespace App\Transformers;

use App\Location;
use League\Fractal\TransformerAbstract;

class LocationTransformer extends TransformerAbstract
{
    public function transform(Location $location)
    {
        return [
            'id' => $location->id,
            'user' => $location->user,
            'name' => $location->name,
            'description' => $location->description,
            'phone' => $location->phone,
            'address' => $location->address,
            'city' => $location->city,
            'state' => $location->state,
            'country' => $location->country,
            'latitude' => $location->latitude,
            'longitude' => $location->longitude,
            'images' => $location->images,
            'currency' => $location->currency,
            'currency_symbol' => $location->currency_symbol,
            'average_order_time' => $location->average_order_time,
            'to_table' => $location->to_table,
            'to_home' => $location->to_home,
            'home_delivery_radius' => $location->home_delivery_max_radius,
            'home_delivery_min_radius' => $location->home_delivery_min_radius,
            'home_delivery_max_radius' => $location->home_delivery_max_radius,
            'home_delivery_price' => $location->home_delivery_price_old,
            'home_delivery_min_price' => $location->home_delivery_min_price,
            'home_delivery_max_price' => $location->home_delivery_max_price,
            'home_delivery_minimum_order' => $location->home_delivery_minimum_order,
            'home_delivery_time_start' => $location->home_delivery_time_start,
            'home_delivery_time_end' => $location->home_delivery_time_end,
            'home_delivery_days' => $location->home_delivery_days,
            'home_delivery_radius_settings' => $location->home_delivery_radius_settings,
            'price_config' => $location->real_price_config,
            'collection' => $location->collection,
            'location_type' => $location->location_type,
            'special_offers' => $location->special_offers,
            'collection_time_start' => $location->collection_time_start,
            'collection_time_end' => $location->collection_time_end,
            'time_start' => $location->time_start,
            'time_end' => $location->time_end,
            'rating' => $location->rating,
            'categories' => $location->categories,
            'tips_type' => $location->tips_type,
            'tips_list' => $location->tips_list_real,
            'printer' => $location->printer,
            'created_at' => $location->created_at,
        ];
    }
}
