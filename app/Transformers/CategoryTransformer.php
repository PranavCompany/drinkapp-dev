<?php

namespace App\Transformers;

use App\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    public function transform(Category $category)
    {
        return [
            'id' => $category->id,
            // 'user' => $category->user,
            'name' => $category->name,
            'parent_id' => $category->parent_id,
            'description' => $category->description,
            'images' => $category->images,
            'type' => $category->type,
            'items' => $category->items,
            // 'status' => $category->status,
            'created_at' => $category->created_at,
        ];
    }
}
