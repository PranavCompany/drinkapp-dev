<?php

namespace App\Transformers;

use App\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformerGrouped extends TransformerAbstract
{
    private $location_id;

    public function __construct($location_id)
    {
        $this->location_id = $location_id;
    }

    public function transform(Category $category)
    {
        return [
            'id' => $category->id,
            'name' => $category->name,
            'parent_id' => $category->parent_id,
            'description' => $category->description,
            'images' => $category->images,
            'children' => $category->children_filtered($this->location_id),
            'type' => $category->type,
            'time_start' => $category->pivot->time_start,
            'time_end' => $category->pivot->time_end,
            'children_exists' => $category->children_exists,
            'created_at' => $category->created_at,
        ];
    }
}
