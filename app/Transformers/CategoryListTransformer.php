<?php

namespace App\Transformers;

use App\Category;
use League\Fractal\TransformerAbstract;

class CategoryListTransformer extends TransformerAbstract
{
    public function transform(Category $category)
    {
        return [
            'id' => $category->id,
            'name' => $category->name,
            'description' => $category->description,
            'type' => $category->type,
            'children' => $category->children,
            'images' => $category->images,
            'time_start' => $category->pivot->time_start,
            'time_end' => $category->pivot->time_end,
            'created_at' => $category->created_at,
        ];
    }
}
