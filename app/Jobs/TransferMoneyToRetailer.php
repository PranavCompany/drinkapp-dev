<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Transfer;

use App\User;
use App\Order;

class TransferMoneyToRetailer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_id)
    {
        $this->order_id = $order_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = Order::find($this->order_id);
        if (isset($order) && $order->paid == true) {
            $retailer = User::find($order->retailer_id);

            if (isset($retailer) && $retailer->stripe_acc_id) {
                Stripe::setApiKey(env('STRIPE_SECRET'));

                $transfer = Transfer::create([
                    'amount' => $order->total_fee * 100,
                    'currency' => 'eur',
                    'destination' => $retailer->stripe_acc_id
                ]);
                info('>>>>> Created transfer for order: ' . $order->id);
                info('>>>>> Response');
                info($transfer);
            }
                
        }        
    }
}
