<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Stripe\Stripe;
use Stripe\Charge;

use App\User;
use App\Order;

class PayOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order_id;
    protected $card_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_id, $card_id)
    {
        $this->order_id = $order_id;
        $this->card_id = $card_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = Order::find($this->order_id);
        if (isset($order) && $order->paid == false) {
            $customer = User::find($order->user_id);
            Stripe::setApiKey(env('STRIPE_SECRET'));
            $charge = Charge::create([
                'amount' => $order->grandtotal_price,
                'currency' => 'eur',
                'customer' => $customer->stripe_id
            ]);
            info('>>>>> Created charge for order: ' . $order->id);
            info('>>>>> Charge response');
            info($charge);
        }
    }
}
