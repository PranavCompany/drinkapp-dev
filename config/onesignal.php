<?php

return array(
    /*
	|--------------------------------------------------------------------------
	| One Signal App Id
	|--------------------------------------------------------------------------
	|
	|
	*/
    'app_id' => env('ONESIGNAL_APP_ID', 'YOUR-APP-ID'),

    /*
	|--------------------------------------------------------------------------
	| Rest API Key
	|--------------------------------------------------------------------------
	|
    |
	|
	*/
    'rest_api_key' => env('ONESIGNAL_REST_API_KEY', 'YOUR-REST-API-KEY-HERE'),
    'user_auth_key' => env('ONESIGNAL_USER_AUTH_KEY', 'YOUR-USER-AUTH-KEY')
);