<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/privacy-policy', 'PolicyController@show');

Auth::routes(['register' => false]);
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/test/query-string', 'TestController@getQueryString');
//
//Route::get('/test/payment_method', 'TestController@payment_methods');
//Route::post('/test/payment_method', 'TestController@add_payment_methods');
//
//Route::get('/test/order', 'TestController@order');
//Route::post('/test/submit_order', 'TestController@submit_order');
//Route::post('/test/pay_order', 'TestController@pay_order');

Route::group(['middleware' => ['auth', 'role:admin|retailer']], function () {
    Route::get('order/export', 'OrderController@export');
    Route::get('order/{id}', 'OrderController@print');
    Route::get('reports/sold/export', 'API\ReportsController@export');
	Route::get('{path?}', 'DashboardController@index')->where('path', '[\/\w\.-]*');
});

/* Webhook routes */
Route::group(['prefix' => 'webhooks', 'namespace' => 'Webhooks'], function(){
	Route::post('stripe/internal', 'StripeController@handle_internal');
	Route::post('stripe/connections', 'StripeController@handle_connections');
});

/* Retail routes */
// Route::group(['middleware' => ['auth', 'role:retailer']], function () {
// 	Route::get('/retails', 'RetailController@index')->name('retails.index');
// 	Route::get('/retails/create', 'RetailController@create')->name('retails.create');
// 	Route::post('/retails', 'RetailController@store')->name('retails.store');
// });

// Route::group(['prefix' => 'retail', 'middleware' => ['auth', 'role:retailer']], function () {
// 	Route::get('{path?}', 'RetailController@dashboard')->where('path', '[\/\w\.-]*')->name('retails.dashboard');
// });


/* Admin routes */
// Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function () {
// 	Route::get('{path?}', 'AdminController@index')->where('path', '[\/\w\.-]*');
// });
