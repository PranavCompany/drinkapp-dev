<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::group(['middleware' => 'auth:api'], function () {
//     Route::get('/user', function (Request $request) {
//         return $request->user();
//     });
// });

// APIs for mobile app
Route::group([
    'namespace' => 'API',
], function () {

    // API version 1.0
    Route::group([
        'namespace' => 'v1',
        'prefix' => 'v1',
    ], function () {

        Route::get('location', 'LocationController@index')->name('api.v1.location.index');
        Route::get('location/types', 'LocationController@types')->name('api.v1.location.types');
        Route::get('location/closest', 'LocationController@closest')->name('api.v1.location.closest');
        Route::get('location/{id}', 'LocationController@get')->name('api.v1.location.get');
        Route::get('location/{id}/categories', 'LocationController@categories')->name('api.v1.location.categories');
        Route::get('/location/{id}/categories/list', 'LocationController@categories_list')->name('api.v1.location.categories.list');
        Route::get('/location/{id}/categories/list-grouped', 'LocationController@categories_list_grouped')->name('api.v1.location.categories.list_grouped');
        Route::get('/location/{id}/location_category_items/{category_id}', 'LocationController@location_category_items')->name('api.v1.location.categories.list');
        Route::get('location/{id}/delivery', 'LocationController@delivery')->name('api.v1.location.delivery');
        Route::post('location/{id}/delivery-price', 'LocationController@delivery_price')->name('api.v1.location.delivery_price');
        Route::post('items', 'ItemController@index')->name('api.v1.items.get');
        Route::group(['middleware' => ['guest:api']], function(){
            Route::post('login', 'AuthController@login')->name('api.v1.auth.login');
            Route::post('login_facebook', 'AuthController@login_facebook')->name('api.v1.auth.login_facebook');
            Route::post('login_google', 'AuthController@login_google')->name('api.v1.auth.login_google');
            Route::post('login_apple', 'AuthController@login_apple')->name('api.v1.auth.login_apple');
            Route::post('register', 'AuthController@register')->name('api.v1.auth.register');
            Route::post('forgot-password', 'AuthController@forgot_password')->name('api.v1.auth.password.forget');
        });

        Route::group(['middleware' => ['auth:api', 'role:customer']], function(){
            Route::post('location/{id}/discount', 'LocationController@discount')->name('api.v1.location.discount');
            Route::post('location/{id}/remove-discount', 'LocationController@remove_discount')->name('api.v1.location.remove_discount');
            Route::post('password', 'AuthController@change_password')->name('api.v1.auth.change_password');
            Route::post('profile', 'AuthController@change_profile')->name('api.v1.auth.change_profile');

            Route::post('/payment-method/{id}/set_default', 'PaymentMethodController@set_default')->name('api.v1.payment_method.set_default');
            Route::resource('payment-method', 'PaymentMethodController', [
                'names' => [
                    'index' => 'api.v1.payment_method.index',
                    'store' => 'api.v1.payment_method.store',
                    'destroy' => 'api.v1.payment_method.destroy',
                ],
                'except' => ['create', 'show', 'edit', 'update']
            ]);


//            Route::group(['middleware' => ['throttle:1,1']], function(){
//                Route::post('order', 'OrderController@store')->name('api.v1.order.create');
//            });
            Route::post('order', 'OrderController@store')->name('api.v1.order.create');
            Route::get('order', 'OrderController@index')->name('api.v1.order.index');
            Route::get('order/last', 'OrderController@last')->name('api.v1.order.last');
            Route::get('order/{id}', 'OrderController@get')->name('api.v1.order.get');
            Route::post('location/rating/{id}', 'LocationController@addrating')->name('api.v1.location.rating');


            // store users onesignal device id
            Route::post('settings/onesignal', 'SettingController@updateOneSignal')->name('api.settings.update_onesignal');
        });
    });

});

// APIs for SPA
Route::group([
    'namespace' => 'API',
    'middleware' => 'auth:api',
], function () {
    Route::get('user', 'Settings\ProfileController@profile')->name('api.user.profile');
    Route::post('user/check_pin', 'Admin\UserController@check_pin')->name('api.user.check_pin');;
    Route::patch('settings/profile', 'Settings\ProfileController@update')->name('api.settings.update_profile');
    Route::patch('settings/currencies', 'Settings\ProfileController@currencies')->name('api.settings.currencies');
    Route::patch('settings/password', 'Settings\SecurityController@change_password')->name('api.settings.password');
    Route::patch('settings/password', 'Settings\SecurityController@change_password')->name('api.settings.password');

    Route::post('image/upload', 'ImageController@upload')->name('api.image.upload');
    Route::get('category/all', 'CategoryController@all')->name('api.category.all');
    Route::get('category/for-items', 'CategoryController@for_items')->name('api.category.for_items');
    Route::get('category/parent', 'CategoryController@parent')->name('api.category.parent');
    Route::get('category/child', 'CategoryController@child')->name('api.category.child');
    Route::get('category/items', 'CategoryController@items')->name('api.category.items');
    Route::get('/location/{id}/menu', 'LocationController@menu')->name('api.location.menu');
    Route::get('/location/{id}/orders_list', 'LocationController@orders_list');
    // Retailer routes
    Route::group([
        'middleware' => 'role:retailer',
    ], function() {
        Route::get('customers', 'CustomerController@index')->name('api.customers.index');

        Route::get('reports/sold', 'ReportsController@sold')->name('api.reports.sold');
        Route::get('reports/locations', 'ReportsController@locations')->name('api.reports.locations');


        Route::get('dashboard/income', 'DashboardController@income')->name('api.dashboard.income');
        Route::get('dashboard/customers', 'DashboardController@customers')->name('api.dashboard.customers');
        Route::get('dashboard/new_customers', 'DashboardController@new_customers')->name('api.dashboard.new_customers');
        Route::get('dashboard/orders', 'DashboardController@orders')->name('api.dashboard.orders');

        Route::get('dashboard/orders_chart', 'DashboardController@orders_chart')->name('api.dashboard.orders_chart');
        Route::get('dashboard/income_location', 'DashboardController@income_location')->name('api.dashboard.income_location');
        Route::get('dashboard/tips', 'DashboardController@tips')->name('api.dashboard.tips');
        Route::get('dashboard/age', 'DashboardController@age')->name('api.dashboard.age');
        Route::get('dashboard/age_retailer', 'DashboardController@age_retailer')->name('api.dashboard.age_retailer');
        Route::get('dashboard/gender_retailer', 'DashboardController@gender_retailer')->name('api.dashboard.gender_retailer');
        Route::get('dashboard/gender', 'DashboardController@gender')->name('api.dashboard.gender');
        Route::get('dashboard/new_customers_chart', 'DashboardController@new_customers_chart')->name('api.dashboard.new_customers_chart');
        Route::get('dashboard/unique_customers_chart', 'DashboardController@unique_customers_chart')->name('api.dashboard.unique_customers_chart');


        Route::post('settings/payment', 'Settings\PaymentController@create')->name('api.payment.create');
        Route::post('settings/payment/{id}', 'Settings\PaymentController@update')->name('api.payment.update');
        Route::get('settings/payment', 'Settings\PaymentController@get')->name('api.payment.get');
        Route::get('settings/get-link', 'Settings\PaymentController@getLink')->name('api.payment.get_link');
        Route::post('settings/connect-account', 'Settings\PaymentController@connectStandardAccount')->name('api.payment.connect_standard_account');
        Route::post('settings/disconnect-account', 'Settings\PaymentController@disconnect')->name('api.payment.disconnect');

        Route::post('/location/{id}/set-printer', 'LocationController@setPrinter');

        Route::post('/location/{id}/menu', 'LocationController@update_menu')->name('api.location.update_menu');
        Route::get('/location/{id}/items', 'LocationController@items');
        Route::get('/location/{id}/categories', 'LocationController@categories');
        Route::get('/location/{id}/categories/list', 'LocationController@categories_list');
        Route::get('/location/{id}/location_category_items/{category_id}', 'LocationController@location_category_items');
        Route::post('/location/{id}/status', 'LocationController@status');
        Route::post('/location/{id}/settings', 'LocationController@settings');
        Route::get('/location/{id}/orders', 'LocationController@orders');
        Route::get('/location/{id}/orders-to-print', 'LocationController@orders_to_print');
        Route::get('/location/options', 'LocationController@options');
        Route::post('/location/{id}/home-delivery', 'LocationController@homeDelivery');
        Route::get('/location/{id}/home-delivery', 'LocationController@homeDeliveryGet');
        Route::resource('location', 'LocationController', [
            'names' => [
                'index' => 'api.location.index',
                'store' => 'api.location.store',
                'edit' => 'api.location.edit',
                'update' => 'api.location.update',
                'destroy' => 'api.location.destroy',
            ],
            'except' => ['create', 'show']
        ]);

        Route::post('/location/{location_id}/discounts/generate', 'DiscountController@generate');
        Route::post('/location/{location_id}/discounts/import', 'DiscountController@import');
        Route::resource('/location/{location_id}/discounts', 'DiscountController', [
            'names' => [
                'index' => 'api.discounts.index',
                'store' => 'api.discounts.store',
                'edit' => 'api.discounts.edit',
                'update' => 'api.discounts.update',
                'destroy' => 'api.discounts.destroy',
            ],
            'except' => ['create', 'show']
        ]);

        Route::resource('category', 'CategoryController', [
            'names' => [
                'index' => 'api.category.index',
                'store' => 'api.category.store',
                'edit' => 'api.category.edit',
                'update' => 'api.category.update',
                'destroy' => 'api.category.destroy',
            ],
            'except' => ['create', 'show']
        ]);

        Route::get('/location-items/categories', 'ItemController@categories');
        Route::get('/location-items/options', 'ItemController@options');
        Route::get('/location-items/options-all', 'ItemController@options_all');
        Route::post('/location-items/change-data', 'ItemController@change_data');
        Route::post('/location-items/mass-actions', 'ItemController@mass_action');
        Route::get('/location-items/autocomplete', 'ItemController@autocomplete');
        Route::post('/location-items/update-price', 'ItemController@update_price');
        Route::get('/location-items/{id}', 'ItemController@location_items');

        Route::resource('option', 'OptionController', [
            'names' => [
                'index' => 'api.option.index',
                'store' => 'api.option.store',
                'edit' => 'api.option.edit',
                'update' => 'api.option.update',
            ],
            'except' => ['create', 'show', 'destroy']
        ]);
        Route::post('/option/items/{id}', 'OptionController@update_item_options');

        Route::resource('addon', 'AddonController', [
            'names' => [
                'index' => 'api.addon.index',
                'store' => 'api.addon.store',
                'edit' => 'api.addon.edit',
                'update' => 'api.addon.update',
            ],
            'except' => ['create', 'show', 'destroy']
        ]);
        Route::post('/addon/items/{id}', 'AddonController@update_item_addons');
        Route::get('/addon/all', 'AddonController@all');

        Route::resource('item', 'ItemController', [
            'names' => [
                'index' => 'api.item.index',
                'store' => 'api.item.store',
                'edit' => 'api.item.edit',
                'update' => 'api.item.update',
                'destroy' => 'api.item.destroy',
            ],
            'except' => ['create', 'show']
        ]);

        Route::resource('order', 'OrderController', [
            'names' => [
                'index' => 'api.order.index',
                'all' => 'api.order.all',
                'store' => 'api.order.store',
                'edit' => 'api.order.edit',
                'update' => 'api.order.update',
                'destroy' => 'api.order.destroy',
            ],
            'except' => ['create', 'show']
        ]);
        Route::post('/order/{id}/change_status', 'OrderController@change_status');
        Route::post('/order/{id}/notification', 'OrderController@notification');
        Route::get('/order/total_today', 'OrderController@total_today');
        Route::get('/orders/{id}/can-print', 'OrderController@can_print');
        Route::get('/orders/{id}/set-printed', 'OrderController@set_printed');
    });

    // Administrator routes
    Route::group([
        'namespace' => 'Admin',
        'prefix' => 'admin',
        'middleware' => 'role:admin',
    ], function() {
        Route::get('/dashboards/locations', 'DashboardController@locations');
        Route::get('/dashboards/locations/charts', 'DashboardController@locations_charts');
        Route::get('/dashboards/retailer/charts', 'DashboardController@retailer_charts');
        Route::get('/dashboards/locations/income_location', 'DashboardController@income_location');
        Route::get('/dashboards/retailer/income_retailer', 'DashboardController@income_retailer');
        Route::get('/dashboards/locations/gender', 'DashboardController@location_gender');
        Route::get('/dashboards/locations/age', 'DashboardController@location_age');
        Route::get('/dashboards/retailer/gender', 'DashboardController@retailer_gender');
        Route::get('/dashboards/retailer/age', 'DashboardController@retailer_age');

        Route::get('/order', 'OrderController@index');
        Route::get('/dashboards/income', 'DashboardController@income');
        Route::post('/location/{id}/status', 'LocationController@status');
        Route::resource('location', 'LocationController', [
        	'names' => [
	            'index' => 'api.admin.location.index',
	            'store' => 'api.admin.location.store',
	            'edit' => 'api.admin.location.edit',
	            'update' => 'api.admin.location.update',
	            'destroy' => 'api.admin.location.destroy',
	        ],
	        'except' => ['create', 'show']
	    ]);


        Route::post('/user/{id}/status', 'UserController@status');
        Route::post('/user/{id}/password', 'UserController@change_password')->name('api.admin.user.change_password');
        Route::post('/user/{id}/change-prices', 'UserController@change_prices')->name('api.admin.user.change_prices');
        Route::resource('user', 'UserController', [
            'names' => [
                'index' => 'api.admin.user.index',
                'store' => 'api.admin.user.store',
                'edit' => 'api.admin.user.edit',
                'update' => 'api.admin.user.update',
                'destroy' => 'api.admin.user.destroy',
            ],
            'except' => ['create', 'show']
        ]);
    });
});
