<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/home.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito|Rubik:300,500,400" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/images/logo-32.png" class="rounded img-fluid">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        {{-- <a class="nav-link" href="">{{ __('Locations') }}</a> --}}
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <main class="py-4">

        <div class="container">
            <div class="row">
                <style type="text/css">.lst-kix_d2weo6nqfxon-2 > li:before {
                        content: "\0025a0  "
                    }

                    .lst-kix_d2weo6nqfxon-3 > li:before {
                        content: "\0025cf  "
                    }

                    .lst-kix_d2weo6nqfxon-4 > li:before {
                        content: "\0025cb  "
                    }

                    .lst-kix_3pg9a1m0y85e-0 > li:before {
                        content: "\0025cf  "
                    }

                    .lst-kix_3pg9a1m0y85e-1 > li:before {
                        content: "\0025cb  "
                    }

                    .lst-kix_d2weo6nqfxon-0 > li:before {
                        content: "\0025cf  "
                    }

                    .lst-kix_3pg9a1m0y85e-2 > li:before {
                        content: "\0025a0  "
                    }

                    .lst-kix_3pg9a1m0y85e-3 > li:before {
                        content: "\0025cf  "
                    }

                    .lst-kix_d2weo6nqfxon-1 > li:before {
                        content: "\0025cb  "
                    }

                    ul.lst-kix_d2weo6nqfxon-8 {
                        list-style-type: none
                    }

                    .lst-kix_3pg9a1m0y85e-6 > li:before {
                        content: "\0025cf  "
                    }

                    .lst-kix_3pg9a1m0y85e-7 > li:before {
                        content: "\0025cb  "
                    }

                    ul.lst-kix_d2weo6nqfxon-7 {
                        list-style-type: none
                    }

                    ul.lst-kix_d2weo6nqfxon-6 {
                        list-style-type: none
                    }

                    ul.lst-kix_d2weo6nqfxon-5 {
                        list-style-type: none
                    }

                    ul.lst-kix_d2weo6nqfxon-4 {
                        list-style-type: none
                    }

                    .lst-kix_d2weo6nqfxon-8 > li:before {
                        content: "\0025a0  "
                    }

                    .lst-kix_3pg9a1m0y85e-4 > li:before {
                        content: "\0025cb  "
                    }

                    .lst-kix_3pg9a1m0y85e-5 > li:before {
                        content: "\0025a0  "
                    }

                    .lst-kix_3pg9a1m0y85e-8 > li:before {
                        content: "\0025a0  "
                    }

                    ul.lst-kix_d2weo6nqfxon-3 {
                        list-style-type: none
                    }

                    ul.lst-kix_d2weo6nqfxon-2 {
                        list-style-type: none
                    }

                    ul.lst-kix_d2weo6nqfxon-1 {
                        list-style-type: none
                    }

                    ul.lst-kix_d2weo6nqfxon-0 {
                        list-style-type: none
                    }

                    .lst-kix_d2weo6nqfxon-6 > li:before {
                        content: "\0025cf  "
                    }

                    .lst-kix_d2weo6nqfxon-7 > li:before {
                        content: "\0025cb  "
                    }

                    .lst-kix_d2weo6nqfxon-5 > li:before {
                        content: "\0025a0  "
                    }

                    .lst-kix_isfihs8ezsez-0 > li:before {
                        content: "\0025cf  "
                    }

                    .lst-kix_isfihs8ezsez-3 > li:before {
                        content: "\0025cf  "
                    }

                    .lst-kix_isfihs8ezsez-1 > li:before {
                        content: "\0025cb  "
                    }

                    .lst-kix_isfihs8ezsez-2 > li:before {
                        content: "\0025a0  "
                    }

                    ul.lst-kix_isfihs8ezsez-5 {
                        list-style-type: none
                    }

                    .lst-kix_isfihs8ezsez-7 > li:before {
                        content: "\0025cb  "
                    }

                    ul.lst-kix_isfihs8ezsez-4 {
                        list-style-type: none
                    }

                    ul.lst-kix_3pg9a1m0y85e-8 {
                        list-style-type: none
                    }

                    ul.lst-kix_isfihs8ezsez-3 {
                        list-style-type: none
                    }

                    ul.lst-kix_isfihs8ezsez-2 {
                        list-style-type: none
                    }

                    ul.lst-kix_isfihs8ezsez-1 {
                        list-style-type: none
                    }

                    ul.lst-kix_isfihs8ezsez-0 {
                        list-style-type: none
                    }

                    .lst-kix_isfihs8ezsez-4 > li:before {
                        content: "\0025cb  "
                    }

                    .lst-kix_isfihs8ezsez-8 > li:before {
                        content: "\0025a0  "
                    }

                    ul.lst-kix_3pg9a1m0y85e-1 {
                        list-style-type: none
                    }

                    ul.lst-kix_3pg9a1m0y85e-0 {
                        list-style-type: none
                    }

                    ul.lst-kix_3pg9a1m0y85e-3 {
                        list-style-type: none
                    }

                    ul.lst-kix_3pg9a1m0y85e-2 {
                        list-style-type: none
                    }

                    .lst-kix_isfihs8ezsez-5 > li:before {
                        content: "\0025a0  "
                    }

                    ul.lst-kix_3pg9a1m0y85e-5 {
                        list-style-type: none
                    }

                    ul.lst-kix_isfihs8ezsez-8 {
                        list-style-type: none
                    }

                    ul.lst-kix_3pg9a1m0y85e-4 {
                        list-style-type: none
                    }

                    .lst-kix_isfihs8ezsez-6 > li:before {
                        content: "\0025cf  "
                    }

                    ul.lst-kix_isfihs8ezsez-7 {
                        list-style-type: none
                    }

                    ul.lst-kix_3pg9a1m0y85e-7 {
                        list-style-type: none
                    }

                    ul.lst-kix_isfihs8ezsez-6 {
                        list-style-type: none
                    }

                    ul.lst-kix_3pg9a1m0y85e-6 {
                        list-style-type: none
                    }

                    ol {
                        margin: 0;
                        padding: 0
                    }

                    table td, table th {
                        padding: 0
                    }

                    .c5 {
                        -webkit-text-decoration-skip: none;
                        color: #1155cc;
                        font-weight: 400;
                        text-decoration: underline;
                        vertical-align: baseline;
                        text-decoration-skip-ink: none;
                        font-size: 11pt;
                        font-family: "Arial";
                        font-style: normal
                    }

                    .c1 {
                        margin-left: 36pt;
                        /*padding-top: 12pt;*/
                        padding-left: 0pt;
                        /*padding-bottom: 12pt;*/
                        line-height: 1.15;
                        orphans: 2;
                        widows: 2;
                        text-align: left
                    }

                    .c2 {
                        padding-top: 0pt;
                        padding-bottom: 0pt;
                        line-height: 1.15;
                        orphans: 2;
                        widows: 2;
                        text-align: left;
                        height: 11pt
                    }

                    .c0 {
                        color: #000000;
                        font-weight: 400;
                        text-decoration: none;
                        vertical-align: baseline;
                        font-size: 11pt;
                        font-family: "Arial";
                        font-style: normal
                    }

                    .c3 {
                        padding-top: 0pt;
                        padding-bottom: 0pt;
                        line-height: 1.15;
                        orphans: 2;
                        widows: 2;
                        text-align: left
                    }

                    .c9 {
                        color: #000000;
                        text-decoration: none;
                        vertical-align: baseline;
                        font-size: 11pt;
                        font-family: "Arial";
                        font-style: normal
                    }

                    .c8 {
                        text-decoration-skip-ink: none;
                        -webkit-text-decoration-skip: none;
                        color: #1155cc;
                        text-decoration: underline
                    }

                    .c10 {
                        background-color: #ffffff;
                        max-width: 468pt;
                        padding: 72pt 72pt 72pt 72pt
                    }

                    .c4 {
                        color: inherit;
                        text-decoration: inherit
                    }

                    .c6 {
                        padding: 0;
                        margin: 0
                    }

                    .c7 {
                        font-weight: 700
                    }

                    .title {
                        padding-top: 0pt;
                        color: #000000;
                        font-size: 26pt;
                        padding-bottom: 3pt;
                        font-family: "Arial";
                        line-height: 1.15;
                        page-break-after: avoid;
                        orphans: 2;
                        widows: 2;
                        text-align: left
                    }

                    .subtitle {
                        padding-top: 0pt;
                        color: #666666;
                        font-size: 15pt;
                        padding-bottom: 16pt;
                        font-family: "Arial";
                        line-height: 1.15;
                        page-break-after: avoid;
                        orphans: 2;
                        widows: 2;
                        text-align: left
                    }

                    li {
                        color: #000000;
                        font-size: 11pt;
                        font-family: "Arial"
                    }

                    p {
                        margin: 0;
                        color: #000000;
                        font-size: 11pt;
                        font-family: "Arial"
                    }

                    h1 {
                        padding-top: 20pt;
                        color: #000000;
                        font-size: 20pt;
                        padding-bottom: 6pt;
                        font-family: "Arial";
                        line-height: 1.15;
                        page-break-after: avoid;
                        orphans: 2;
                        widows: 2;
                        text-align: left
                    }

                    h2 {
                        padding-top: 18pt;
                        color: #000000;
                        font-size: 16pt;
                        padding-bottom: 6pt;
                        font-family: "Arial";
                        line-height: 1.15;
                        page-break-after: avoid;
                        orphans: 2;
                        widows: 2;
                        text-align: left
                    }

                    h3 {
                        padding-top: 16pt;
                        color: #434343;
                        font-size: 14pt;
                        padding-bottom: 4pt;
                        font-family: "Arial";
                        line-height: 1.15;
                        page-break-after: avoid;
                        orphans: 2;
                        widows: 2;
                        text-align: left
                    }

                    h4 {
                        padding-top: 14pt;
                        color: #666666;
                        font-size: 12pt;
                        padding-bottom: 4pt;
                        font-family: "Arial";
                        line-height: 1.15;
                        page-break-after: avoid;
                        orphans: 2;
                        widows: 2;
                        text-align: left
                    }

                    h5 {
                        padding-top: 12pt;
                        color: #666666;
                        font-size: 11pt;
                        padding-bottom: 4pt;
                        font-family: "Arial";
                        line-height: 1.15;
                        page-break-after: avoid;
                        orphans: 2;
                        widows: 2;
                        text-align: left
                    }
                    p.c3 {
                        text-align: left;
                    }
                    h6 {
                        padding-top: 12pt;
                        color: #666666;
                        font-size: 11pt;
                        padding-bottom: 4pt;
                        font-family: "Arial";
                        line-height: 1.15;
                        page-break-after: avoid;
                        font-style: italic;
                        orphans: 2;
                        widows: 2;
                        text-align: left
                    }</style>
                <div class="shadow-lg p-5">
                <p class="c3"><span class="c0">Drinkapp Ltd are committed to protecting and respecting your privacy.</span>
                </p>
                <p class="c3"><span class="c0">This policy sets out the basis on which any personal data We collect from you, or that you provide to Us, will be processed by Us. Please read the following carefully to understand Our views and practices regarding your personal data and how We will treat it.</span>
                </p>
                <p class="c3"><span class="c0">For the purpose of the EU General Data Protection Regulations (&ldquo;GDPR&rdquo;) and national laws implementing GDPR and any legislation that replaces it in whole or in part and any other legislation relating to the protection of personal data, the data controller is drinkapp Ltd </span>
                </p>
                <p class="c2"><span class="c0"></span></p>
                <p class="c3"><span class="c9 c7">SCOPE OF POLICY</span></p>
                <p class="c3"><span>This policy, (together with Our end-user licence agreements as set out at (Apple devices) </span><span
                            class="c8"><a class="c4"
                                          href="https://www.google.com/url?q=http://www.apple.com/legal/internet-services/itunes/appstore/dev/stdeula/&amp;sa=D&amp;ust=1559553471116000">http://www.apple.com/legal/internet-services/itunes/appstore/dev/stdeula/ </a></span><span>or (Android devices) </span><span
                            class="c8"><a class="c4"
                                          href="https://www.google.com/url?q=https://play.google.com/intl/en_uk/about/play-terms.html&amp;sa=D&amp;ust=1559553471116000">https://play.google.com/intl/en_uk/about/play-terms.html</a></span><span
                            class="c0">&nbsp;(the EULAs) and any additional terms of use incorporated by reference into the EULA, collectively Our Terms of Use) applies to your use of:</span>
                </p>
                <ul class="c6 lst-kix_3pg9a1m0y85e-0 start">
                    <li class="c1"><span class="c0">the drinkapp Order and Pay mobile application software (App) hosted on (Apple devices) the Apple iTunes App Store or (Android devices) the Google Play store (App Site), once you have downloaded a copy of the App onto your mobile telephone or handheld device (Device).</span>
                    </li>
                    <li class="c1"><span class="c0">Any of the services accessible through the App (Services)</span></li>
                </ul>
                <p class="c2"><span class="c0"></span></p>
                <p class="c3"><span class="c9 c7">INFORMATION WE MAY COLLECT FROM YOU</span></p>
                <p class="c3"><span class="c0">We may collect and process the following data about you:</span></p>
                <ul class="c6 lst-kix_isfihs8ezsez-0 start">
                    <li class="c1"><span class="c7">Information you give to Us (Submitted information):</span><span
                                class="c0">&nbsp;You may give Us information about you by filling in forms on the App, or by corresponding with Us (for example, by e-mail or telephone). This includes information you provide when you, download or register to use the App, &nbsp;purchase food or drink (including order history) and when you report a problem with the App. The information you give Us may include your name, address, e-mail address and telephone number.<br> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span>
                    </li>
                    <li class="c1"><span class="c7">Location information.</span><span class="c0">&nbsp;We use GPS technology to determine your current location. Our location-enabled Services require your location data for the feature to work. You will be asked to consent to your data being used for this purpose.<br><br></span>
                    </li>
                    <li class="c1"><span
                                class="c7">Information We receive from other sources (Third Party Information).</span><span
                                class="c0">&nbsp;We work with third party business partners who provide technical, payment and delivery services and may receive information about you from them which may include your email address and Stripe user name and payment method.<br><br></span>
                    </li>
                    <li class="c1"><span class="c7">Unique application numbers</span><span class="c0">: when you install or uninstall a service containing a unique application number or when such a Service searches for automatic updates, that number and information about your installation, for example, the type of operating system, may be sent to Us.</span>
                    </li>
                </ul>
                <p class="c2"><span class="c0"></span></p>
                <p class="c3"><span class="c9 c7">USES MADE OF THE INFORMATION</span></p>
                <p class="c3"><span class="c0">We use information held about you in the following ways:</span></p>
                <ul class="c6 lst-kix_d2weo6nqfxon-0 start">
                    <li class="c1"><span class="c0">Submitted Information: to confirm the order process the payment, create order history and for App personalisation.<br><br></span>
                    </li>
                    <li class="c1"><span class="c0">Log information: for order history and troubleshooting. &nbsp; <br><br></span>
                    </li>
                    <li class="c1"><span class="c0">Location information: for GPS tracking to locate the nearest venue and route planning.<br><br></span>
                    </li>
                    <li class="c1">
                        <span>Third Party Information: information required by (Apple Devices) Apple (</span><span
                                class="c8"><a class="c4"
                                              href="https://www.google.com/url?q=http://www.apple.com/uk/privacy/privacy-policy/&amp;sa=D&amp;ust=1559553471121000">http://www.apple.com/uk/privacy/privacy-policy/</a></span><span>) or (Android Devices) Google (</span><span
                                class="c8"><a class="c4"
                                              href="https://www.google.com/url?q=https://policies.google.com/privacy?hl%3Den%26gl%3Duk&amp;sa=D&amp;ust=1559553471121000">https://www.google.co.uk/intl/en/policies/privacy/</a></span><span
                                class="c0">) and Stripe (https://stripe.com/gb/privacy) to fulfil orders.<br> &nbsp; &nbsp; &nbsp; &nbsp;</span>
                    </li>
                    <li class="c1"><span class="c0">Unique application numbers: Apple and Google assign a non-identifiable unique ID when the App is downloaded, this is used to create order history/repeat orders and personalise the App.</span>
                    </li>
                </ul>
                <p class="c2"><span class="c0"></span></p>
                <p class="c2"><span class="c0"></span></p>
                <p class="c3"><span class="c7">DISCLOSURE OF YOUR INFORMATION</span><span class="c0">&nbsp;</span></p>
                <p class="c3"><span class="c0">We may disclose your personal information to third parties:</span></p>
                <p class="c2"><span class="c0"></span></p>
                <p class="c3"><span class="c7">WHERE WE STORE YOUR PERSONAL DATA</span><span class="c0">&nbsp;</span></p>
                <p class="c3"><span>The data that We collect from you will be transferred to, and stored at, a destination inside the European Economic Area (</span><span
                            class="c7">EEA</span><span class="c0">) by DrinkApp Ltd. It will be processed by staff operating inside the EEA who work for us or for one of our suppliers. These staff may be engaged in the fulfilment of your request, order or reservation, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. DrinkApp Ltd will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy. </span>
                </p>
                <p class="c3"><span class="c0">All information you provide to Us is stored on Our secure servers. Any payment transactions carried out by Us or Our chosen third-party provider of payment processing services will be encrypted. </span>
                </p>
                <p class="c3"><span class="c0">We limit access to your personal data to those employees, agents, contractors and other third parties who have a business need to know. They will only process your personal data on our instructions and they are subject to a duty of confidentiality. </span>
                </p>
                <p class="c2"><span class="c0"></span></p>
                <p class="c3"><span class="c0">Personal data is collected on your Device in order to request services and products from us. </span>
                </p>
                <p class="c2"><span class="c0"></span></p>
                <p class="c3"><span class="c7 c9">YOUR RIGHTS</span></p>
                <p class="c3"><span class="c0">You have the right to: </span></p>
                <p class="c3"><span class="c0">Request access to your personal data (commonly known as a &quot;data subject access request&quot;). This enables you to receive a copy of the personal data we hold about you and to check that we are lawfully processing it. </span>
                </p>
                <p class="c3"><span class="c0">Request correction of the personal data that we hold about you. This enables you to have any incomplete or inaccurate data we hold about you corrected, though we may need to verify the accuracy of the new data you provide to us. </span>
                </p>
                <p class="c3"><span class="c0">Request erasure of your personal data. This enables you to ask us to delete or remove personal data where there is no good reason for us continuing to process it. You also have the right to ask us to delete or remove your personal data where you have successfully exercised your right to object to processing (see below), where we may have processed your information unlawfully or where we are required to erase your personal data to comply with local law. Note, however, that we may not always be able to comply with your request of erasure for specific legal reasons which will be notified to you, if applicable, at the time of your request. </span>
                </p>
                <p class="c3"><span class="c0">Object to processing of your personal data where we are relying on a legitimate interest (or those of a third party) and there is something about your particular situation which makes you want to object to processing on this ground as you feel it impacts on your fundamental rights and freedoms. You also have the right to object where we are processing your personal data for direct marketing purposes. In some cases, we may demonstrate that we have compelling legitimate grounds to process your information which override your rights and freedoms. </span>
                </p>
                <p class="c3"><span class="c0">Request restriction of processing of your personal data. This enables you to ask us to suspend the processing of your personal data in the following scenarios: (a) if you want us to establish the data&#39;s accuracy; (b) where our use of the data is unlawful but you do not want us to erase it; (c) where you need us to hold the data even if we no longer require it as you need it to establish, exercise or defend legal claims; or (d) you have objected to our use of your data but we need to verify whether we have overriding legitimate grounds to use it. </span>
                </p>
                <p class="c3"><span class="c0">Request the transfer of your personal data to you or to a third party. We will provide to you, or a third party you have chosen, your personal data in a structured, commonly used, machine-readable format. Note that this right only applies to automated information which you initially provided consent for us to use or where we used the information to perform a contract with you. </span>
                </p>
                <p class="c3"><span class="c0">Withdraw consent at any time where we are relying on consent to process your personal data. However, this will not affect the lawfulness of any processing carried out before you withdraw your consent. If you withdraw your consent, we may not be able to provide certain products or services to you. We will advise you if this is the case at the time you withdraw your consent. </span>
                </p>
                <p class="c3"><span class="c0">Our Sites may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates (including, but not limited to, websites on which the App or the Services are advertised). If you follow a link to any of these websites, please note that these websites and any services that may be accessible through them have their own privacy policies and that We do not accept any responsibility or liability for these policies or for any personal data that may be collected through these websites or services, such as contact and location data. Please check these policies before you submit any personal data to these websites or use these services. </span>
                </p>
                <p class="c3"><span class="c9 c7">CHANGES TO PRIVACY POLICY </span></p>
                <p class="c3"><span class="c0">Any changes We may make to Our privacy policy in the future will be posted on this page. The new terms may be displayed on-screen and you will be required to read and accept them to continue your use of the App or the Services.</span>
                </p>
                <p class="c3"><span class="c9 c7">CONTACT </span></p>
                <p class="c3"><span>customerservices@</span><span class="c5"><a class="c4"
                                                                                href="https://www.google.com/url?q=http://drinkapp.com&amp;sa=D&amp;ust=1559553471126000">Drinkapp.com</a></span>
                </p>
                <p class="c2"><span class="c0"></span></p>
                </div>
            </div>
        </div>
    </main>

</div>
</body>
</html>
