@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Hello!')
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
switch ($level) {
case 'success':
case 'error':
    $color = $level;
    break;
default:
    $color = 'primary';
}
?>
{{--@component('mail::button', ['url' => $actionUrl, 'color' => $color])--}}
{{--{{ $actionText }}--}}
{{--@endcomponent--}}
@endisset
@component('mail::table')
    | ORDER INFO     |               |          |
    | :------------- |-------------:| :--------:|
    @foreach ($outroLines as $line)
        {{ $line }}
    @endforeach
    {{--| Col 2 is      | Centered      | $10      |--}}
    {{--| Col 3 is      | Right-Aligned | $20      |--}}
@endcomponent
{{-- Outro Lines --}}


{{-- Salutation
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Regards'),<br>{{ config('app.name') }}
@endif
--}}


@isset($actionText)
@component('mail::subcopy')
@lang(
"You did not buy this items? Problem with the transactions?\n".
'[:actionText](:actionURL)',
[
'actionText' => $actionText,
'actionURL' => $actionUrl,
]
)
@endcomponent
@endisset
@endcomponent
