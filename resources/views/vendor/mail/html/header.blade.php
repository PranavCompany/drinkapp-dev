<header class="header">
    <a href="{{ $url }}">
        {{ $slot }}
    </a>
</header>
