<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
        @media (max-width: 1200px) {
            .wrapper {
                width: 70%!important;
            }
        }
        @media (max-width: 900px) {
            .wrapper {
                width: 80%!important;
            }
        }

        @media (max-width: 600px) {
            .wrapper {
                width: 85%!important;
            }
        }
        @media (max-width: 350px) {
            .wrapper {
                width: 95%!important;;
            }
        }
    </style>

    {{ $header ?? '' }}

    <div class="wrapper">
        {{ Illuminate\Mail\Markdown::parse($slot) }}

        {{ $subcopy ?? '' }}
    </div>

    {{ $footer ?? '' }}
</body>
</html>
