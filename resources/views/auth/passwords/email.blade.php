@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div style="width:320px;">
            <div class="text-center mt-5 mb-4">
                <a href="/"><img src="/images/logo-192.png" class="rounded"></a>
            </div>           
                    
            <div class="shadow-lg p-4">
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <h3 class="mb-3">Forgot Password</h3>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-group">
                        {{-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> --}}

                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email Address">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-primary btn-block">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
