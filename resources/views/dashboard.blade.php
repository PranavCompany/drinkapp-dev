<!-- @php
$config = [
    'appName' => config('app.name'),
    'locale' => $locale = app()->getLocale(),
    'locales' => config('app.locales')
];
@endphp -->
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito|Rubik:300,500,400" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="{{asset('epson_js/epos-2.14.0.js')}}" defer></script>
    {{-- Global configuration object --}}
    <script>window.config = @json($config);</script>

    <script type="text/javascript">
        window.Drink = {
            csrfToken: "{{ csrf_token() }}"
        }
        window.User = {!! Auth::user() !!}
        window.Language = "{{ config('app.locale') }}"
    </script>
</head>
<body>
    <div id="app"></div>

    {{-- Load the application scripts --}}
    @if (app()->isLocal())
        <!-- <script src="{{ mix('js/app.js') }}"></script> -->
    @else
        <script src="{{ mix('js/manifest.js') }}"></script>
        <script src="{{ mix('js/vendor.js') }}"></script>
        <script src="{{ mix('js/app.js') }}"></script>
    @endif

    {{-- Google Map API --}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAduOnmBGeS7MTOREPwMNGGHouG9dOf8xc&libraries=places"
        async defer></script>

    {{-- Stripe.js --}}
    <script src="https://js.stripe.com/v3/"></script>
</body>
</html>
