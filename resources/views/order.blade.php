<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        * {
            font-size: 12px;
            font-family: 'Times New Roman';
            -webkit-print-color-adjust: exact !important;
        }

        table {
            border-top: 1px solid black;
            border-collapse: collapse;
        }
        .border {
            border-top: 1px solid black;
            border-collapse: collapse;
        }
        .headers {
            color: black;
        }

        .title {
            font-weight: 700;
        }

        td.name,
        th.name {
            width: 130px;
            max-width: 130px;
        }

        td.quantity,
        th.quantity {
            width: 25px;
            max-width: 25px;
            font-weight: 700;
            word-break: break-all;
        }

        td.check,
        th.check {
            width: 25px;
            max-width: 25px;
            word-break: break-all;
        }

        .centered {
            text-align: center;
            align-content: center;
        }

        .order {
            width: 155px;
            max-width: 155px;
        }

        img {
            max-width: inherit;
            width: inherit;
        }
    </style>
</head>
<body>
<div class="order">
    <p class="headers">
        @if ($order->table)
            To table: {{$order->table}}
        @elseif($order->delivery_address)
            Delivery
        @else
            Collect at bar
        @endif
    </p>
    <p class="headers">{{$order->created_at_print}}</p>
    <p class="headers">Queue number: {{$order->queue}}</p>
    <p class="title">Order details: </p>
    <table>
        @if ($order->phone_number)
            <tr class="border">
                <td>Number:</td>
                <td>{{$order->phone_number}}</td>
            </tr>
        @endif
        @if ($order->delivery_address)
                <tr class="border">
                <td>Address:</td>
                <td>{{$order->delivery_address}}</td>
            </tr>
        @endif
        <tr class="border">
            <td>Customer:</td>
            <td>{{$order->user->name}}</td>
        </tr>
        @if(!empty($order->description))
            <tr class="border">
                <td>Note:</td>
                <td>{{$order->description}}</td>
            </tr>
        @endif
    </table>
    <p class="title">Items: </p>
    <table>
        <tbody>
        @foreach($order->order_items as $item)
            <tr class="border">
                <td class="quantity">{{$item->quantity}}X</td>
                <td class="name">{{$item->item_name}}</td>
            </tr>
            @if($item->options)
                @foreach($item->options as $option)
                    <tr>
                        <td></td>
                        <td class="name" style="font-style: italic">{{$option['name']}}: {{$option['value']}}</td>
                    </tr>
                @endforeach
            @endif
            @if($item->addons)
                @foreach($item->addons as $addon)
                    <tr>
                    <td>+ {{$item->quantity}}</td>
                    <td class="name"  style="font-style: italic">{{$addon['name']}}</td>
                    </tr>
                @endforeach
            @endif
        @endforeach
        </tbody>
    </table>
</div>
<script>
    // window.print();
    window.onclick = function () {
        window.close();
    };
    // window.onafterprint = function () {
    //     window.parent.focus();
    //     window.close();
    // }
</script>
</body>
</html>
