@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img class="img-logo" src="{{config('app.url')}}/images/logo-64.png">
            <span class="app-name">{{ config('app.name') }}</span>
        @endcomponent
    @endslot

    {{-- Body --}}
    Hi <strong>{{ $name }}</strong>,
    <br /><br />
    You are invited to DrinkApp.<br/><br/>
    Your username: {{ $email }}.<br/>
    Your password: {{ $password }}.<br/><br/>
    @component('mail::button', ['url' => config('app.url'), 'color' => 'primary'])
    Login
    @endcomponent

    {{-- Subcopy --}}
    {{-- @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset --}}

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            {{-- © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.') --}}
            DrinkApp. Inc. 1355 Market Street, Suite 900 San Fransisco, CA 94103
        @endcomponent
    @endslot
@endcomponent
