import * as types from '../mutation-types'

// state
export const state = {
    title: '',
    subtitle: '',
    locations: [],
    selected_location: '',
    selected_location_delivery: '',
    selected_location_printer: '',
    selected_location_printer_https_errors: false,
    can_print: {},
    sound: '',
    pin: '',
    sound_counter: 0,
    orders_total: '',
    interval: false,
    location_orders: {
        'pending': [],
        'collection': [],
        'delivery': [],
        'last_order_id': '',
    },
};

// getters
export const getters = {
    title: state => state.title,
    pin: state => state.pin,
    subtitle: state => state.subtitle,
    locations: state => state.locations,
    orders_total: state => state.orders_total,
    selected_location: state => state.selected_location,
    selected_location_delivery: state => state.selected_location_delivery,
    selected_location_printer: state => state.selected_location_printer,
    selected_location_printer_https_errors: state => state.selected_location_printer_https_errors,
    can_print: state => state.can_print,
    location_orders: state => state.location_orders,
    sound: state => state.sound,
    sound_counter: state => state.sound_counter,
    interval: state => state.interval,
};

// mutations
export const mutations = {
    [types.SET_TITLE] (state, { title }) {
        state.title = title
    },
    [types.SET_PIN] (state, { pin }) {
        state.pin = pin
    },
    [types.SET_ORDERS_TOTAL] (state, { orders_total }) {
        state.orders_total = orders_total
    },
    [types.SET_INTERVAL] (state, { interval }) {
        state.interval = interval
    },
    [types.SET_SUBTITLE] (state, { subtitle }) {
        state.subtitle = subtitle
    },
    [types.SET_LOCATIONS] (state, { locations }) {
        state.locations = locations;
    },
    [types.SET_SELECTED_LOCATION] (state, { selected_location }) {
        state.selected_location = selected_location;
    },
    [types.SET_SELECTED_LOCATION_DELIVERY] (state, { selected_location_delivery }) {
        state.selected_location_delivery = selected_location_delivery;
    },
    [types.SET_SELECTED_LOCATION_PRINTER] (state, { selected_location_printer }) {
        state.selected_location_printer = selected_location_printer;
    },
    [types.SET_SELECTED_LOCATION_PRINTER_HTTPS_ERROR] (state, { selected_location_printer_https_errors }) {
        state.selected_location_printer_https_errors = selected_location_printer_https_errors;
    },
    [types.SET_CAN_PRINT] (state, { can_print }) {
        state.can_print = can_print;
    },
    [types.SET_LOCATION_ORDERS] (state, { location_orders }) {
        if (state.selected_location && state.locations) {
            let total_orders = 0;
            const list = ['collection', 'delivery', 'pending'];
            state.locations.forEach(function (value, index) {
                if (value.id === state.selected_location) {
                    list.forEach(function (i) {
                        if (typeof location_orders[i] === 'object') {
                            total_orders += Object.keys(location_orders[i]).length;
                        }
                    });
                    state.locations[index].total_orders = total_orders;
                }
            });
        }
        state.location_orders = location_orders;
    },
    [types.SET_SOUND] (state, { sound }) {
        state.sound = sound;
    },
    [types.SET_SOUND_COUNTER] (state, { sound_counter }) {
        state.sound_counter = sound_counter;
    },
};

// actions
export const actions = {
    setTitle ({ commit }, { title }) {
        commit(types.SET_TITLE, { title })
    },
    setPin ({ commit }, { pin }) {
        commit(types.SET_PIN, { pin })
    },
    setInterval ({ commit }, { interval }) {
        commit(types.SET_INTERVAL, { interval })
    },
    setSound ({ commit }, { sound }) {
        commit(types.SET_SOUND, { sound })
    },
    setSoundCounter ({ commit }, { sound_counter }) {
        commit(types.SET_SOUND_COUNTER, { sound_counter })
    },
    setSubtitle ({ commit }, { subtitle }) {
        commit(types.SET_SUBTITLE, { subtitle })
    },
    setLocations ({ commit }, { locations }) {
        commit(types.SET_LOCATIONS, { locations })
    },
    setSelectedLocation ({ commit }, { selected_location }) {
        commit(types.SET_SELECTED_LOCATION, { selected_location })
    },
    setSelectedLocationDelivery ({ commit }, { selected_location_delivery }) {
        commit(types.SET_SELECTED_LOCATION_DELIVERY, { selected_location_delivery })
    },
    setSelectedLocationPrinter ({ commit }, { selected_location_printer }) {
        commit(types.SET_SELECTED_LOCATION_PRINTER, { selected_location_printer })
    },
    setSelectedLocationPrinterHTTPSError ({ commit }, { selected_location_printer_https_errors }) {
        commit(types.SET_SELECTED_LOCATION_PRINTER_HTTPS_ERROR, { selected_location_printer_https_errors })
    },
    setCanPrint ({ commit }, { can_print }) {
        commit(types.SET_CAN_PRINT, { can_print })
    },
    setLocationOrders ({ commit }, { location_orders }) {
        commit(types.SET_LOCATION_ORDERS, { location_orders })
    },
    setOrdersTotal ({ commit }, { orders_total }) {
        commit(types.SET_ORDERS_TOTAL, { orders_total })
    }
};
