import store from '~/store'

export default (to, from, next) => {
  	if (store.getters['auth/user'].role_names && store.getters['auth/user'].role_names.indexOf('retailer') == -1) {
    	next({ name: 'admin.dashboard' })
  	} else {
    	next()
  	}
}
