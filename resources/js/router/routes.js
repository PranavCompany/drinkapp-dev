// const Welcome = () => import('~/pages/welcome').then(m => m.default || m)
// const Login = () => import('~/pages/auth/login').then(m => m.default || m)
// const Register = () => import('~/pages/auth/register').then(m => m.default || m)
// const PasswordEmail = () => import('~/pages/auth/password/email').then(m => m.default || m)
// const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
// const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)

const Home = () => import('~/pages/home').then(m => m.default || m);
const Dashboard = () => import('~/pages/dashboard/index').then(m => m.default || m);
const ReportsSold = () => import('~/pages/reports/sold').then(m => m.default || m);

const Locations = () => import('~/pages/locations/index').then(m => m.default || m)
const LocationList = () => import('~/pages/locations/list').then(m => m.default || m)
const LocationCreate = () => import('~/pages/locations/create').then(m => m.default || m)
const LocationEdit = () => import('~/pages/locations/edit').then(m => m.default || m)
const LocationSettings = () => import('~/pages/locations/settings').then(m => m.default || m)
const LocationDiscountsList = () => import('~/pages/locations/discounts').then(m => m.default || m)
const LocationDiscountsEdit = () => import('~/pages/locations/discount_edit').then(m => m.default || m)
const LocationDiscountsCreate = () => import('~/pages/locations/discount_create').then(m => m.default || m)
const LocationShow = () => import('~/pages/locations/show').then(m => m.default || m)
const LocationDetail = () => import('~/pages/locations/detail').then(m => m.default || m)
const LocationMenu = () => import('~/pages/locations/menu_items').then(m => m.default || m)
const LocationOrder = () => import('~/pages/locations/order').then(m => m.default || m)

const RetailItemOptionsCat = () => import('~/pages/items/options_index').then(m => m.default || m)
const RetailItemOptions = () => import('~/pages/items/options').then(m => m.default || m)
const RetailItemOptionsCreate = () => import('~/pages/items/options_create').then(m => m.default || m)
const RetailItemOptionsEdit = () => import('~/pages/items/options_edit').then(m => m.default || m)

const RetailItemAddonsCat = () => import('~/pages/addons/index').then(m => m.default || m)
const RetailItemAddons = () => import('~/pages/addons/list').then(m => m.default || m)
const RetailItemAddonsCreate = () => import('~/pages/addons/create').then(m => m.default || m)
const RetailItemAddonsEdit = () => import('~/pages/addons/edit').then(m => m.default || m)


const Categories = () => import('~/pages/categories/index').then(m => m.default || m)
const CategoryList = () => import('~/pages/categories/list').then(m => m.default || m)
const CategoryCreate = () => import('~/pages/categories/create').then(m => m.default || m)
const CategoryEdit = () => import('~/pages/categories/edit').then(m => m.default || m)
const CategoryDetail = () => import('~/pages/categories/detail').then(m => m.default || m)

const Items = () => import('~/pages/items/index').then(m => m.default || m)
const ItemList = () => import('~/pages/items/list').then(m => m.default || m)
const ItemCreate = () => import('~/pages/items/create').then(m => m.default || m)
const ItemEdit = () => import('~/pages/items/edit').then(m => m.default || m)

const Users = () => import('~/pages/customers/index').then(m => m.default || m)
const UsersList = () => import('~/pages/customers/list').then(m => m.default || m)

const Order = () => import('~/pages/orders/index').then(m => m.default || m)
const OrderActive = () => import('~/pages/orders/active').then(m => m.default || m)
const OrdersList = () => import('~/pages/orders/list').then(m => m.default || m)


const AdminOrder = () => import('~/pages/admin/orders/index').then(m => m.default || m)
const AdminOrdersList = () => import('~/pages/admin/orders/list').then(m => m.default || m)

const Settings = () => import('~/pages/settings/index').then(m => m.default || m)
const SettingsProfile = () => import('~/pages/settings/profile').then(m => m.default || m)
const SettingsPassword = () => import('~/pages/settings/password').then(m => m.default || m)
const SettingsPayment = () => import('~/pages/settings/payment').then(m => m.default || m)
const SettingsCurrencies= () => import('~/pages/settings/currencies').then(m => m.default || m)

const Admin = () => import('~/pages/admin/index').then(m => m.default || m)
const AdminDashboard = () => import('~/pages/admin/dashboard').then(m => m.default || m)

const AdminUser = () => import('~/pages/admin/user/index').then(m => m.default || m)
const AdminUserList = () => import('~/pages/admin/user/list').then(m => m.default || m)
const AdminUserCreate = () => import('~/pages/admin/user/create').then(m => m.default || m)
const AdminUserEdit = () => import('~/pages/admin/user/edit').then(m => m.default || m)
const AdminUserChangePassword = () => import('~/pages/admin/user/password').then(m => m.default || m)
const AdminUserPriceConfig = () => import('~/pages/admin/user/price_config').then(m => m.default || m)

const AdminLocation = () => import('~/pages/admin/location/index').then(m => m.default || m);
const AdminLocationList = () => import('~/pages/admin/location/list').then(m => m.default || m);
const AdminLocationShow = () => import('~/pages/admin/location/show').then(m => m.default || m);
const AdminLocationDetail = () => import('~/pages/admin/location/detail').then(m => m.default || m);
const AdminLocationCreate = () => import('~/pages/admin/location/create').then(m => m.default || m);
const AdminLocationMenu = () => import('~/pages/admin/location/menu').then(m => m.default || m);
const AdminLocationOrder= () => import('~/pages/admin/location/order').then(m => m.default || m);


const AdminSettings = () => import('~/pages/admin/settings/index').then(m => m.default || m)

const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)

export default [
    // {   path: '/', name: 'welcome', component: Welcome },
    // {   path: '/login', name: 'login', component: Login },
    // {   path: '/register', name: 'register', component: Register },
    // {   path: '/password/reset', name: 'password.request', component: PasswordEmail },
    // {   path: '/password/reset/:token', name: 'password.reset', component: PasswordReset },

    {
        path: '/',
        component: Home,
        children: [
            {   path: '', redirect: { name: 'orders.active' } },
            {   path: 'dashboard', name: 'dashboard', component: Dashboard  },
            {
                path: 'locations',
                component: Locations,
                children: [
                    {   path: '/', name: 'locations.list', component: LocationList },
                    {   path: 'create', name: 'locations.create', component: LocationCreate },
                    {
                        path: ':location_id',
                        name: 'locations.detail',
                        component: LocationShow,
                        redirect: { name: 'locations.detail.index' },
                        children: [
                            {   path: 'detail', name: 'locations.detail.index', component: LocationDetail },
                            {   path: 'menu', name: 'locations.detail.menu', component: LocationMenu },
                            {   path: 'order', name: 'locations.detail.order', component: LocationOrder },
                            {   path: 'edit', name: 'locations.detail.edit', component: LocationEdit },
                            {   path: 'settings', name: 'locations.detail.settings', component: LocationSettings },
                            {   path: 'discounts', name: 'locations.detail.discounts.index', component: LocationDiscountsList },
                            {   path: 'discounts/create', name: 'locations.detail.discounts.create', component: LocationDiscountsCreate },
                            {   path: 'discounts/:discount_id', name: 'locations.detail.discounts.edit', component: LocationDiscountsEdit },
                        ]
                    }
                ]
            },
            {
                path: 'categories',
                component: Categories,
                children: [
                    {   path: '/', name: 'categories.list', component: CategoryList },
                    {   path: 'create', name: 'categories.create', component: CategoryCreate },
                    {
                        path: 'options',
                        component: RetailItemOptionsCat,
                        children: [
                            {   path: '/', name: 'items.options', component: RetailItemOptions},
                            {   path: 'create', name: 'items.options.create', component: RetailItemOptionsCreate},
                            {   path: ':option_id/edit', name: 'items.options.edit', component: RetailItemOptionsEdit},
                        ]
                    },

                    {
                        path: 'addons',
                        component: RetailItemAddonsCat,
                        children: [
                            {   path: '/', name: 'items.addons', component: RetailItemAddons},
                            {   path: 'create', name: 'items.addons.create', component: RetailItemAddonsCreate},
                            {   path: ':addon_id/edit', name: 'items.addons.edit', component: RetailItemAddonsEdit},
                        ]
                    },

                    {   path: ':category_id/edit', name: 'categories.edit', component: CategoryEdit },
                    {   path: ':category_id/detail', name: 'categories.detail', component: CategoryDetail },
                    {
                        path: 'items',
                        component: Items,
                        children: [
                            {   path: '/', name: 'items.list', component: ItemList },
                            {   path: 'create', name: 'items.create', component: ItemCreate },
                            {   path: ':item_id/edit', name: 'items.edit', component: ItemEdit },
                        ]
                    },
                ]
            },
            {
                name: 'customers',
                path: 'customers',
                component: Users,
                children: [
                    {   path: '', name: 'customers.list', component: UsersList },
                ]
            },
            {
                path: 'reports',
                name: 'reports',
                redirect: { name: 'reports.sold' },
                component: Order,
                children: [
                    {   path: 'sold', name: 'reports.sold', component: ReportsSold },
                ]
            },
            {
                path: 'orders',
                name: 'orders',
                redirect: { name: 'orders.active' },
                component: Order,
                children: [
                    {   path: 'active', name: 'orders.active', component: OrderActive },
                    {   path: 'list', name: 'orders.list', component: OrdersList },
                ]
            },
            {
                path: 'settings',
                name: 'settings',
                component: Settings,
                redirect: { name: 'settings.profile' },
                children: [
                    { path: 'profile', name: 'settings.profile', component: SettingsProfile },
                    { path: 'password', name: 'settings.password', component: SettingsPassword },
                    { path: 'payment', name: 'settings.payment', component: SettingsPayment },
                    { path: 'currencies', name: 'settings.currencies', component: SettingsCurrencies }
                ]
            },
        ]
    },
    { path: '/home', redirect: '/'},
    {
        path: '/admin',
        component: Admin,
        children: [
            { path: '', redirect: { name: 'admin.dashboard' } },
            { path: 'dashboard', name: 'admin.dashboard', component: AdminDashboard },

            {
                path: 'users',
                component: AdminUser,
                children: [
                    { path: '/', name: 'admin.users.list', component: AdminUserList },
                    { path: 'create', name: 'admin.users.create', component: AdminUserCreate },
                    { path: ':id/edit', name: 'admin.users.edit', component: AdminUserEdit },
                    { path: ':id/change-password', name: 'admin.users.password', component: AdminUserChangePassword },
                    { path: ':id/price-config', name: 'admin.users.price_config', component: AdminUserPriceConfig },
                ]
            },
            {
                path: 'locations',
                component: AdminLocation,
                redirect: { name: 'admin.locations.list' },
                children: [
                    { path: '/', name: 'admin.locations.list', component: AdminLocationList },
                    { path: 'create', name: 'admin.locations.create', component: AdminLocationCreate },
                    {
                        path: ':location_id',
                        name: 'admin.locations.detail',
                        component: AdminLocationShow,
                        redirect: { name: 'admin.locations.detail.index' },
                        children: [
                            {   path: 'detail', name: 'admin.locations.detail.index', component: AdminLocationDetail },
                            {   path: 'menu', name: 'admin.locations.detail.menu', component: AdminLocationMenu },
                            {   path: 'order', name: 'admin.locations.detail.order', component: AdminLocationOrder },
                        ]
                    }
                ]
            },
            {
                path: 'orders',
                name: 'admin.orders',
                redirect: { name: 'admin.orders.list' },
                component: AdminOrder,
                children: [
                    {   path: 'list', name: 'admin.orders.list', component: AdminOrdersList },
                ]
            },
            {
                path: 'settings',
                name: 'admin.settings',
                component: AdminSettings,
                redirect: { name: 'admin.settings.profile' },
                children: [
                    { path: 'profile', name: 'admin.settings.profile', component: SettingsProfile },
                    { path: 'password', name: 'admin.settings.password', component: SettingsPassword }
                ]
            },
        ]
    },

    {   path: '*', component: NotFound }
]
