const Admin = () => import('~/pages/admin/index').then(m => m.default || m)
const AdminDashboard = () => import('~/pages/admin/dashboard').then(m => m.default || m)

const AdminRetailer = () => import('~/pages/admin/retailer/index').then(m => m.default || m)
const AdminRetailerList = () => import('~/pages/admin/retailer/list').then(m => m.default || m)
const AdminRetailerCreate = () => import('~/pages/admin/retailer/create').then(m => m.default || m)
const AdminRetailerEdit = () => import('~/pages/admin/retailer/edit').then(m => m.default || m)

const AdminUser = () => import('~/pages/admin/user/index').then(m => m.default || m)
const AdminUserList = () => import('~/pages/admin/user/list').then(m => m.default || m)
const AdminUserCreate = () => import('~/pages/admin/user/create').then(m => m.default || m)
const AdminUserEdit = () => import('~/pages/admin/user/edit').then(m => m.default || m)

const AdminLocation = () => import('~/pages/admin/location/list').then(m => m.default || m)
const AdminLocationList = () => import('~/pages/admin/location/index').then(m => m.default || m)
const AdminLocationCreate = () => import('~/pages/admin/location/create').then(m => m.default || m)
const AdminLocationEdit = () => import('~/pages/admin/location/edit').then(m => m.default || m)

const AdminSettings = () => import('~/pages/admin/settings/index').then(m => m.default || m)
const AdminSettingsProfile = () => import('~/pages/admin/settings/profile').then(m => m.default || m)
const AdminSettingsPassword = () => import('~/pages/admin/settings/password').then(m => m.default || m)

const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)

export default [
    {
        path: '/admin',
        component: Admin,
        children: [
            { path: '', redirect: { name: 'admin.dashboard' } },
            { path: 'dashboard', name: 'admin.dashboard', component: AdminDashboard },
            
            {
                path: 'retailer',
                name: 'admin.retailers',
                component: AdminRetailer,
                children: [
                    { path: '/', name: 'admin.retailers.list', component: AdminRetailerList },
                    { path: 'create', name: 'admin.retailers.create', component: AdminRetailerCreate },
                    { path: 'edit/:id', name: 'admin.retailers.edit', component: AdminRetailerEdit },
                ]
            },
            {
                path: 'user',
                name: 'admin.users',
                component: AdminUser,
                children: [
                    { path: '/', name: 'admin.users.list', component: AdminUserList },
                    { path: 'create', name: 'admin.users.create', component: AdminUserCreate },
                    { path: 'edit/:id', name: 'admin.users.edit', component: AdminUserEdit },
                ]
            },
            {
                path: 'location',
                name: 'admin.locations',
                component: AdminLocation,
                children: [
                    { path: '/', name: 'admin.locations.list', component: AdminLocationList },
                    { path: 'create', name: 'admin.locations.create', component: AdminLocationCreate },
                    { path: 'edit/:id', name: 'admin.locations.edit', component: AdminLocationEdit },
                ]
            },
            {
                path: 'settings',
                name: 'admin.settings',
                component: AdminSettings,
                children: [
                    { path: '', redirect: { name: 'admin.settings.profile' } },
                    { path: 'profile', name: 'admin.settings.profile', component: AdminSettingsProfile },
                    { path: 'password', name: 'admin.settings.password', component: AdminSettingsPassword }
                ]
            },
        ]
    },
    {
        path: '*',
        component: NotFound
    }
]
