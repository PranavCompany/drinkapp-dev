const Retail = () => import('~/pages/retail/index').then(m => m.default || m)
const RetailDashboard = () => import('~/pages/retail/dashboard').then(m => m.default || m)
const RetailReports = () => import('~/pages/retail/dashboard').then(m => m.default || m)

const RetailLocation = () => import('~/pages/retail/location/list').then(m => m.default || m)
const RetailLocationList = () => import('~/pages/retail/location/index').then(m => m.default || m)
const RetailLocationCreate = () => import('~/pages/retail/location/create').then(m => m.default || m)
const RetailLocationEdit = () => import('~/pages/retail/location/edit').then(m => m.default || m)

const RetailSettings = () => import('~/pages/retail/settings/index').then(m => m.default || m)
const RetailSettingsProfile = () => import('~/pages/retail/settings/profile').then(m => m.default || m)
const RetailSettingsPassword = () => import('~/pages/retail/settings/password').then(m => m.default || m)

const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)

export default [
    {
        path: '/retail/:retail_id',
        component: Retail,
        children: [
            { path: '', redirect: { name: 'retail.dashboard' } },
            { path: 'dashboard', name: 'retail.dashboard', component: RetailDashboard },
            { path: 'reports', name: 'retail.reports', component: RetailReports },

            {
                path: 'location',
                name: 'retail.locations',
                component: RetailLocation,
                children: [
                    { path: '/', name: 'retail.locations.list', component: RetailLocationList },
                    { path: 'create', name: 'retail.locations.create', component: RetailLocationCreate },
                    { path: 'edit/:id', name: 'retail.locations.edit', component: RetailLocationEdit },
                ]
            },
            {
                path: 'category',
                name: 'retail.categories',
                component: RetailLocation,
                children: [
                    { path: '/', name: 'retail.categories.list', component: RetailLocationList },
                    { path: 'create', name: 'retail.categories.create', component: RetailLocationCreate },
                    { path: 'edit/:id', name: 'retail.categories.edit', component: RetailLocationEdit },
                ]
            },
            {
                path: 'item',
                name: 'retail.items',
                component: RetailLocation,
                children: [
                    { path: '/', name: 'retail.items.list', component: RetailLocationList },
                    { path: 'create', name: 'retail.items.create', component: RetailLocationCreate },
                    { path: 'edit/:id', name: 'retail.items.edit', component: RetailLocationEdit },
                ]
            },
            {
                path: 'order',
                name: 'retail.orders',
                component: RetailLocation,
                children: [
                    { path: '/', name: 'retail.orders.list', component: RetailLocationList },
                    { path: 'create', name: 'retail.orders.create', component: RetailLocationCreate },
                    { path: 'edit/:id', name: 'retail.orders.edit', component: RetailLocationEdit },
                ]
            },
            {
                path: 'settings',
                name: 'retail.settings',
                component: RetailSettings,
                children: [
                    { path: '', redirect: { name: 'retail.settings.profile' } },
                    { path: 'profile', name: 'retail.settings.profile', component: RetailSettingsProfile },
                    { path: 'password', name: 'retail.settings.password', component: RetailSettingsPassword }
                ]
            },
        ]
    },
    {
        path: '*',
        component: NotFound
    }
]
