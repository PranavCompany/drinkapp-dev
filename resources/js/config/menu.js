export const admin = [
    {
        icon: 'th-large',
        name: 'Dashboard', //this.$t('profile'),
        route: 'admin.dashboard'
    },
    {
        icon: 'users',
        name: 'Users', //this.$t('profile'),
        route: 'admin.users.list'
    },
    {
        icon: 'store',
        name: 'Retail Locations', //this.$t('profile'),
        route: 'admin.locations.list'
    },
    {
        icon: 'clipboard-list',
        name: 'All Orders',
        route: 'admin.orders.list'
    },
    {
        icon: 'cog',
        name: 'Settings', //this.$t('profile'),
        route: 'admin.settings',
        children: [{
            icon: 'user',
            name: 'Profile',
            route: 'admin.settings.profile'
        }, {
            icon: 'lock',
            name: 'Security',
            route: 'admin.settings.password'
        }]
    }
];
export const retail = [
    {
        icon: 'th-large',
        name: 'Dashboard', //this.$t('profile'),
        route: 'dashboard'
    },
    {
        icon: 'store',
        name: 'Retail Locations', //this.$t('profile'),
        route: 'locations.list'
    },
    {
        icon: 'users',
        name: 'Customers', //this.$t('profile'),
        route: 'customers.list'
    },
    {
        icon: 'list-alt',
        name: 'Menu',
        route: 'categories.list',
        children: [
            {
                icon: 'list-alt',
                name: 'Menu Categories',
                route: 'categories.list',
            },
            {
                icon: 'cocktail',
                name: 'Menu Items', //this.$t('profile'),
                route: 'items.list'
            },
            {
                icon: 'list-alt',
                name: 'Items Options',
                route: 'items.options',
            },
            {
                icon: 'list-alt',
                name: 'Items Addons',
                route: 'items.addons',
            },
        ]
    },

    {
        icon: 'clipboard-list',
        name: 'Orders',
        route: 'orders',
        children: [
            {
                icon: 'th-large',
                name: 'Active Orders',
                route: 'orders.active'
            },
            {
                icon: 'clipboard-list',
                name: 'All Orders',
                route: 'orders.list'
            }
        ]
    },
    {
        icon: 'clipboard-list',
        name: 'Reports',
        route: 'reports',
        children: [
            {
                icon: 'clipboard-list',
                name: 'Products sold',
                route: 'reports.sold'
            },

        ]
    },
    {
        icon: 'cog',
        name: 'Settings', //this.$t('profile'),
        route: 'settings',
        children: [{
            icon: 'user',
            name: 'Profile',
            route: 'settings.profile'
        }, {
            icon: 'lock',
            name: 'Security',
            route: 'settings.password'
        }, {
            icon: 'credit-card',
            name: 'Currencies',
            route: 'settings.currencies'
        }, {
            icon: 'credit-card',
            name: 'Get paid',
            route: 'settings.payment'
        }]
    }
];
