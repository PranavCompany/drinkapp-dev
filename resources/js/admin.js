require('./bootstrap');

import Vue from 'vue'
import store from '~/store'
import router from '~/router/admin'
import i18n from '~/plugins/i18n'
import http from '~/plugins/http'
import App from '~/components/App'

import '~/plugins'
import '~/components'

Vue.use(http);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  	i18n,
  	store,
  	router,
  	...App
});
