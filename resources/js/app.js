require('./bootstrap');

import Vue from 'vue'
import VueCurrencyFilter from 'vue-currency-filter';
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import http from '~/plugins/http'
import App from '~/components/App'
import '~/plugins'
import '~/components'
import moment from 'moment';
import 'vue-select/dist/vue-select.css';

Vue.use(VueCurrencyFilter);
Vue.use(http);

Vue.config.productionTip = false
Vue.prototype.moment = moment;
/* eslint-disable no-new */
new Vue({
  	i18n,
  	store,
  	router,
  	...App
});
