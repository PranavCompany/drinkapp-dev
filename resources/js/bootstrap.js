window._ = require('lodash')

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  	window.Popper = require('popper.js').default
  	// window.$ = window.jQuery = require('jquery')

  	require('bootstrap')

  	// window.swal = require('sweetalert2')

  	window.toastr = require('toastr/build/toastr.min.js')
  	window.toastr.options = {
	    positionClass: 'toast-top-right',
	    showDuration: '300',
	    hideDuration: '1000',
	    timeOut: '5000',
	    extendedTimeOut: '1000',
	    showEasing: 'swing',
	    hideEasing: 'linear',
	    showMethod: 'fadeIn',
	    hideMethod: 'fadeOut'
  	}
} catch (e) {
  	console.log('bootstrap error')
}
