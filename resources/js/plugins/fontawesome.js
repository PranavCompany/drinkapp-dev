import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/free-regular-svg-icons'

import {
  	faCreditCard, faImage, faKey, faRedo, faTimes, faPhone, faMapMarkerAlt, faPlus, faEye, faBell, faClipboardList, faCocktail, faListAlt, faThLarge, faHome, faUser, faUsers, faTachometerAlt, faPencilAlt, faSearch, faCircle, faTrashAlt, faStore, faLock, faSignOutAlt, faCog, faChevronLeft, faChevronRight
} from '@fortawesome/free-solid-svg-icons'

import {
  	faGithub
} from '@fortawesome/free-brands-svg-icons'

// import {
//   faEye
// } from '@fortawesome/free-regular-svg-icons'

library.add(
  	faCreditCard, faImage, faKey, faRedo, faTimes, faPhone, faMapMarkerAlt, faPlus, faEye, faBell, faClipboardList, faCocktail, faListAlt, faThLarge, faHome, faUser, faUsers, faTachometerAlt, faStore, faSearch, faPencilAlt, faTrashAlt, faCircle, faLock, faSignOutAlt, faCog, faGithub, faChevronLeft, faChevronRight,
)

Vue.component('fa', FontAwesomeIcon)
