import axios from "axios";

export const setListCurrencies = (user, centered) => {
    let fields = [];
    const map_ = {
        eur: {
            name: 'price_eur',
            title: 'Price EUR',
            callback: 'price_eur',
            titleClass: centered ? 'text-center' : '',
        },
        gbp: {
            name: 'price_gbp',
            title: 'Price GBP',
            titleClass: centered ? 'text-center' : '',
            callback: 'price_gbp'
        },
        aed: {
            name: 'price_aed',
            title: 'Price AED',
            titleClass: centered ? 'text-center' : '',
            callback: 'price_aed'
        },
        usd: {
            name: 'price_usd',
            title: 'Price USD',
            titleClass: centered ? 'text-center' : '',
            callback: 'price_usd'
        }
    };
    user.currencies.forEach((value) => {
        fields.push(map_[value]);
    });
    return fields;
};

export const setPrinter = (that, ip, location_id) => {
    let ePosDev = new window.epson.ePOSDevice();
    ePosDev.connect(ip, 8043, cbConnect);
    function cbConnect(data, c) {
        if(data == 'OK' || data == 'SSL_CONNECT_OK') {
            that.$store.dispatch('page/setSelectedLocationPrinterHTTPSError', {selected_location_printer_https_errors: false});
            ePosDev.createDevice('local_printer', ePosDev.DEVICE_TYPE_PRINTER,
                {'crypto':false, 'buffer':false}, cbCreateDevice_printer);
        } else {
            if (data == 'ERROR_TIMEOUT') {
                axios.get('https://'+ip).then(function (resp) {
                }).catch(function (resp) {
                    if (resp.message === 'Network Error') {
                        that.$store.dispatch('page/setSelectedLocationPrinterHTTPSError', {selected_location_printer_https_errors: true});
                    }
                });
            }
            let printers = that.$store.getters['page/can_print'];
            printers[location_id] = false;
            that.$store.dispatch('page/setCanPrint', {can_print: printers});
        }
    }
    function cbCreateDevice_printer(devobj, retcode) {
        let printers = that.$store.getters['page/can_print'];
        if( retcode == 'OK' ) {
            let printer = devobj;
            printer.timeout = 50000;
            printers[location_id] = {
                printer: printer,
                device: ePosDev,
            };
        } else {
            printers[location_id] = false;
        }
        that.$store.dispatch('page/setCanPrint', {can_print: printers});
    }
};
